<?php

use Monolog\Handler\NullHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\SyslogUdpHandler;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Log Channel
    |--------------------------------------------------------------------------
    |
    | This option defines the default log channel that gets used when writing
    | messages to the logs. The name specified in this option should match
    | one of the channels defined in the "channels" configuration array.
    |
    */

    'default' => env('LOG_CHANNEL', 'stack'),

    /*
    |--------------------------------------------------------------------------
    | Deprecations Log Channel
    |--------------------------------------------------------------------------
    |
    | This option controls the log channel that should be used to log warnings
    | regarding deprecated PHP and library features. This allows you to get
    | your application ready for upcoming major versions of dependencies.
    |
    */

    'deprecations' => env('LOG_DEPRECATIONS_CHANNEL', 'null'),

    /*
    |--------------------------------------------------------------------------
    | Log Channels
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log channels for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Drivers: "single", "daily", "slack", "syslog",
    |                    "errorlog", "monolog",
    |                    "custom", "stack"
    |
    */

    'channels' => [
        'stack' => [
            'driver' => 'stack',
            'channels' => ['single'],
            'ignore_exceptions' => false,
        ],

        'single' => [
            'driver' => 'single',
            'path' => storage_path('logs/laravel.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'daily' => [
            'driver' => 'daily', //daily  才是使用日期的关键  会在日志文件名后面加上日期，然后下面参数  days 才有效
            'path' => storage_path('logs/laravel.log'),
            'level' => env('LOG_LEVEL', 'debug'),
            'days' => 100, #保留100个文件，以日期保存
        ],

        'slack' => [
            'driver' => 'slack',
            'url' => env('LOG_SLACK_WEBHOOK_URL'),
            'username' => 'Laravel Log',
            'emoji' => ':boom:',
            'level' => env('LOG_LEVEL', 'critical'),
        ],

        'papertrail' => [
            'driver' => 'monolog',
            'level' => env('LOG_LEVEL', 'debug'),
            'handler' => SyslogUdpHandler::class,
            'handler_with' => [
                'host' => env('PAPERTRAIL_URL'),
                'port' => env('PAPERTRAIL_PORT'),
            ],
        ],

        'stderr' => [
            'driver' => 'monolog',
            'level' => env('LOG_LEVEL', 'debug'),
            'handler' => StreamHandler::class,
            'formatter' => env('LOG_STDERR_FORMATTER'),
            'with' => [
                'stream' => 'php://stderr',
            ],
        ],

        'syslog' => [
            'driver' => 'syslog',
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'errorlog' => [
            'driver' => 'errorlog',
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'null' => [
            'driver' => 'monolog',
            'handler' => NullHandler::class,
        ],

        'emergency' => [
            'path' => storage_path('logs/laravel.log'),
        ],

        //定义图书馆接口日志目录  独立日志文件   //用法：Log::channel('libapi')->debug('内容'); Log::channel('libapi')->info('内容');    info 为级别
        'libapi' => [
            'driver' => 'daily', //以日期保存
            'path' => storage_path('logs/laravel-lib-api.log'),
            'level' => env('LOG_LEVEL', 'debug'),
            'days' => 100, #保留100个文件，以日期保存
        ],

        //定义图书馆接口日志目录  独立日志文件   //用法：Log::channel('libapi')->debug('内容'); Log::channel('libapi')->info('内容');    info 为级别
        'draw_activity' => [
            'driver' => 'daily', //以日期保存
            'path' => storage_path('logs/laravel-draw-activity.log'),
            'level' => env('LOG_LEVEL', 'debug'),
            'days' => 100, #保留100个文件，以日期保存
        ],

        //定义图书馆接口日志目录  独立日志文件   //用法：Log::channel('libapi')->debug('内容'); Log::channel('libapi')->info('内容');    info 为级别
        'sqllog' => [
            'driver' => 'daily', //以日期保存
            'path' => storage_path('logs/laravel-sql-log.log'),
            'level' => env('LOG_LEVEL', 'debug'),
            'days' => 100, #保留100个文件，以日期保存
        ],

        //定义邮政接口接口日志目录  独立日志文件   //用法：Log::channel('libapi')->debug('内容'); Log::channel('libapi')->info('内容');    info 为级别
        'postallog' => [
            'driver' => 'daily', //以日期保存
            'path' => storage_path('logs/laravel-postal-log.log'),
            'level' => env('LOG_LEVEL', 'debug'),
            'days' => 100, #保留100个文件，以日期保存
        ],

        //定义邮政接口接口日志目录  独立日志文件   //用法：Log::channel('libapi')->debug('内容'); Log::channel('libapi')->info('内容');    info 为级别
        'templatepushlog' => [
            'driver' => 'daily', //以日期保存
            'path' => storage_path('logs/laravel-template-push-log.log'),
            'level' => env('LOG_LEVEL', 'debug'),
            'days' => 100, #保留100个文件，以日期保存
        ],
         //系统执行的cmd文件  独立日志文件   //用法：Log::channel('handcmdlog')->debug('内容'); Log::channel('libapi')->info('内容');    info 为级别
         'handcmdlog' => [
            'driver' => 'daily', //以日期保存
            'path' => storage_path('logs/laravel-hand-cmd-log.log'),
            'level' => env('LOG_LEVEL', 'debug'),
            'days' => 100, #保留100个文件，以日期保存
        ],
    ],

];
