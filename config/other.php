<?php


/**
 * 自定义其他信息
 */


return [
    //项目名，用于拼接二维码链接
    // "project_name" => 'resource_integration_scyb/',
    "project_name" => '',

    //图书馆名称
    'lib_name' => '宜宾市图书馆',

    //独立活动显示名称
    'unit_name' => '主办方',

    //后台管理员token过期时间
    'admin_token_refresh_time' => 7200, //单位s


    //程序有效期，超过这个时间段，将不能在使用
    'app_start_time' => '2024-09-01', //开始时间，记录作用，无意义
    'app_expire_time' => '2035-03-01', //如果没填就是永久
    'app_expire_msg' => '试用已过期，如需继续使用，请联系管理员处理！', //如果没填就是永久


    'md5_key' => 'a5fbf270764293be79c88838f33f17e7',

    'gaode_key' => '6c8bb9226fbcef660db6c3f1b36267af', //高德地图key


    // 'time_filtrate_interval_day' => 31,//时间筛选最大的天数

    // 'invite_user' => true, //是否显示邀请二维码按钮 不在这里配置，采用积分消息配置

    //是否验证图书馆接口 true 需要， false 不需要 (用于接口可有可无的情况，必须验证的接口，无需此判断)
    'is_validate_lib_api' => true, //图书到家登录接口不实用此配置，因为图书到家必须要读者证号登录接口





    //在线办证是否需要选择书店id
    'online_registration_is_need_shop_id' => false, //false不需要



    //视频加密key
    'video_key' => '37b81cb42a7a67a6beab74de3bf53c51',

    //是否需要积分系统
    'is_need_score' => true, //true 需要，false不需要
    'score_table' => 'user_account_lib', //积分关联表 user_account_lib 读者证表   user_info 用户信息表（用于不绑定读者证时使用） 暂时不能修改，修改要改相应代码逻辑


    'tel_key' => 'resource_integration_scyb', //手机验证码发送的key


    //列表返回的序号名
    'list_index_key' => 'key',

    //扫码签到有效范围 单位米
    'scan_sign_valid_range' => 1000,

    //年度账单开启月份
    'year_bill_open_month' => ['01', '02'],

    //当前项目是小程序还是公众号
    'project_type' => 'applet', //小程序 applet   微信公众号 wechat



    //是否发送微信模板消息
    'is_send_wechat_temp_info' => false,


    //景点打卡距离判断
    'scenic_range' => '5000', //


    //宜宾市图书馆微信公众号信息
    //  'appid' => 'wx126482e83bcf2975',
    //  'appsecret' => 'c3e6b1c149cd9c30910969337e9bdf01',

    //宜宾市图书馆微信小程序信息
    'applet_appid' => 'wx928e082c72a7ced8',
    'applet_appsecret' => 'e844065371e48273661dac6ac373bb47',

    //是否微信、web端同时登录授权
    'is_web_auth' => false, //默认false

    //推送地址
    'borrow_success' => '', //图书馆借书通知
    'return_success' => '', //图书馆还书成功通知
    'apply_success' => '', //申请成功通知
    'apply_reject' => '', //申请驳回通知
    'apply_reject1' => '', //申请驳回通知,归还失败的，用这个链接
    'renew_success' => '', //图书馆续借成功通知
    'make_inform' => '', //预约的图书到馆通知
    'activity_new_issue' => '', //活动新发布通知
    'my_activity_list' => '', //我的活动列表


    //违规配置
    'violate_config' => [
        ['field' => 'activity', 'name' => '活动报名'],
        ['field' => 'reservation', 'name' => '场馆预约'],
        // ['field' => 'contest', 'name' => '在线投票'],
        //  ['field' => 'scenic', 'name' => '景点打卡'],
        //  ['field' => 'study_room_reservation', 'name' => '空间预约'],
        // ['field' => 'competite', 'name' => '作品征集活动'],
    ], //activity  活动违规（手动违规） reservation 预约违规（手动+自动）contest 大赛违规（手动违规）scenic 景点打卡（手动违规） study_room_reservation 空间预约违规（手动+自动）

    /**
     * 全局条形码
     * 200:成功       筛选列表   无数据也是返回的  200，但是数据是空数组
     * 201:参数错误
     * 202:添加错误，修改错误等错误
     * 203:暂无数据         筛选列表   无数据也是返回的  200，但是数据是空数组
     * 204:需绑定读者证号，跳到绑定读者证号界面（读者证号）（特殊除外，会特殊说明）
     * 205:前台请求接口JWT错误 ，需重新获取Jwt      后台为登录过期，从新登录
     * 206:授权已过期，请重新授权（微信授权）    后台一般为无权限使用模块（后台一般不做处理）
     * 207:开始自定义
     *
     * 301:试用已过期！
     * 302:邀请码输入错误
     */

    //后台存在的权限id  空表示所有
    'admin_show_auth_id' => [],
    //前台接口权限  空表示所有
    'wechat_port_auth' => [],

];
