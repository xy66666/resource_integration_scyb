<?php


/**
 * jwt 配置文件，自己配置
 */
return [


    //自定义获取jwt token 的参数
    'appid' => 'resource_integration_scyb',
    'appsecret' => '6e8ec9a624d6da49ee72a9aef970fe7a',


    'issue' => 'resource_integration_scyb',//签发人
    'accept' => 'user',//接收人
    'title' => 'resource_integration_scyb',//标题id
    'expires_in' => '1200',//token刷新时间（过期时间）  默认 20 分钟
    'sign' => '31426d88e5128fe5e15a92653b350898',//签名



];
