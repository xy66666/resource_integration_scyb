<?php

use App\Http\Controllers\Admin;
use App\Http\Controllers\Live;
use App\Http\Controllers;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\VideoPlayController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('videoPlay/play', [VideoPlayController::class, 'play']); // 播放视频资源接口


// 后台接口

//后台管理员登录
Route::post('admin/login/login', [Admin\LoginController::class, 'login']);
Route::get('admin/login/getCaptcha', [Admin\LoginController::class, 'getCaptcha']); //获取图形验证码
Route::get('slideVerify/getCaptcha', [App\Http\Controllers\SlideVerifyController::class, 'getCaptcha']); //获取滑动验证码参数

Route::get('text/bookTypesFilterList', [App\Http\Controllers\TextController::class, 'bookTypesFilterList']); //书籍类型筛选列表(用于下拉框选择)

Route::get('text/getCityAll', [App\Http\Controllers\TextController::class, 'getCityAll']); //获取城市列表

//文件上传
Route::post('/uploads/files', [App\Http\Controllers\UploadsFileController::class, 'commonUpload']); //公共文件上传
// Route::post('/uploads/ocrIdCardImg', [App\Http\Controllers\IdCardUploadsOcrController::class, 'ocrIdCardImg']); //身份证图片上传，然后进行ocr识别

// Route::post('/pictureLiveUploads/files', [App\Http\Controllers\PictureLiveUploadsFileController::class, 'commonUpload']); //图片直播公共文件上传


// /***直播 */
// Route::post('live/liveVideoCallback/getVideo', [Live\LiveVideoCallbackController::class, 'getVideo']); //  视频录制回调地址（处理数据）(唯一)  必须是post格式
// Route::post('live/liveVideoCallback/channelStatus', [Live\LiveVideoCallbackController::class, 'channelStatus']); // 频道状态变化回调地址（处理数据）(唯一)  必须是post格式


// //异步发送推送数据
// Route::post('admin/serviceNotice/asyncSend', [Admin\ServiceNoticeController::class, 'asyncSend']); // 异步发送数据
Route::get('admin/userInfo/asyncSend', [Admin\UserInfoController::class, 'asyncSend']); // 异步发送数据


/***直播**/

Route::middleware(['check.admin.header.token', 'check.admin.permission'])->prefix('admin')->group(function () {

    /**
     * 应用显示灰色
     */
    Route::get('/appViewGray/list', [Admin\AppViewGrayController::class, 'lists']); // 列表
    Route::get('/appViewGray/detail', [Admin\AppViewGrayController::class, 'detail']); // 详情
    Route::post('/appViewGray/add', [Admin\AppViewGrayController::class, 'add']); // 新增
    Route::post('/appViewGray/change', [Admin\AppViewGrayController::class, 'change']); // 编辑
    Route::post('/appViewGray/del', [Admin\AppViewGrayController::class, 'del']); // 删除


    /**
     * 协议规则
     */
    Route::get('/userProtocol/getProtocol', [Admin\UserProtocolController::class, 'getProtocol']); //获取用户协议
    Route::post('/userProtocol/setProtocol', [Admin\UserProtocolController::class, 'setProtocol']); //配置用户协议


    /**
     * 管理员管理
     */
    Route::get('/manage/filterList', [Admin\ManageController::class, 'filterList']);
    Route::get('/manage/list', [Admin\ManageController::class, 'lists']);
    Route::get('/manage/detail', [Admin\ManageController::class, 'detail']);
    Route::post('/manage/add', [Admin\ManageController::class, 'add']);
    Route::post('/manage/change', [Admin\ManageController::class, 'change']);
    Route::post('/manage/del', [Admin\ManageController::class, 'del']);
    Route::post('/manage/changePwd', [Admin\ManageController::class, 'manageChangePwd']); //修改密码
    Route::post('/manage/changeSelfPwd', [Admin\ManageController::class, 'manageChangeSelfPwd']); //修改自己的密码


    /**
     * 新闻类型
     */
    Route::get('/newsType/filterList', [Admin\NewsTypeController::class, 'filterList']); // 筛选列表
    Route::get('/newsType/list', [Admin\NewsTypeController::class, 'lists']); // 列表
    Route::get('/newsType/detail', [Admin\NewsTypeController::class, 'detail']); // 详情
    Route::post('/newsType/add', [Admin\NewsTypeController::class, 'add']); // 新增
    Route::post('/newsType/change', [Admin\NewsTypeController::class, 'change']); // 编辑
    Route::post('/newsType/del', [Admin\NewsTypeController::class, 'del']); // 删除

    /**
     * 新闻
     */
    Route::get('/news/list', [Admin\NewsController::class, 'lists']); // 列表
    Route::get('/news/detail', [Admin\NewsController::class, 'detail']); // 详情
    Route::post('/news/add', [Admin\NewsController::class, 'add']); // 新增
    Route::post('/news/change', [Admin\NewsController::class, 'change']); // 编辑
    Route::post('/news/del', [Admin\NewsController::class, 'del']); // 删除
    Route::post('/news/topAndCancel', [Admin\NewsController::class, 'topAndCancel']); // 置顶与取消置顶



    /**
     * 数字阅读类型
     */
    Route::get('/digitalType/list', [Admin\DigitalTypeController::class, 'lists']);
    Route::get('/digitalType/detail', [Admin\DigitalTypeController::class, 'detail']);
    Route::post('/digitalType/add', [Admin\DigitalTypeController::class, 'add']);
    Route::post('/digitalType/change', [Admin\DigitalTypeController::class, 'change']);
    Route::post('/digitalType/del', [Admin\DigitalTypeController::class, 'del']);
    Route::get('/digitalType/filterList', [Admin\DigitalTypeController::class, 'filterList']); //数字阅读类型列表（用于筛选）


    /**
     * 数字阅读
     */
    Route::get('/digital/list', [Admin\DigitalController::class, 'lists']); // 列表
    Route::get('/digital/detail', [Admin\DigitalController::class, 'detail']); // 详情
    Route::post('/digital/add', [Admin\DigitalController::class, 'add']); // 新增
    Route::post('/digital/change', [Admin\DigitalController::class, 'change']); // 编辑
    Route::post('/digital/del', [Admin\DigitalController::class, 'del']); // 删除
    Route::post('/digital/cancelAndRelease', [Admin\DigitalController::class, 'cancelAndRelease']); //  撤销 和发布
    Route::post('/digital/sortChange', [Admin\DigitalController::class, 'sortChange']); // 数字阅读排序

    /**
     * 新书推荐类型
     */
    Route::get('/newBookRecommendType/filterList', [Admin\NewBookRecommendTypeController::class, 'filterList']); // 筛选列表
    Route::get('/newBookRecommendType/list', [Admin\NewBookRecommendTypeController::class, 'lists']); // 列表
    Route::get('/newBookRecommendType/detail', [Admin\NewBookRecommendTypeController::class, 'detail']); // 详情
    Route::post('/newBookRecommendType/add', [Admin\NewBookRecommendTypeController::class, 'add']); // 新增
    Route::post('/newBookRecommendType/change', [Admin\NewBookRecommendTypeController::class, 'change']); // 编辑
    Route::post('/newBookRecommendType/del', [Admin\NewBookRecommendTypeController::class, 'del']); // 删除

    /**
     * 新书推荐
     */
    Route::get('/newBookRecommend/list', [Admin\NewBookRecommendController::class, 'lists']); // 列表
    Route::get('/newBookRecommend/detail', [Admin\NewBookRecommendController::class, 'detail']); // 详情
    Route::post('/newBookRecommend/add', [Admin\NewBookRecommendController::class, 'add']); // 新增
    Route::post('/newBookRecommend/change', [Admin\NewBookRecommendController::class, 'change']); // 编辑
    Route::post('/newBookRecommend/del', [Admin\NewBookRecommendController::class, 'del']); // 删除
    Route::post('/newBookRecommend/cancelAndRelease', [Admin\NewBookRecommendController::class, 'cancelAndRelease']); // 推荐与取消推荐




    /**
     * 活动
     */
    Route::get('/activity/getActivityApplyParam', [Admin\ActivityController::class, 'getActivityApplyParam']); // 获取报名参数
    Route::get('/activity/list', [Admin\ActivityController::class, 'lists']); // 列表
    Route::get('/activity/detail', [Admin\ActivityController::class, 'detail']); // 详情
    Route::post('/activity/add', [Admin\ActivityController::class, 'add']); // 新增
    Route::post('/activity/change', [Admin\ActivityController::class, 'change']); // 编辑
    Route::post('/activity/del', [Admin\ActivityController::class, 'del']); // 删除
    Route::post('/activity/cancel', [Admin\ActivityController::class, 'cancel']); // 活动撤销
    Route::post('/activity/release', [Admin\ActivityController::class, 'release']); // 活动发布
    Route::post('/activity/sign', [Admin\ActivityController::class, 'sign']); // 签到
    Route::post('/activity/switchoverType', [Admin\ActivityController::class, 'switchoverType']); // 切换类型
    Route::post('/activity/recomAndCancel', [Admin\ActivityController::class, 'recomAndCancel']); // 活动推荐与取消推荐
    Route::post('/activity/push', [Admin\ActivityController::class, 'push']); // 活动推送 发送模板消息给用户
    Route::post('/activity/copyData', [Admin\ActivityController::class, 'copyData']); // 复制活动

    /**
     * 活动报名信息管理
     */
    Route::get('/activityApply/activityApplyList', [Admin\ActivityApplyController::class, 'activityApplyList']); // 活动申请列表
    Route::get('/activityApply/activityApplyUser', [Admin\ActivityApplyController::class, 'activityApplyUser']); // 活动人员列表
    Route::post('/activityApply/agreeAndRefused', [Admin\ActivityApplyController::class, 'agreeAndRefused']); // 审核通过 和 拒绝
    Route::post('/activityApply/violateAndCancel', [Admin\ActivityApplyController::class, 'violateAndCancel']); // 活动报名违规 与 取消违规操作

    /**
     * 活动类型
     */
    Route::get('/activityType/filterList', [Admin\ActivityTypeController::class, 'filterList']); // 筛选列表
    Route::get('/activityType/list', [Admin\ActivityTypeController::class, 'lists']); // 列表
    Route::get('/activityType/detail', [Admin\ActivityTypeController::class, 'detail']); // 详情
    Route::post('/activityType/add', [Admin\ActivityTypeController::class, 'add']); // 新增
    Route::post('/activityType/change', [Admin\ActivityTypeController::class, 'change']); // 编辑
    Route::post('/activityType/del', [Admin\ActivityTypeController::class, 'del']); // 删除




    /**
     * 活动标签
     */
    Route::get('/activityTag/filterList', [Admin\ActivityTagController::class, 'filterList']); // 筛选列表
    Route::get('/activityTag/list', [Admin\ActivityTagController::class, 'lists']); // 列表
    Route::get('/activityTag/detail', [Admin\ActivityTagController::class, 'detail']); // 详情
    Route::post('/activityTag/add', [Admin\ActivityTagController::class, 'add']); // 新增
    Route::post('/activityTag/change', [Admin\ActivityTagController::class, 'change']); // 编辑
    Route::post('/activityTag/del', [Admin\ActivityTagController::class, 'del']); // 删除



    /**
     * 权限
     */
    Route::get('/permission/list', [Admin\PermissionController::class, 'permissionList']); // 权限列表
    Route::get('/permission/tree', [Admin\PermissionController::class, 'permissionTree']); // 权限树形列表
    Route::get('/permission/detail', [Admin\PermissionController::class, 'permissionDetail']); // 权限详情
    Route::post('/permission/add', [Admin\PermissionController::class, 'permissionInsert']); // 权限新增
    Route::post('/permission/change', [Admin\PermissionController::class, 'permissionChange']); // 权限编辑
    Route::post('/permission/del', [Admin\PermissionController::class, 'permissionDel']); // 权限删除
    Route::post('/permission/orderChange', [Admin\PermissionController::class, 'permissionOrderChange']); // 更改权限排序

    /**
     * 角色
     */
    Route::get('/role/list', [Admin\RoleController::class, 'roleList']); // 角色列表
    Route::get('/role/filterList', [Admin\RoleController::class, 'roleFilterList']); // 角色列表 （筛选使用）
    Route::get('/role/detail', [Admin\RoleController::class, 'roleDetail']); // 角色详情
    Route::post('/role/add', [Admin\RoleController::class, 'roleAdd']); // 角色新增
    Route::post('/role/change', [Admin\RoleController::class, 'roleChange']); // 角色编辑
    Route::post('/role/del', [Admin\RoleController::class, 'roleDel']); // 角色删除


    /**
     * banner管理
     */
    Route::get('/banner/list', [Admin\BannerController::class, 'lists']); // 列表
    Route::get('/banner/detail', [Admin\BannerController::class, 'detail']); // 详情
    Route::post('/banner/add', [Admin\BannerController::class, 'add']); // 新增
    Route::post('/banner/change', [Admin\BannerController::class, 'change']); // 编辑
    Route::post('/banner/del', [Admin\BannerController::class, 'del']); // 删除
    Route::post('/banner/cancelAndRelease', [Admin\BannerController::class, 'cancelAndRelease']); // 切换显示方式
    Route::post('/banner/sortChange', [Admin\BannerController::class, 'sortChange']); // banner 图排序




    /**
     * 积分设置
     */
    Route::get('/scoreInfo/list', [Admin\ScoreInfoController::class, 'lists']); // 积分规则列表
    Route::post('/scoreInfo/change', [Admin\ScoreInfoController::class, 'change']); // 修改积分规则
    Route::get('/scoreInfo/scoreLog', [Admin\ScoreInfoController::class, 'scoreLog']); // 积分变动记录
    Route::post('/scoreInfo/scoreChange', [Admin\ScoreInfoController::class, 'scoreChange']); // 手动调整用户积分


    Route::get('/scoreInfo/customScoreRule', [Admin\ScoreInfoController::class, 'customScoreRule']); // 获取自定义积分规则
    Route::post('/scoreInfo/customScoreRuleChange', [Admin\ScoreInfoController::class, 'customScoreRuleChange']); // 设置自定义积分规则

    /**
     * 违规设置
     */
    Route::get('/violateConfig/detail', [Admin\ViolateConfigController::class, 'detail']); // 详情
    Route::post('/violateConfig/change', [Admin\ViolateConfigController::class, 'change']); // 修改违规设置

    /**
     * 用户管理
     */
    Route::get('/userInfo/list', [Admin\UserInfoController::class, 'lists']); //用户列表
    Route::post('/userInfo/clearViolate', [Admin\UserInfoController::class, 'clearViolate']); //  清空违规次数
    Route::get('/userInfo/getViolateList', [Admin\UserInfoController::class, 'getViolateList']); //  获取用户违规列表

    /**
     * 用户留言管理
     */
    Route::get('/feedback/list', [Admin\FeedbackController::class, 'lists']); //用户留言列表
    Route::post('/feedback/reply', [Admin\FeedbackController::class, 'reply']); //  用户留言回复


    /**
     * 主页信息
     */
    Route::get('/index/index', [Admin\IndexController::class, 'index']); //首页数据统计
    Route::get('/index/accessStatistics', [Admin\IndexController::class, 'accessStatistics']); //公众号访问统计

    /**
     * 预约
     */
    Route::get('/reservation/getReservationApplyParam', [Admin\ReservationController::class, 'getReservationApplyParam']); // 获取预约参数
    Route::get('/reservation/filterTagList', [Admin\ReservationController::class, 'filterTagList']); //文化配送标签列表（预定义）
    Route::get('/reservation/list', [Admin\ReservationController::class, 'lists']); //列表
    Route::get('/reservation/detail', [Admin\ReservationController::class, 'detail']); //详情
    Route::post('/reservation/add', [Admin\ReservationController::class, 'add']); //新增
    Route::post('/reservation/change', [Admin\ReservationController::class, 'change']); //修改
    Route::post('/reservation/del', [Admin\ReservationController::class, 'del']); //删除
    Route::post('/reservation/playAndCancel', [Admin\ReservationController::class, 'playAndCancel']); //推荐与取消推荐


    Route::get('/reservationOffline/getMakeQuantum', [Admin\ReservationOfflineController::class, 'getMakeQuantum']); //手动预约获取预约时间段
    Route::post('/reservationOffline/makeInfo', [Admin\ReservationOfflineController::class, 'makeInfo']); //手动预约时间段


    Route::get('/reservation/getReservationQr', [Admin\ReservationController::class, 'getReservationQr']); //出示用户领取、归还钥匙二维码(动态二维码) （node 7 适用）  有效期3分钟


    //特殊排班时间
    Route::get('/reservationSpecialSchedule/list', [Admin\ReservationSpecialScheduleController::class, 'lists']); //特殊时间列表
    Route::post('/reservationSpecialSchedule/add', [Admin\ReservationSpecialScheduleController::class, 'add']); //特殊时间新增
    Route::post('/reservationSpecialSchedule/change', [Admin\ReservationSpecialScheduleController::class, 'change']); //特殊时间编辑
    Route::get('/reservationSpecialSchedule/detail', [Admin\ReservationSpecialScheduleController::class, 'detail']); //特殊时间详情
    Route::post('/reservationSpecialSchedule/del', [Admin\ReservationSpecialScheduleController::class, 'del']); //特殊时间删除


    Route::get('/reservationSeat/scheduleList', [Admin\ReservationSeatController::class, 'scheduleList']); //获取排版信息
    Route::get('/reservationSeat/list', [Admin\ReservationSeatController::class, 'lists']); //座位预览
    Route::get('/reservationSeat/geSeatListBytStatus', [Admin\ReservationSeatController::class, 'geSeatListBytStatus']); //根据状态获取格子列表 (只能用于 node 为 7 的情况)
    Route::post('/reservationSeat/add', [Admin\ReservationSeatController::class, 'add']); //座位新增
    Route::post('/reservationSeat/playAndCancel', [Admin\ReservationSeatController::class, 'playAndCancel']); //座位发布与撤销
    Route::post('/reservationSeat/del', [Admin\ReservationSeatController::class, 'del']); //座位删除
    Route::post('/reservationSeat/downloadQr', [Admin\ReservationSeatController::class, 'downloadQr']); //下载座位二维码

    Route::get('/reservationApply/list', [Admin\ReservationApplyController::class, 'lists']); //预约申请列表
    Route::get('/reservationApply/reservationApplyHistory', [Admin\ReservationApplyController::class, 'reservationApplyHistory']); //获取用户对于某个预约的预约记录
    Route::post('/reservationApply/agreeAndRefused', [Admin\ReservationApplyController::class, 'agreeAndRefused']); //审核通过 和 拒绝
    Route::post('/reservationApply/violateAndCancel', [Admin\ReservationApplyController::class, 'violateAndCancel']); //预约违规 与 取消违规操作

    Route::post('/reservationApply/applyCancel', [Admin\ReservationApplyController::class, 'applyCancel']); //管理员取消预约
    Route::post('/reservationApply/applySignIn', [Admin\ReservationApplyController::class, 'applySignIn']); //管理员打卡签到\签退
    Route::post('/reservationApply/applySignOut', [Admin\ReservationApplyController::class, 'applySignOut']); // 管理员操作用户打卡签退(弃用，改为签到同一个接口)
    Route::post('/reservationApply/collectionAndReturn', [Admin\ReservationApplyController::class, 'collectionAndReturn']); //管理员操作领取和归还钥匙

    /**
     * 整个项目 数据分析
     */
    Route::get('/dataAnalysis/appletStatistics', [Admin\DataAnalysisController::class, 'appletStatistics']); // 小程序访问统计
    Route::get('/dataAnalysis/activityStatistics', [Admin\DataAnalysisController::class, 'activityStatistics']); // 活动参与人数统计
    Route::get('/dataAnalysis/recomAndSurveyStatistics', [Admin\DataAnalysisController::class, 'recomAndSurveyStatistics']); // 荐购、问卷统计

    Route::get('/dataAnalysis/accessStatistics', [Admin\DataAnalysisController::class, 'accessStatistics']); // 小程序访问总统计图


    Route::get('/dataAnalysis/authRegisterStatistics', [Admin\DataAnalysisController::class, 'authRegisterStatistics']); // 用户授权注册总统计图
    Route::get('/dataAnalysis/otherStatistics', [Admin\DataAnalysisController::class, 'otherStatistics']); // 其他数据统计
    Route::get('/dataAnalysis/activityTypeStatistics', [Admin\DataAnalysisController::class, 'activityTypeStatistics']); // 活动类型分布统计图
    Route::get('/dataAnalysis/activityApplyStatistics', [Admin\DataAnalysisController::class, 'activityApplyStatistics']); // 活动参与报名人数统计
    Route::get('/dataAnalysis/competiteActivityStatistics', [Admin\DataAnalysisController::class, 'competiteActivityStatistics']); // 征集活动作品大类型统计
    Route::get('/dataAnalysis/competiteActivityPersonStatistics', [Admin\DataAnalysisController::class, 'competiteActivityPersonStatistics']); // 征集活动投递人次
    Route::get('/dataAnalysis/databaseWorksStatistics', [Admin\DataAnalysisController::class, 'databaseWorksStatistics']); // 数据库作品统计
    Route::get('/dataAnalysis/userStatistics', [Admin\DataAnalysisController::class, 'userStatistics']); // 用户统计
    Route::get('/dataAnalysis/singleUserStatistics', [Admin\DataAnalysisController::class, 'singleUserStatistics']); // 单个用户统计







    /**
     * 在线办证配置
     */
    Route::get('/onlineRegistrationConfig/getOnlineRegistrationConfigParam', [Admin\OnlineRegistrationConfigController::class, 'getOnlineRegistrationConfigParam']); // 获取在线办证参数（获取已配置的）
    Route::get('/onlineRegistrationConfig/setOnlineRegistrationConfigParam', [Admin\OnlineRegistrationConfigController::class, 'setOnlineRegistrationConfigParam']); // 配置在线办证参数

    /**
     * 在线办证
     */
    Route::get('/onlineRegistration/getDynamicParam', [Admin\OnlineRegistrationController::class, 'getDynamicParam']); // 获取动态参数
    Route::post('/onlineRegistration/sendVerify', [Admin\OnlineRegistrationController::class, 'sendVerify']); // 发送验证码
    Route::get('/onlineRegistration/getCertificateType', [Admin\OnlineRegistrationController::class, 'getCertificateType']); // 获取证件类型
    Route::get('/onlineRegistration/getCardType', [Admin\OnlineRegistrationController::class, 'getCardType']); // 获取在线办证类型
    Route::get('/onlineRegistration/getFrontCardType', [Admin\OnlineRegistrationController::class, 'getFrontCardType']); // 获取在前台线办证类型
    Route::post('/onlineRegistration/registration', [Admin\OnlineRegistrationController::class, 'registration']); // 读者证-办证接口
    Route::get('/onlineRegistration/sponsorRegistrationList', [Admin\OnlineRegistrationController::class, 'sponsorRegistrationList']); //（发起方）办证统计列表 （前台在线办证列表）
    Route::get('/onlineRegistration/sponsorRegistrationStatistics', [Admin\OnlineRegistrationController::class, 'sponsorRegistrationStatistics']); //（发起方）办证统计数据  （前台在线办证列表）
    Route::get('/onlineRegistration/confirmPostageList', [Admin\OnlineRegistrationController::class, 'confirmPostageList']); //（确认方）办证统计列表
    Route::get('/onlineRegistration/confirmPostageStatistics', [Admin\OnlineRegistrationController::class, 'confirmPostageStatistics']); //（确认方） 邮费统计 金额及书籍本数 统计
    Route::post('/onlineRegistration/sponsorSettleState', [Admin\OnlineRegistrationController::class, 'sponsorSettleState']); //（发起方）标记办证状态为结算中
    Route::post('/onlineRegistration/confirmSettleState', [Admin\OnlineRegistrationController::class, 'confirmSettleState']); //（确认方） 标记邮费状态为已结算
    Route::post('/onlineRegistration/toReceive', [Admin\OnlineRegistrationController::class, 'toReceive']); // 领取读者证
    Route::get('/onlineRegistration/getOnlineParam', [Admin\OnlineRegistrationController::class, 'getOnlineParam']); // 获取办证参数
    Route::post('/onlineRegistration/refund', [Admin\OnlineRegistrationController::class, 'refund']); //  写入图书馆系统接口失败，办证失败，进行退款操作  （用于自动退款失败后手动退款）



    /**
     * 导出功能
     */
    Route::get('/activityApplyExport/activityApplyList', [Controllers\Export\ActivityApplyExport::class, 'activityApplyList']); // 活动申请列表导出
    Route::get('/activityApplyExport/activityApplyUser', [Controllers\Export\ActivityApplyExport::class, 'activityApplyUser']); // 活动人员列表导出
    Route::get('/activityApplyExport/activityApplyListAll', [Controllers\Export\ActivityApplyExport::class, 'activityApplyListAll']); // 批量活动申请列表导出
    Route::get('/activityApplyExport/activityApplyUserAll', [Controllers\Export\ActivityApplyExport::class, 'activityApplyUserAll']); // 批量活动人员列表导出


    Route::get('/reservationApplyExport/index', [Controllers\Export\ReservationApplyExport::class, 'index']); //  预约申请、预约人数列表导出


});
