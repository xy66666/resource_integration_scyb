<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Wechat;
use App\Http\Controllers\ScanQrController;
use App\Http\Controllers\ScoreRuleController;
use App\Http\Controllers\JwtController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//网站web端接口


Route::get('wechat/auth/index', [Wechat\WechatAuthController::class, 'indexInfo'])->withoutMiddleware(['invite', 'wechat_auth']); // 微信确认登陆后接受,获取信息  - 用户确认授权  不能用 post  和 any，否则前端即使用get，也会预请求，造成 code 重复使用
Route::get('wechat/auth/silentIndex', [Wechat\WechatAuthController::class, 'silentIndexInfo'])->withoutMiddleware(['invite', 'wechat_auth']); // 微信确认登陆后接受,获取信息 - 静默授权  不能用 post  和 any，否则前端即使用get，也会预请求，造成 code 重复使用
Route::get('wechat/auth/appletIndex', [Wechat\WechatAuthController::class, 'appletIndexInfo'])->withoutMiddleware(['invite', 'wechat_auth']); // 微信确认登陆后接受,获取信息 - 小程序授权   不能用 post  和 any，否则前端即使用get，也会预请求，造成 code 重复使用
Route::any('wechat/service/createParam', [Wechat\ServiceController::class, 'createParam'])->withoutMiddleware(['invite', 'wechat_auth']); // 获取微信分享需要参数
Route::get('/scoreRule/list', [ScoreRuleController::class, 'lists'])->withoutMiddleware(['jwt', 'wechat_auth']); //积分规则,因为微信也要使用，所以排除
#Route::get('/scoreRule/customScoreRule', [ScoreRuleController::class, 'customScoreRule'])->withoutMiddleware('jwt'); //获取自定义积分规则,因为微信也要使用，所以排除

Route::post('/getToken', [JwtController::class, 'getAuthToken'])->withoutMiddleware(['jwt', 'invite', 'wechat_auth']); //获取token,->withoutMiddleware([CheckJwtHeaderToken::class])排除这个中间件，因为在路由服务中间件中，自动运用了这个中间件

#Route::get('wechat/userAccess/limitAccess', [Wechat\UserAccessDataController::class, 'limitAccess'])->withoutMiddleware(['invite', 'wechat_auth']); // 获取活动访问人数（答题活动）

Route::get('wechat/index/checkInviteCode', [Wechat\IndexController::class, 'checkInviteCode'])->withoutMiddleware(['wechat_auth']); // 第一次验证邀请码是否正确


//不需要token
Route::prefix('wechat')->group(function () {

    Route::get('/index/getAddressOrLonLat', [Wechat\IndexController::class, 'getAddressOrLonLat']); // 根据地址转经纬度、或根据经纬度转换地址

    Route::get('/userProtocol/getUserProtocol', [Wechat\UserProtocolController::class, 'getUserProtocol']); // 用户协议

    Route::get('/index/otherAccressAdd', [Wechat\IndexController::class, 'otherAccressAdd']); // 记录其他数据访问量

    /**
     * 图书馆相关（扫码借）
     */
    Route::get('/libInfo/getBookInfoList', [Wechat\LibInfoController::class, 'getBookInfoList']); // 获取检索信息列表
    Route::get('/libInfo/getLibBookDetail', [Wechat\LibInfoController::class, 'getLibBookDetail']); // 获取馆藏书籍详


    //新闻管理
    Route::get('/news/typeList', [Wechat\NewsController::class, 'typeList']); //新闻类型
    Route::get('/news/list', [Wechat\NewsController::class, 'lists']); //新闻列表

    //数字资源管理
    Route::get('/digital/typeList', [Wechat\DigitalController::class, 'typeList']); //数字资源类型
    Route::get('/digital/list', [Wechat\DigitalController::class, 'lists']); //列表
    Route::post('/digital/addBrowseNum', [Wechat\DigitalController::class, 'addBrowseNum']); //数字资源添加浏览量




    /**
     * 活动管理
     */
    Route::get('/activity/getActivityCalendar', [Wechat\ActivityController::class, 'getActivityCalendar']); // 根据时间获取活动 展示活动日历
    Route::get('/activity/getActivityCalendarByTime', [Wechat\ActivityController::class, 'getActivityCalendarByTime']); // 根据时间获取活动 展示活动日历
    Route::get('/activity/getHotActivity', [Wechat\ActivityController::class, 'getHotActivity']); // 获取热门活动
    Route::get('/activity/getActivityRecom', [Wechat\ActivityController::class, 'getActivityRecom']); // 获取推荐，每个类型推荐
    Route::get('/activity/typeList', [Wechat\ActivityController::class, 'typeList']); // 活动类型列表
    Route::get('/activity/list', [Wechat\ActivityController::class, 'lists']); // 活动列表


    /**
     * 预约
     */
    Route::get('/reservation/reservationNodeList', [Wechat\ReservationController::class, 'reservationNodeList']); // 预约种类列表
    Route::get('/reservation/reservationHallList', [Wechat\ReservationController::class, 'reservationHallList']); // 列表（每个种类都返回几个）
    Route::get('/reservation/list', [Wechat\ReservationController::class, 'lists']); //  预约列表
    //Route::get('/reservation/detail', [Wechat\ReservationController::class, 'detail']); //  预约详情


    /**
     * 线上办证
     */
    Route::post('/onlineRegistration/sendVerify', [Wechat\OnlineRegistrationController::class, 'sendVerify']); // 发送验证码
    Route::get('/onlineRegistration/getOnlineParam', [Wechat\OnlineRegistrationController::class, 'getOnlineParam']); // 获取下拉选项参数
    Route::get('/onlineRegistration/getDynamicParam', [Wechat\OnlineRegistrationController::class, 'getDynamicParam']); // 获取动态参数

    Route::get('/onlineRegistration/getCardType', [Wechat\OnlineRegistrationController::class, 'getCardType']); //获取可办证的类型


});

//token必选
Route::middleware(['check.wechat.token'])->prefix('wechat')->group(function () {

    /**
     * 个人中心
     */
    Route::get('/userInfo/index', [Wechat\UserInfoController::class, 'index']); // 个人中心首页


    //扫描二维码
    Route::post('/scanQr/scanAffirm', [ScanQrController::class, 'scanAffirm']); //扫描二维码

    //扫描二维码
    Route::post('/userInfo/wechatInfoChange', [Wechat\UserInfoController::class, 'wechatInfoChange']); //修改头像 和 昵称

    //用户留言管理
    Route::post('/feedback/add', [Wechat\FeedbackController::class, 'addFeedback']); //用户留言


    /**
     * 活动管理
     */
    Route::get('/activity/myActList', [Wechat\ActivityController::class, 'myActList']); // 我的活动列表
    Route::post('/activity/apply', [Wechat\ActivityController::class, 'apply']); // 活动报名
    Route::post('/activity/cancel', [Wechat\ActivityController::class, 'cancel']); // 取消活动报名
    Route::get('/activity/getApplyQr', [Wechat\ActivityController::class, 'getApplyQr']); // 出示二维码



    /**
     * 用户地址管理
     */
    Route::get('/userAddress/list', [Wechat\UserAddressController::class, 'lists']); // 列表
    Route::get('/userAddress/detail', [Wechat\UserAddressController::class, 'detail']); // 详情
    Route::post('/userAddress/add', [Wechat\UserAddressController::class, 'add']); // 新增
    Route::post('/userAddress/change', [Wechat\UserAddressController::class, 'change']); // 编辑
    Route::post('/userAddress/del', [Wechat\UserAddressController::class, 'del']); // 删除
    Route::post('/userAddress/switchoverDefault', [Wechat\UserAddressController::class, 'switchoverDefault']); // 修改默认状态


    /**
     * 系统消息
     */
    // Route::get('/systemInfo/list', [Wechat\SystemInfoController::class, 'lists']); // 列表
    // Route::get('/systemInfo/detail', [Wechat\SystemInfoController::class, 'detail']); // 详情

    /*用户获取消息列表*/
    Route::get('/system/systemList', [Wechat\SystemInfoController::class, 'systemList']); // 列表
    Route::get('/system/systemMessage', [Wechat\SystemInfoController::class, 'systemMessage']); //获取用户分类消息列表


    /**
     * 个人中心
     */
    Route::get('/userInfo/scoreList', [Wechat\UserInfoController::class, 'scoreList']); // 获取用户积分明细

    /**
     * 预约
     */
    Route::get('/reservation/seatList', [Wechat\ReservationController::class, 'seatList']); //  座位列表
    Route::post('/reservation/makeInfo', [Wechat\ReservationController::class, 'makeInfo']); // 文化配送 预约时间段
    Route::post('/reservation/cancelMake', [Wechat\ReservationController::class, 'cancelMake']); // 用户取消预约
    Route::get('/reservation/myMakeList', [Wechat\ReservationController::class, 'myMakeList']); //  我的预约列表
    Route::get('/reservation/getApplyQr', [Wechat\ReservationController::class, 'getApplyQr']); //  出示二维码


    /**
     * 年度账单
     */
    Route::get('/index/getYearBill', [Wechat\IndexController::class, 'getYearBill']); // 年度账单

    /**
     * 新闻
     */
    Route::post('/news/collectAndCancel', [Wechat\NewsController::class, 'collectAndCancel']); // 新闻收藏与取消收藏
    Route::get('/news/collectList', [Wechat\NewsController::class, 'collectList']); // 收藏新闻列表

    /**
     * 在线办证
     */
    Route::post('/onlineRegistration/registration', [Wechat\OnlineRegistrationController::class, 'registration']); // 办证
    Route::get('/onlineRegistration/historyList', [Wechat\OnlineRegistrationController::class, 'historyList']); // 办证历史记录
    Route::get('/onlineRegistration/detail', [Wechat\OnlineRegistrationController::class, 'detail']); // 办证详情信息



});

//token可选
Route::middleware(['check.may.wechat.token'])->prefix('wechat')->group(function () {

    //在线办证（行为分析时需要token，所以加在这里）
    Route::get('/onlineRegistration/getCertificateType', [Wechat\OnlineRegistrationController::class, 'getCertificateType']); //获取证件类型
    //馆藏检索（行为分析时需要token，所以加在这里）
    Route::get('/libInfo/hotLibSearchList', [Wechat\LibInfoController::class, 'hotLibSearchList']); // 馆藏检索热词列表


    //首页接口
    Route::get('/index/index', [Wechat\IndexController::class, 'index']); //首页

    /**
     * 活动管理
     */
    Route::get('/activity/detail', [Wechat\ActivityController::class, 'detail']); // 活动详情(我的活动详情)


    /**
     * 预约
     */
    Route::get('/reservation/detail', [Wechat\ReservationController::class, 'detail']); //  预约详情

    /**
     * 新闻
     */
    Route::get('/news/detail', [Wechat\NewsController::class, 'detail']); //新闻详情



    /**
     *楼层展览
     */
    Route::get('/index/getFloorExhibition', [Wechat\IndexController::class, 'getFloorExhibition']); // 楼层展览

});


//必须绑定读者证
Route::middleware(['check.bind.reader.token'])->prefix('wechat')->group(function () {


    Route::post('/libInfo/unBindReaderId', [Wechat\LibInfoController::class, 'unBindReaderId']); // 解除绑定读者证
    Route::post('/libInfo/changePwd', [Wechat\LibInfoController::class, 'changePwd']); // 修改读者证密码
    Route::post('/libInfo/cardDelay', [Wechat\LibInfoController::class, 'cardDelay']); // 读者证延期
    Route::post('/libInfo/lost', [Wechat\LibInfoController::class, 'lost']); // 读者证挂失操作
    Route::post('/libInfo/lostRecover', [Wechat\LibInfoController::class, 'lostRecover']); // 读者证恢复

    Route::get('/libInfo/getBorrowList', [Wechat\LibInfoController::class, 'getBorrowList']); // 获取借阅记录列表
    Route::post('/libInfo/renew', [Wechat\LibInfoController::class, 'renewBook']); // 在线续借

    /**
     * 积分签到
     */
    Route::get('/signScore/signScoreInfo', [Wechat\SignScoreController::class, 'signScoreInfo']); // 获取积分签到信息
    Route::post('/signScore/sign', [Wechat\SignScoreController::class, 'sign']); // 用户打卡签到



});

//token必选
Route::middleware(['check.wechat.token', 'limit.access.data'])->prefix('wechat')->group(function () {

    Route::post('/libInfo/bingReaderId', [Wechat\LibInfoController::class, 'bingReaderId']); // 绑定读者证  或 密码错误重新登录
});
