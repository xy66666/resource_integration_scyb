<?php

// 应用公共文件

/**
 * 获得guid
 **/

use Illuminate\Support\Facades\Log;

if (!function_exists('get_guid')) {
    function get_guid()
    {
        $guid = md5(time() . config('other.md5_key') . get_rnd_string(6));
        return $guid;
    }
}

/**
 * 随机字符串
 **/
if (!function_exists('get_rnd_string')) {
    function get_rnd_string($length)
    {
        $pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
        $key = '';
        for ($i = 0; $i < $length; $i++) {
            @$key .= $pattern[mt_rand(0, 35)];
        }
        return $key;
    }
}

/**
 * 随机数字
 **/
if (!function_exists('get_rnd_number')) {
    function get_rnd_number($length)
    {
        $pattern = '1234567890';
        $key = '';
        for ($i = 0; $i < $length; $i++) {
            @$key .= $pattern[mt_rand(0, 9)];
        }
        return $key;
    }
}

/**
 * 随机字符串全大写
 **/
if (!function_exists('get_rnd_upper_string')) {
    function get_rnd_upper_string($length)
    {
        $pattern = '1234567890QWERTYUIOPLKJHGFDSAZXCVBNM';
        $key = '';
        for ($i = 0; $i < $length; $i++) {
            $key .= $pattern[mt_rand(0, 35)];
        }
        return $key;
    }
}

/**
 * 根据身份证号码获取用户年龄
 * @param $id_card 身份证号码
 */
if (!function_exists('get_user_age_by_id_card')) {
    function get_user_age_by_id_card($id_card)
    {
        //过了这年的生日才算多了1周岁
        if (empty($id_card)) {
            return '';
        }
        $date = strtotime(substr($id_card, 6, 8));
        //获得出生年月日的时间戳
        $today = strtotime('today');
        //获得今日的时间戳 111cn.net
        $diff = floor(($today - $date) / 86400 / 365);
        //得到两个日期相差的大体年数

        //strtotime加上这个年数后得到那日的时间戳后与今日的时间戳相比
        $age = strtotime(substr($id_card, 6, 8) . ' +' . $diff . 'years') > $today ? ($diff + 1) : $diff;
        return $age;
    }
}
/**
 * 根据身份证号码获取用户生日
 * @param $id_card 身份证号码
 */
if (!function_exists('get_user_birthday_by_id_card')) {
    function get_user_birthday_by_id_card($id_card)
    {
        if (empty($id_card)) {
            return '';
        }
        $date = strtotime(substr($id_card, 6, 8));
        return date('Y-m-d', $date);
    }
}
/**
 * 根据身份证号码获取用户性别
 * @param $id_card 身份证号码
 */
if (!function_exists('get_user_sex_by_id_card')) {
    function get_user_sex_by_id_card($id_card)
    {
        if (empty($id_card)) {
            return null;
        }
        $sexint = (int)substr($id_card, 16, 1);

        //return $sexint % 2 === 0 ? '女' : '男';
        return $sexint % 2 === 0 ? '2' : '1';
    }
}

/**
 * 获取两个日期之间的所有日期 2019-08-02至2019-08-16
 * @param $start_time 开始时间
 * @param $end_time 结束时间
 **/
if (!function_exists('pr_dates')) {
    function pr_dates($start_time, $end_time)
    {
        $dt_start = strtotime($start_time);
        $dt_end = strtotime($end_time);
        $value = [];
        while ($dt_start <= $dt_end) {
            $value[] = date('Y-m-d', $dt_start);
            $dt_start = strtotime('+1 day', $dt_start);
        }
        return $value;
    }
}


/**
 * 获取一天的所有小时
 **/
if (!function_exists('pr_day_hour')) {
    function pr_day_hour()
    {
        for ($i = 0; $i < 24; $i++) {
            $value[] = sprintf("%02s", $i);
        }
        return $value;
    }
}


/**
 * 根据年获取所有的月份
 **/
if (!function_exists('get_month_by_year')) {
    function get_month_by_year($year)
    {
        //判断是否是当前年
        if ($year >= date('Y')) {
            $month = date("n");
        } else {
            $month = 12;
        }
        $month_arr = [];
        for ($i = 1; $i <= $month; $i++) {
            if ($i < 10) {
                $month_arr[] = $year . '-0' . $i;
            } else {
                $month_arr[] = $year . '-' . $i;
            }
        }
        return $month_arr;
    }
}


/**
 * 获取未来一个星期的日期时间 (从今天开始的一周的时间)
 * $week 是否返回星期
 */
if (!function_exists('get_continuous_week_time')) {
    function get_continuous_week_time($week = false, $display_day = 7)
    {
        for ($i = 0; $i < $display_day; $i++) {
            $dateArray[$i] = date('Y-m-d', strtotime(date('Y-m-d') . '+' . $i . 'day'));
        };
        if ($week) {
            return get_date($dateArray);
        }
        return $dateArray;
    }
}
/**
 * 返回输入日期数组对应的星期和日期
 * @param $dateArray 需要的日期数组，如未来七天的日期
 **/
if (!function_exists('get_date')) {
    function get_date($dateArray)
    {
        $b = [];
        foreach ($dateArray as $key => $value) {
            $b[] = ['id' => $key, 'date' => $value];
        };
        foreach ($b as $k => $v) {
            $week = get_week_by_date($v['date']);
            $b[$k]['week'] = $week[1];
            $b[$k]['weeks'] = $week[0] ? (int)$week[0] : 7; //星期日为 7
            $b[$k]['date'] = $v['date'];
        }
        return $b;
    }
}
/**
 * 返回输入日期星期几
 * @param $date 日期
 * @param $prefix 前缀   周  星期
 **/
if (!function_exists('get_week_by_date')) {
    function get_week_by_date($date, $prefix = '星期')
    {
        $date_str = date('Y-m-d', strtotime($date));
        $arr = explode("-", $date_str);
        $year = $arr[0];
        $month = sprintf('%02d', $arr[1]);
        $day = sprintf('%02d', $arr[2]);
        $hour = $minute = $second = 0;
        $strap = mktime($hour, $minute, $second, $month, $day, $year);
        $number_wk = date("w", $strap);
        $weekArr = [$prefix . "日", $prefix . "一", $prefix . "二", $prefix . "三", $prefix . "四", $prefix . "五", $prefix . "六"];
        return [$number_wk, $weekArr[$number_wk]];
    }
}

/**
 * 数字转换为星期几
 * @param 数字 1-7日期
 * @param $prefix 前缀   周  星期
 **/
if (!function_exists('number_to_week')) {
    function number_to_week($number, $prefix = '星期')
    {
        if (empty($number)) return $prefix . '日'; // 0 也返回为星期日
        $weekArr = ['1' => $prefix . "一", $prefix . "二", $prefix . "三", $prefix . "四", $prefix . "五", $prefix . "六", $prefix . "日"];
        return $weekArr[$number];
    }
}


/**
 * 获取后面几天的日期
 */
if (!function_exists('get_behind_date')) {
    function get_behind_date($time = '', $day = 7, $format = 'Y-m-d')
    {
        $time = $time != '' ? $time : time();

        $date = [];
        for ($i = 0; $i < $day; $i++) {
            $date[$i] = date($format, strtotime('+' . $i . ' days', $time));
        }
        return $date;
    }
}

/**
 * 获取未来一个星期的日期时间
 * $week 是否返回星期
 */
if (!function_exists('get_week_time')) {
    function get_week_time($week = false)
    {
        //for($i=1;$i<8;$i++){
        for ($i = 0; $i < 7; $i++) {
            $dateArray[$i] = date('Y-m-d', strtotime(date('Y-m-d') . '+' . $i . 'day'));
        };
        if ($week) {
            return get_date($dateArray);
        }
        return $dateArray;
    }
}


/**
 * 验证电话号码的正确性
 */
if (!function_exists('verify_tel')) {
    function verify_tel($tel)
    {
        if (!is_numeric($tel)) {
            return false;
        }
        $search = '/^(1([3456789][0-9]{9}))$/';
        if (preg_match($search, $tel)) {
            return true;
        } else {
            return false;
        }
    }
}

//验证电话号码或座机是否符合要求
if (!function_exists('verify_tel_and_phone')) {
    function verify_tel_and_phone($phone)
    {
        // 验证联系电话
        $isMob = "/^1[3456789]{1}\d{9}$/";

        $isTel = "/^([0-9]{3,4}-?)?[0-9]{7,8}$/";

        if (!preg_match($isMob, $phone) && !preg_match($isTel, $phone)) {
            return false;
        } else {
            return true;
        }
    }
}

/**
 * 判断是否是微信环境
 */
if (!function_exists('is_wechat_env')) {
    function is_wechat_env()
    {
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false) {
            return true;
        }
        return false;
    }
}


/**
 * @param $arr [要排序的数组]  根据单个字段排序
 * @param $field 需要排序的字段
 * @param $condition [要排序的规则, SORT_NUMERIC 数字排序, SORT_DESC 从大到小, SORT_STRING 字符串排序 ,SORT_ASC 从小到大]
 * @return bool|mixed
 * 对二维数组的某个字段进行排序
 */
if (!function_exists('sort_arr_by_one_field')) {
    function sort_arr_by_one_field($arr, $field, $condition = [SORT_NUMERIC, SORT_ASC])
    {
        $temp = array();
        $i = 0;

        foreach ($arr as $k => $a) {
            $temp[$i][] = $a[$field];
        }
        $temp[$i + 1] = $arr; //把总的数据，变换为数组的第二个数据

        array_multisort($temp[0], $condition[0], $condition[1], $temp[1]);
        return array_pop($temp);
    }
}


//对象转换为数组
if (!function_exists('object_array')) {
    function object_array($array)
    {
        if (is_object($array)) {
            $array = (array)$array;
        }
        if (is_array($array)) {
            foreach ($array as $key => $value) {
                $array[$key] = object_array($value);
            }
        }
        return $array;
    }
}


/**
 * 判断isbn号的正确性
 * 1.如果为10位，第一位必须为'7',
 * 2.如果为13位，前4位必须为'9787'或者'9797'
 * 3.除最后一位外，其余必须是纯数字
 **/

if (!function_exists('check_isbn')) {
    function check_isbn($isbn)
    {
        $isbn = str_replace('-', '', $isbn);
        $isbn_num = substr($isbn, 0, -1);
        if (!is_numeric($isbn_num)) {
            return false;
        }
        if (strlen($isbn) == '10') {
            if (substr($isbn, 0, 1) != '7') {
                return false;
            }
        } elseif (strlen($isbn) == '13') {
            $isbns = substr($isbn, 0, 4);
            if ($isbns !== '9787' && $isbns !== '9797') {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }
}

/**
 * 判断索书号规则
 */
if (!function_exists('check_callno')) {
    function check_callno($callno)
    {
        $data = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'X', 'Z'];
        $callno = substr($callno, 0, 1);
        if (!in_array($callno, $data)) {
            return false;
        }
        return true;
    }
}

//是否包含空格
if (!function_exists('is_include_blank')) {
    function is_include_blank($str)
    {
        if (preg_match("/ /", $str)) {
            return true; //包含空格
        }
        return false;
    }
}
//是否有中文
if (!function_exists('is_include_chinese')) {
    function is_include_chinese($data)
    {
        if (preg_match("/[\x7f-\xff]/", $data)) {
            return true; //包含中文
        }
        return false;
    }
}
//是否包含特殊字符
if (!function_exists('is_include_special')) {
    function is_include_special($str)
    {
        if (preg_match('/^[ _:：,，.。…\/、~`＠＃￥％＆×＋｜｛｝＝－＊＾＄～｀!@#$%^&*()—=（）！￥{}【】\[\]\|\"\'’‘“”；;《》<>\?\？\·]$/u', $str)) {
            return true; //包含空特殊字符
        }
        return false;
    }
}

//去掉特殊字符
if (!function_exists('replace_special_char')) {
    function replace_special_char($str)
    {
        $regex = "/\/|\～|\，|\。|\！|\？|\“|\”|\【|\】|\『|\』|\：|\；|\《|\》|\’|\‘|\ |\·|\~|\!|\@|\#|\\$|\%|\^|\&|\*|\(|\)|\_|\+|\{|\}|\:|\<|\>|\?|\[|\]|\,|\.|\/|\;|\'|\`|\-|\=|\\\|\|/";
        return preg_replace($regex, "", $str);
    }
}

//判断是否存在  emoji；
if (!function_exists('have_special_char')) {
    function have_special_char($str)
    {
        $length = mb_strlen($str);
        $array = [];
        for ($i = 0; $i < $length; $i++) {
            $array[] = mb_substr($str, $i, 1, 'utf-8');
            if (strlen($array[$i]) >= 4) {
                return true;
            }
        }
        return false;
    }
}


/**
 * 判断是否为合法的身份证号码
 * @param $mobile
 * @return int
 */
if (!function_exists('is_legal_no')) {
    function is_legal_no($vStr)
    {
        $vCity = array(
            '11',
            '12',
            '13',
            '14',
            '15',
            '21',
            '22',
            '23',
            '31',
            '32',
            '33',
            '34',
            '35',
            '36',
            '37',
            '41',
            '42',
            '43',
            '44',
            '45',
            '46',
            '50',
            '51',
            '52',
            '53',
            '54',
            '61',
            '62',
            '63',
            '64',
            '65',
            '71',
            '81',
            '82',
            '91'
        );
        if (!preg_match('/^([\d]{17}[xX\d]|[\d]{15})$/', $vStr)) {
            return false;
        }
        if (!in_array(substr($vStr, 0, 2), $vCity)) {
            return false;
        }
        $vStr = preg_replace('/[xX]$/i', 'a', $vStr);
        $vLength = strlen($vStr);
        if ($vLength == 18) {
            $vBirthday = substr($vStr, 6, 4) . '-' . substr($vStr, 10, 2) . '-' . substr($vStr, 12, 2);
        } else {
            $vBirthday = '19' . substr($vStr, 6, 2) . '-' . substr($vStr, 8, 2) . '-' . substr($vStr, 10, 2);
        }
        if (date('Y-m-d', strtotime($vBirthday)) != $vBirthday) {
            return false;
        }
        if ($vLength == 18) {
            $vSum = 0;
            for ($i = 17; $i >= 0; $i--) {
                $vSubStr = substr($vStr, 17 - $i, 1);
                $vSum += (pow(2, $i) % 11) * (($vSubStr == 'a') ? 10 : intval($vSubStr, 11));
            }
            if ($vSum % 11 != 1) {
                return false;
            }
        }
        return true;
    }
}


//计算两个时间的差值
if (!function_exists('time_diff')) {
    function time_diff($begin_time, $end_time)
    {
        if ($begin_time < $end_time) {
            $starttime = $begin_time;
            $endtime = $end_time;
        } else {
            $starttime = $end_time;
            $endtime = $begin_time;
        }

        if (strtotime($starttime) !== false) $starttime = strtotime($starttime); //如果不是时间戳格式，需要转换
        if (strtotime($endtime) !== false) $endtime = strtotime($endtime); //如果不是时间戳格式，需要转换

        $timediff = $endtime - $starttime;
        $days = intval($timediff / 86400);
        $remain = $timediff % 86400;
        $hours = intval($remain / 3600);
        $remain = $remain % 3600;
        $mins = intval($remain / 60);
        $secs = $remain % 60;
        $res = array("day" => $days, "hour" => $hours, "min" => $mins, "sec" => $secs);
        return $res;
    }
}
//计算两个时间的天数
if (!function_exists('time_diff_day')) {
    function time_diff_day($begin_time, $end_time)
    {
        if (strtotime($begin_time) !== false) $begin_time = strtotime($begin_time); //如果不是时间戳格式，需要转换
        if (strtotime($end_time) !== false) $end_time = strtotime($end_time); //如果不是时间戳格式，需要转换

        if ($begin_time < $end_time) {
            $starttime = $begin_time;
            $endtime = $end_time;
        } else {
            $starttime = $end_time;
            $endtime = $begin_time;
        }
        $timediff = $endtime - $starttime;
        $days = intval($timediff / 86400);
        return $days + 1;
    }
}


/**
 * 根据年月获取开始时间和结束时间
 * @param $month
 * @param $year
 * @return array
 */
if (!function_exists('get_start_end_time_by_year_month')) {
    function get_start_end_time_by_year_month($month, $year)
    {
        //拼接查询时间
        if (empty($month)) {
            //查询是否是当前
            $month = $year == date('Y') || $year > date('Y') ? date('m') : 12; //查询的年份大于当前年的情况，默认是当前年
            $start_time = $year . '-01-01';
            if ($year >= date('Y') && ($month == date('m') || $month == date('n'))) {
                $day = date('d');
            } else {
                $day = get_day_by_year_month($year . '-' . $month);
            }
            $end_time = $year . '-' . $month . '-' . $day;
        } else {
            $year = $year > date('Y') ? date('Y') : $year; //判断年不存在的问题
            if ($year >= date('Y') && ($month == date('m') || $month == date('n'))) {
                $day = date('d');
            } else {
                $day = get_day_by_year_month($year . '-' . $month);
            }
            $start_time = $year . '-' . $month . '-01';
            $end_time = $year . '-' . $month . '-' . $day;
        }
        return array($start_time, $end_time);
    }
}


//根据图片链接获取图片名
if (!function_exists('get_img_name_by_url')) {
    function get_img_name_by_url($url)
    {
        if (empty($url)) return false;
        $img_arr = explode('/', $url);
        $img_name_arr = explode('.', end($img_arr));
        return $img_name_arr[0];
    }
}
/**
 * 生成随机订单号 (18)
 */
if (!function_exists('get_order_id')) {
    function get_order_id()
    {
        $str = get_rnd_number(6);
        //日期时间加上6位随机数
        $orderId = date("ymdHis") . $str;
        return $orderId;
    }
}

/**
 * 根据年月获取当月天数
 * @param $date
 * @param $rtype 1天数 2具体日期数组
 * @return
 */
if (!function_exists('get_day_by_year_month')) {
    function get_day_by_year_month($date, $rtype = '1')
    {
        $tem = strpos('/', $date) !== false ? explode('/', $date) : explode('-', $date); //切割日期 得到年份和月份
        $year = $tem['0'];
        $month = $tem['1'];
        if (in_array($month, array(1, 3, 5, 7, 8, '01', '03', '05', '07', '08', 10, 12))) {
            // $text = $year.'年的'.$month.'月有31天';
            $text = '31';
        } elseif ($month == 2) {
            if ($year % 400 == 0 || ($year % 4 == 0 && $year % 100 !== 0))    //判断是否是闰年
            {
                // $text = $year.'年的'.$month.'月有29天';
                $text = '29';
            } else {
                // $text = $year.'年的'.$month.'月有28天';
                $text = '28';
            }
        } else {
            // $text = $year.'年的'.$month.'月有30天';
            $text = '30';
        }
        if ($rtype == '2') {
            for ($i = 1; $i <= $text; $i++) {
                $r[] = $year . "-" . $month . "-" . $i;
            }
        } else {
            $r = $text;
        }
        return $r;
    }
}


/**
 * 获取本周开始时间和结束时间
 * @param $date 日期
 **/
if (!function_exists('get_this_week_time')) {
    function get_this_week_time($date = null)
    {
        //当前日期
        $sdefaultDate = $date ? $date : date("Y-m-d");
        //$first =1 表示每周星期一为开始日期 0表示每周日为开始日期
        $first = 1;
        //获取当前周的第几天 周日是 0 周一到周六是 1 - 6
        $w = date('w', strtotime($sdefaultDate));
        //获取本周开始日期，如果$w是0，则表示周日，减去 6 天
        $week_start = date('Y-m-d 00:00:00', strtotime("$sdefaultDate -" . ($w ? $w - $first : 6) . ' days'));
        //本周结束日期
        $week_end = date('Y-m-d 23:59:59', strtotime("$week_start +6 days"));

        return [$week_start, $week_end];
    }
}

/**
 * 获取本月开始时间和结束时间
 * @param $date 日期
 **/
if (!function_exists('get_month_time')) {
    function get_month_time($date = null)
    {
        $date = date("Y-m-d");
        // 本月第一天
        $month_start = date('Y-m-01 00:00:00', strtotime($date));

        // 本月最后一天
        $month_end = date('Y-m-d 23:59:59', strtotime("$month_start +1 month -1 day"));

        return [$month_start, $month_end];
    }
}


/**
 * 取汉字的第一个字的首字母
 * @param type $str
 * @return string|null
 */
if (!function_exists('get_first_charter')) {
    function get_first_charter($str)
    {
        if ($str == '重庆' || $str == '重庆市') {
            return 'C';
        }
        if (empty($str) || is_numeric(substr($str, 0, 1))) {
            return 'X';
        }
        $fchar = ord($str[0]);

        if ($fchar >= ord('A') && $fchar <= ord('z')) {
            return strtoupper($str[0]);
        }
        /*   $s1=iconv('UTF-8','gb2312',$str);
           $s2=iconv('gb2312','UTF-8',$s1);*/
        $s1 = iconv('UTF-8', 'GBK', $str);
        $s2 = iconv('GBK', 'UTF-8', $s1);
        $s = $s2 == $str ? $s1 : $str;

        $asc = ord($s[0]) * 256 + ord($s[1]) - 65536;

        if ($asc >= -20319 && $asc <= -20284) {
            return 'A';
        }
        if ($asc >= -20283 && $asc <= -19776) {
            return 'B';
        }
        if ($asc >= -19775 && $asc <= -19219) {
            return 'C';
        }
        if ($asc >= -19218 && $asc <= -18711) {
            return 'D';
        }
        if ($asc >= -18710 && $asc <= -18527) {
            return 'E';
        }
        if ($asc >= -18526 && $asc <= -18240) {
            return 'F';
        }
        if ($asc >= -18239 && $asc <= -17923) {
            return 'G';
        }
        if ($asc >= -17922 && $asc <= -17418) {
            return 'H';
        }
        if ($asc >= -17417 && $asc <= -16475) {
            return 'J';
        }
        if ($asc >= -16474 && $asc <= -16213) {
            return 'K';
        }
        if ($asc >= -16212 && $asc <= -15641) {
            return 'L';
        }
        if ($asc >= -15640 && $asc <= -15166) {
            return 'M';
        }
        if ($asc >= -15165 && $asc <= -14923) {
            return 'N';
        }
        if ($asc >= -14922 && $asc <= -14915) {
            return 'O';
        }
        if ($asc >= -14914 && $asc <= -14631) {
            return 'P';
        }
        if ($asc >= -14630 && $asc <= -14150) {
            return 'Q';
        }
        if ($asc >= -14149 && $asc <= -14091) {
            return 'R';
        }
        if ($asc >= -14090 && $asc <= -13319) {
            return 'S';
        }
        if ($asc >= -13318 && $asc <= -12839) {
            return 'T';
        }
        if ($asc >= -12838 && $asc <= -12557) {
            return 'W';
        }
        if ($asc >= -12556 && $asc <= -11848) {
            return 'X';
        }
        if ($asc >= -11847 && $asc <= -11056) {
            return 'Y';
        }
        if ($asc >= -11055 && $asc <= -10247) {
            return 'Z';
        }
        //下面为自己补充内容
        if ($asc == -9767 || $asc == -4637) {
            return 'D';
        }
        if ($asc == -6928) {
            return 'L';
        }
        if ($asc == -6745) {
            return 'P';
        }
        if ($asc == -9743) {
            return 'B';
        }
        if ($asc == -7182) {
            return 'L';
        }
        if ($asc == -7703) {
            return 'Q';
        }
        if ($asc == -7189) {
            return 'W';
        }
        if ($asc == -7753) {
            return 'D';
        }
        if ($asc == -7725) {
            return 'S';
        }
        if ($asc == -5944) {
            return 'Z';
        }
        if ($asc == -7754) {
            return 'X';
        }
        if ($asc == -7760) {
            return 'L';
        }
        if ($asc == -9509) {
            return 'P';
        }
        if ($asc == -6987) {
            return 'L';
        }
        if ($asc == -3141) {
            return 'L';
        }
        if ($asc == -6973) {
            return 'Z';
        }
        if ($asc == -8776) {
            return 'G';
        }
        if ($asc == -6996 || $asc == -7766) {
            return 'T';
        }
        return 'X'; //默认返回X
    }
}

# 发起网络请求
if (!function_exists('request_url')) {
    function request_url($url, $params = null, $timeout = 10)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);


        //获取授权失败增加 2021-06-18
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        if (!empty($params)) {
            if (is_array($params)) {
                $params = http_build_query($params);
            } //如果传的是数组，就要进行对参数进行 &拼接
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        }
        $body = curl_exec($ch);

        curl_close($ch);

        return $body;
    }
}

/**
 * 把文件夹下的内容放到指定文件夹下
 * @param $temp_file_path 临时文件路径
 * @param $move_file_path 移动后文件路径
 */
if (!function_exists('move_dir_file')) {
    function move_dir_file($temp_file_path, $move_file_path)
    {
        if (file_exists($temp_file_path)) {
            if (!file_exists($move_file_path)) {
                mkdir($move_file_path, 0777, true);
            }

            $file = scandir($temp_file_path);
            //文件里包括"."和".."，两个目录
            for ($i = 2, $count = count($file); $i < $count; $i++) {
                $file_url = $temp_file_path . $file[$i];
                if (!is_dir($file_url)) {
                    rename($file_url, $move_file_path . $file[$i]);
                }
            }
        }
    }
}

/**
 * 通过图片、音频链接地址，下载图片到本地指定位置(视频未测试)
 * @param $url 网络图片地址   //http://img3m4.ddimg.cn/22/6/26910994-3_u_6.jpg
 * @param $local_dir 本地存放地址
 * @param $new_name  新名称
 * @return mixed
 */
if (!function_exists('file_to_local')) {
    function file_to_local($url, $local_dir, $new_name = null)
    {
        if (!@fopen($url, "r")) {
            return ''; //图片或音频文件不存在直接返回false
        }
        if (empty($local_dir)) {
            $local_dir = public_path('uploads') . "/images/" . date("Y-m-d"); //如果没路径，默认放在这个路径下
        }
        // 分割获取后缀
        $arr = explode('.', $url);
        // 初始化一个新的会话，返回一个cURL句柄，供curl_setopt(), curl_exec()和curl_close() 函数使用。
        $curl = curl_init($url);
        // 组装文件名
        $filename = date("Ymdhis") . '.' . end($arr);
        // 为给定的cURL会话句柄设置一个选项。
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        //获取授权失败增加 2021-06-18
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);

        // 抓取url并把它传给浏览器
        $imageData = curl_exec($curl);

        // 关闭curl资源，并关闭系统资源
        curl_close($curl);
        // fopen() 函数打开文件或者 URL。
        $tp = @fopen($filename, 'a');
        // fwrite() 函数写入文件（可安全用于二进制文件）。
        fwrite($tp, $imageData);
        // 函数关闭一个打开文件
        fclose($tp);

        // 文件夹不存在，创建文件夹
        if (!file_exists($local_dir)) mkdir($local_dir, 0777, true);
        // 新文件名
        if ($new_name) {
            $newFileName = $local_dir . '/' . $new_name;
        } else {
            $newFileName = $local_dir . '/' . uniqid() . '.' . end($arr);
        }

        // 将下载文件复制到指定文字，且文件名为新文件名
        if (rename($filename, $newFileName)) {
            // 删除旧图片
            @unlink($filename);
        }

        $file_name = explode("uploads/", $newFileName);
        // 删除uploads路径,返回路径
        return $file_name[1];
    }
}
/**
 * 数字转换为字母(1 为 A； 2 为B)
 * @param $mobile
 * @return int
 */
if (!function_exists('serial_letter')) {
    function serial_letter($number)
    {
        return chr(ord($number) + 16);
    }
}

/**
 * 字母转换为数字   A 为 0  B 为 1  C 为 2
 */
if (!function_exists('letter_to_decimal')) {
    function letter_to_decimal($abc)
    {
        $ten = 0;

        $len = strlen($abc);

        for ($i = 1; $i <= $len; $i++) {
            $char = substr($abc, 0 - $i, 1); //反向获取单个字符

            $int = ord($char);

            $ten += ($int - 65) * pow(26, $i - 1);
        }

        return $ten;
    }
}


/**
 * 数字转换为字母   1 为 A  2 为 B  3 为 C
 */
if (!function_exists('decimal_to_letter')) {
    function decimal_to_letter($number)
    {
        $number = intval($number);
        if ($number <= 0)
            return false;
        $letterArr = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
        $letter = '';
        do {
            $key = ($number - 1) % 26;
            $letter = $letterArr[$key] . $letter;
            $number = floor(($number - $key) / 26);
        } while ($number > 0);
        return $letter;
    }
}


/**
 * 删除文件或者文件夹
 * $file 可删除单个，可删除多个 （数组形式）
 * $path 路径，可为空
 */
if (!function_exists('delete_file_or_dir')) {
    function delete_file_or_dir($file, $path = null)
    {
        if ($path) {
            $path = substr($path, -1, 1) == '/' ? $path : $path . '/';
        }
        if (is_array($file)) {
            foreach ($file as $key => $val) {
                if (is_dir($path . $val)) {
                    //删除文件夹(只能删除空文件夹)
                    @rmdir($path . $val);
                } else {
                    //删除文件
                    @unlink($path . $val);
                }
            }
        } else {
            if (is_dir($path . $file)) {
                @rmdir($path . $file);
            } else {
                @unlink($path . $file);
            }
        }
    }
}
/**
 * 删除及文件夹及文件夹下的内容
 * $dir_path 文件夹路径
 * $is_del_dir 是否删除文件夹
 */
if (!function_exists('delete_file_and_dir')) {
    function delete_file_and_dir($dir_path, $is_del_dir = false)
    {
        $dir_path = substr($dir_path, -1, 1) == '/' ? $dir_path : $dir_path . '/';

        if (file_exists($dir_path)) {
            $file = scandir($dir_path);
            //文件里包括"."和".."，两个目录
            for ($i = 2, $count = count($file); $i < $count; $i++) {
                $file_url = $dir_path . $file[$i];
                @unlink($file_url);
            }
        }
        if ($is_del_dir) rmdir($dir_path);
    }
}

/**
 * 阿拉伯数字到中文大写转换
 *
 * @param $num
 * @param bool $mode
 * @return string
 */
if (!function_exists('get_chinese_number')) {
    function get_chinese_number($num, $mode = true)
    {
        $char = array("零", "壹", "贰", "叁", "肆", "伍", "陆", "柒", "捌", "玖");
        $dw = array("", "拾", "佰", "仟", "", "万", "亿", "兆");
        $dec = "点";
        $retval = "";
        if ($mode) {
            preg_match_all("/^0*(\d*)\.?(\d*)/", $num, $ar);
        } else {
            preg_match_all("/(\d*)\.?(\d*)/", $num, $ar);
        }
        if ($ar[2][0] != 00) {
            $retval = $dec . get_chinese_number($ar[2][0], false); //如果有小数，则用递归处理小数
        }
        if ($ar[1][0] != 00) {
            $str = strrev($ar[1][0]);
            for ($i = 0; $i < strlen($str); $i++) {
                $out[$i] = $char[$str[$i]];
                if ($mode) {
                    $out[$i] .= $str[$i] != "0" ? $dw[$i % 4] : "";
                    if ($str[$i] + $str[($i - 1 >= 0) ? $i - 1 : 0] == 0) {
                        $out[$i] = "";
                    }
                    if ($i % 4 == 0) {
                        $out[$i] .= $dw[4 + floor($i / 4)];
                    }
                }
            }
            $retval = join("", array_reverse($out)) . $retval;
        }
        return $retval;
    }
}

/**
 * 将秒数转换成年天时分秒
 *
 * @param 秒数 $seconds
 * @return void
 */
if (!function_exists('second_to_time')) {
    function second_to_time($time)
    {
        if (is_numeric($time)) {
            $value = array(
                "years" => 0,
                "days" => 0,
                "hours" => 0,
                "minutes" => 0,
                "seconds" => 0,
            );
            $t = '';
            if ($time >= 31556926) {
                $value["years"] = floor($time / 31556926);
                $time = ($time % 31556926);
                $t .= $value["years"] . "年";
            }
            if ($time >= 86400) {
                $value["days"] = floor($time / 86400);
                $time = ($time % 86400);
                $t .= $value["days"] . "天";
            }
            if ($time >= 3600) {
                $value["hours"] = floor($time / 3600);
                $time = ($time % 3600);
                $t .= $value["hours"] . "小时";
            }
            if ($time >= 60) {
                $value["minutes"] = floor($time / 60);
                $time = ($time % 60);
                $t .= $value["minutes"] . "分";
            }
            $value["seconds"] = floor($time);
            $t .= $value["seconds"] . "秒";
            return $t;
        } else {
            return (bool)false;
        }
    }
}


/**
 * 加密函数
 * @param unknown $data
 * @param unknown $key
 */
if (!function_exists('encrypt1')) {
    function encrypt1($data, $key)
    {
        $txt = serialize($data);
        $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-=+";
        $nh = rand(0, 64);
        $ch = $chars[$nh];
        $mdKey = md5($key . $ch);
        $mdKey = substr($mdKey, $nh % 8, $nh % 8 + 7);
        $txt = base64_encode($txt);
        $tmp = '';
        $i = 0;
        $j = 0;
        $k = 0;
        for ($i = 0; $i < strlen($txt); $i++) {
            $k = $k == strlen($mdKey) ? 0 : $k;
            $j = ($nh + strpos($chars, $txt[$i]) + ord($mdKey[$k++])) % 64;
            $tmp .= $chars[$j];
        }
        return urlencode($ch . $tmp);
    }
}

/**
 * 解密函数
 * @param unknown $str
 * @param unknown $key
 */
if (!function_exists('decrypt1')) {
    function decrypt1($str, $key)
    {
        $txt = urldecode($str);
        $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-=+";
        $ch = $txt[0];
        $nh = strpos($chars, $ch);
        $mdKey = md5($key . $ch);
        $mdKey = substr($mdKey, $nh % 8, $nh % 8 + 7);
        $txt = substr($txt, 1);
        $tmp = '';
        $i = 0;
        $j = 0;
        $k = 0;
        for ($i = 0; $i < strlen($txt); $i++) {
            $k = $k == strlen($mdKey) ? 0 : $k;
            $j = strpos($chars, $txt[$i]) - $nh - ord($mdKey[$k++]);
            while ($j < 0) $j += 64;
            $tmp .= $chars[$j];
        }
        $data = unserialize(base64_decode($tmp));
        return $data;
    }
}


/**
 * 验证字符串，只能包含字母或数字
 */
if (!function_exists('letter_or_number')) {
    function letter_or_number($data)
    {
        if (!preg_match("/^[a-z\d]*$/i", $data)) {
            return "只能包含数字和字母"; //有数字有字母 ";
        }
        return true;
    }
}


/**
 * 验证管理员账号  （如果框架验证器内部有验证，推荐用框架内部验证器）
 * 1.只能是字母或数字，不能有其他字符
 * 2.账号长度5-20位的字符串
 */
if (!function_exists('validate_account')) {
    function validate_account($account)
    {
        if (strlen($account) > 20 || strlen($account) < 5) {
            return "账号必须为5-20位的字符串";
        }
        if (!preg_match("/^[a-z\d]*$/i", $account)) {
            return "账号只能包含数字和字母"; //有数字有字母 ";
        }
        return true;
    }
}


/**
 * 验证管理员密码
 * 1. 不能全部是数字
 * 2. 不能全部是字母
 * 3. 必须是数字和字母组合
 * 4. 不包含特殊字符
 * 5. 密码长度6-20位的字符串
 * return 返回：$msg
 */
if (!function_exists('validate_password')) {
    function validate_password($pwd)
    {
        if (strlen($pwd) > 20 || strlen($pwd) < 6) {
            return "密码必须为6-20位的字符串";
        }

        if (preg_match("/^\d*$/", $pwd)) {
            return "密码必须包含字母和数字"; //全数字
        }

        if (preg_match("/^[a-z]*$/i", $pwd)) {
            return "密码必须包含字母和数字"; //全字母
        }

        if (!preg_match("/^[a-z\d]*$/i", $pwd)) {
            return "密码只能包含数字和字母"; //有数字有字母 ";
        }
        return true;
    }
}


/**
 * @desc 根据两点间的经纬度计算距离
 * @param float $lat1 起点纬度值
 * @param float $lng1 起点经度值
 * @param float $lat2 终点纬度值
 * @param float $lng2 终点经度值
 */
if (!function_exists('get_distance')) {
    function get_distance($lng1, $lat1, $lng2, $lat2)
    {
        $pi = '3.1415926535898';
        $earthRadius = 6367000; //approximate radius of earth in meters
        $lat1 = ($lat1 * $pi) / 180;
        $lng1 = ($lng1 * $pi) / 180;

        $lat2 = ($lat2 * $pi) / 180;
        $lng2 = ($lng2 * $pi) / 180;

        $calcLongitude = $lng2 - $lng1;
        $calcLatitude = $lat2 - $lat1;
        $stepOne = pow(sin($calcLatitude / 2), 2) + cos($lat1) * cos($lat2) * pow(sin($calcLongitude / 2), 2);
        $stepTwo = 2 * asin(min(1, sqrt($stepOne)));
        $calculatedDistance = $earthRadius * $stepTwo;

        return round($calculatedDistance);
    }
}


/**
 * 删除过期图片
 * @param $filedir 需要删除的文件夹路径
 * @param $expir_time 过期时间   默认 1 天  单位  秒
 */
if (!function_exists('del_past_imgs')) {
    function del_past_imgs($filedir, $expir_time = 86400)
    {
        if (file_exists($filedir)) {
            $file = scandir($filedir);
            //文件里包括"."和".."，两个目录
            for ($i = 2, $count = count($file); $i < $count; $i++) {
                $file_path = $filedir . $file[$i];
                $file_expira = filectime($file_path);
                $expiras = time() - $expir_time; //默认有效期为 1 天
                if ($file_expira < $expiras) {
                    @unlink($file_path);
                }
            }
        }
    }
}


/**
 * 图片移动到指定位置，并返回移动后的路径
 * @param temp_img 临时文件路径
 * @param move_dir 移动后文件夹名
 */
if (!function_exists('move_uploaded_files')) {
    function move_uploaded_files($temp_img, $move_dir)
    {
        if (!file_exists(dirname($move_dir))) {
            mkdir(dirname($move_dir), 0777, true);
        }
        chmod($temp_img, 0777);
        $res = copy($temp_img, $move_dir);
        if ($res) {
            @unlink($temp_img);
            return true;
        }
        return false;
    }
}


/**
 * 处理文字，过多显示显示省略号
 * @return void
 */
if (!function_exists('cut_str')) {
    function cut_str($sourcestr, $cutlength)
    {
        $returnstr = '';
        $i = 0;
        $n = 0;
        $str_length = strlen($sourcestr); //字符串的字节数
        while (($n < $cutlength) and ($i <= $str_length)) {
            $temp_str = substr($sourcestr, $i, 1);
            $ascnum = Ord($temp_str); //得到字符串中第$i位字符的ascii码
            if ($ascnum >= 224)    //如果ASCII位高与224
            {
                $returnstr = $returnstr . substr($sourcestr, $i, 3); //根据UTF-8编码规范，将3个连续的字符计为单个字符
                $i = $i + 3;            //实际Byte计为3
                $n++;            //字串长度计1
            } elseif ($ascnum >= 192) //如果ASCII位高与192
            {
                $returnstr = $returnstr . substr($sourcestr, $i, 2); //根据UTF-8编码规范，将2个连续的字符计为单个字符
                $i = $i + 2;            //实际Byte计为2
                $n++;            //字串长度计1
            } elseif ($ascnum >= 65 && $ascnum <= 90) //如果是大写字母
            {
                $returnstr = $returnstr . substr($sourcestr, $i, 1);
                $i = $i + 1;            //实际的Byte数仍计1个
                $n++;            //但考虑整体美观，大写字母计成一个高位字符
            } else                //其他情况下，包括小写字母和半角标点符号
            {
                $returnstr = $returnstr . substr($sourcestr, $i, 1);
                $i = $i + 1;            //实际的Byte数计1个
                $n = $n + 0.5;        //小写字母和半角标点等与半个高位字符宽…
            }
        }
        if ($str_length > $i) {
            $returnstr = $returnstr . "…"; //超过长度时在尾处加上省略号
        }
        return $returnstr;
    }
}

/**
 * 把距离的米转换为千米（大于4位的）
 */
if (!function_exists('m_switch_km')) {
    function m_switch_km($num)
    {
        if (strlen($num) > 3) {
            $start = substr($num, 0, strlen($num) - 3); //截取前几位
            $end = substr($num, strlen($num) - 3); //截取后面3位
            $num = $start . "." . $end;
            $data['num'] = round($num, 2);
            $data['symbol'] = 'km';
            return $data;
        } else {
            $data['num'] = $num;
            $data['symbol'] = 'm';
            return $data;
        }
    }
}


/**
 * 判断是否是日期时间格式
 * @param $dateTime 日期时间  2021-04-21 09:47:43 格式  与$format Y-m-d H:i:s对应
 * @param $dateTime 日期时间  2021-04-21 格式  与$format 为 Y-m-d 对应
 */
if (!function_exists('is_date')) {
    function is_date($dateTime, $format = 'Y-m-d H:i:s')
    {
        if (date($format, strtotime($dateTime)) == $dateTime) {
            return true;
        } else {
            return false;
        }
    }
}

/**
 * 昵称有特殊符号，特殊符号统一替换
 * @param string $nickname 字符串
 */
if (!function_exists('nickname_switch_no_sepcial')) {
    function nickname_switch_no_sepcial($nickname)
    {
        $nickname_arr = mb_str_split($nickname);
        $nickname = '';
        foreach ($nickname_arr as $val) {
            if (have_special_char($val)) {
                $nickname .= '�';
            } else {
                $nickname .= $val;
            }
        }
        return $nickname;
    }
}

/**
 * 将字符串分割为数组
 * @param string $str 字符串
 * @return array       分割得到的数组
 */
if (!function_exists('mb_str_split')) {
    function mb_str_split($str)
    {
        return preg_split('/(?<!^)(?!$)/u', $str);
    }
}


/**
 * 删除时间比较久的文件
 * $filedir 文件夹路径，只支持一级路径（里面不能在包含文件夹）
 * $times 多久之前的文件  单位秒
 */
if (!function_exists('del_past_file')) {
    function del_past_file($dir_path, $times = 60)
    {
        if (file_exists($dir_path)) {
            $file = scandir($dir_path);
            //文件里包括"."和".."，两个目录
            for ($i = 2, $count = count($file); $i < $count; $i++) {
                $file_path = $dir_path . '/' . $file[$i];
                $file_change_time = filemtime($file_path); //获取文件修改时间戳
                $expiras = time() - $times;
                if ($file_change_time < $expiras) {
                    @unlink($file_path);
                }
            }
        }
    }
}
/**
 * 文件大小转换
 * 参数单位为“字节”
 */
if (!function_exists('format_bytes')) {
    function format_bytes($bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = round($bytes / 1073741824 * 100) / 100 . 'GB';
        } elseif ($bytes >= 1048576) {
            $bytes = round($bytes / 1048576 * 100) / 100 . 'MB';
        } elseif ($bytes >= 1024) {
            $bytes = round($bytes / 1024 * 100) / 100 . 'KB';
        } else {
            $bytes = $bytes . 'Bytes';
        }
        return $bytes;
    }
}


/**
 * 加密(rc4)
 */
if (!function_exists('rc4_encrypt')) {
    function rc4_encrypt($data, $pwd)
    {
        $res = rc4($data, $pwd);
        return to_hex($res);
    }
}
/**
 * 解密(rc4)
 **/
if (!function_exists('rc4_decrypt')) {
    function rc4_decrypt($data, $pwd)
    {
        $str = from_hex($data);
        return rc4($str, $pwd);
    }
}
/**
 * [rc4 description]
 * @param  [type] $data [需加密的数据]   $data需加密字符串或需要解密的密文
 * @param  [type] $pwd  [密钥]
 * @return [type]       [加密后为二进制，需要转换成16进制]
 */
if (!function_exists('rc4')) {
    function rc4($data, $pwd)
    {
        $key[] = "";
        $box[] = "";
        $pwd_length = strlen($pwd);
        $data_length = strlen($data);
        $cipher = '';
        for ($i = 0; $i < 256; $i++) {
            $key[$i] = ord($pwd[$i % $pwd_length]);
            $box[$i] = $i;
        }
        for ($j = $i = 0; $i < 256; $i++) {
            $j = ($j + $box[$i] + $key[$i]) % 256;
            $tmp = $box[$i];
            $box[$i] = $box[$j];
            $box[$j] = $tmp;
        }
        for ($a = $j = $i = 0; $i < $data_length; $i++) {
            $a = ($a + 1) % 256;
            $j = ($j + $box[$a]) % 256;
            $tmp = $box[$a];
            $box[$a] = $box[$j];
            $box[$j] = $tmp;
            $k = $box[(($box[$a] + $box[$j]) % 256)];
            $cipher .= chr(ord($data[$i]) ^ $k);
        }
        return $cipher;
    }
}
/**
 * 把数据转换成16进制
 * @param  [type]  $sa  [需要转换的数据]
 * @param integer $len [数据长度]
 */
if (!function_exists('to_hex')) {
    function to_hex($sa, $len = 0)
    {
        $buf = "";
        if ($len == 0)
            $len = strlen($sa);
        for ($i = 0; $i < $len; $i++) {
            $val = dechex(ord($sa[$i]));
            if (strlen($val) < 2)
                $val = "0" . $val;
            $buf .= $val;
        }
        return $buf;
    }
}
/**
 * 把16进制转换成字符串
 * @param  [type] $sa [16进制数据]
 */
if (!function_exists('from_hex')) {
    function from_hex($sa)
    {
        $buf = "";
        $len = strlen($sa);
        for ($i = 0; $i < $len; $i += 2) {
            $val = chr(hexdec(substr($sa, $i, 2)));
            $buf .= $val;
        }
        return $buf;
    }
}
/**
 *获取时间小时和分
 * @param $time 比如11:52:00
 * @return $new_time 统一转为11:52
 **/
if (!function_exists('get_hour_minute_by_time')) {
    function get_hour_minute_by_time($time)
    {
        return date("H:i", strtotime($time));
    }
}

/**
 *判断多个时间数组之间有无交叉重叠
 * @param $time 比如11:52:00
 * @return $new_time 统一转为11:52
 **/
if (!function_exists('compare_date')) {
    function compare_date($arr)
    {
        array_multisort(array_column($arr, 'start_time'), SORT_ASC, $arr);
        foreach ($arr as $key => $val) {
            if (get_hour_minute_by_time($val['start_time']) >= get_hour_minute_by_time($val['end_time']))
                return false;
            if ($key > 0 && get_hour_minute_by_time($arr[$key]['start_time']) < get_hour_minute_by_time($arr[$key - 1]['end_time']))
                return false;
        }
        return true;
    }
}

/*
* 返回输入日期星期几
* @param $date 日期
* */
if (!function_exists('get_week')) {
    function get_week($date, $prefix = '星期')
    {
        $date_str = date('Y-m-d', strtotime($date));
        $arr = explode("-", $date_str);
        $year = $arr[0];
        $month = sprintf('%02d', $arr[1]);
        $day = sprintf('%02d', $arr[2]);
        $hour = $minute = $second = 0;
        $strap = mktime($hour, $minute, $second, $month, $day, $year);
        $number_wk = date("w", $strap);
        $weekArr = array($prefix . "日", $prefix . "一", $prefix . "二", $prefix . "三", $prefix . "四", $prefix . "五", $prefix . "六");
        return [$number_wk, $weekArr[$number_wk]];
    }
}

/**
 * 判断2个时间相差的月份
 * @var date1 日期1  $date1 = "2003-08-11";
 * @var date2 日期2 $date2 = "2008-11-06";
 * @var tags 年月日之间的分隔符标记,默认为'-'
 */
if (!function_exists('get_month_num')) {
    function get_month_num($date1, $date2, $tags = '-')
    {
        $date1 = explode($tags, $date1);
        $date2 = explode($tags, $date2);
        return abs($date1[0] - $date2[0]) * 12 + abs($date1[1] - $date2[1]);
    }
}


/**
 * 讲数字名称，换成中文星期名称
 */
if (!function_exists('get_week_name')) {
    function get_week_name($number, $prefix = '星期')
    {
        if ($number === 0 || $number === '0') {
            return $prefix . '日'; //特殊处理
        }
        switch ($number) {
            case 1:
                return $prefix . "一";
                break;
            case 2:
                return $prefix . "二";
                break;
            case 3:
                return $prefix . "三";
                break;
            case 4:
                return $prefix . "四";
                break;
            case 5:
                return $prefix . "五";
                break;
            case 6:
                return $prefix . "六";
                break;
            case 7:
                return $prefix . "日";
                break;
            default:
                return '未知';
                break;
        }
    }
}

/**
 * 验证邮箱格式
 */
if (!function_exists('is_email')) {
    function is_email($email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return false;
        } else {
            return true;
        }
    }
}


/**
 * 获取富文本编译器中的所有图片
 * @param string $content
 * @param string $type 类型，img 图片  video 视频 audio 音频
 * @return array
 */
if (!function_exists('get_source_by_content')) {
    function get_source_by_content($content = "", $type = 'img')
    {
        $src_arr = [];
        // $pattern_tag = '/<img\b.*?(?:\>|\/>)/i';
        $pattern_tag = '/<' . $type . '\b.*?(?:\>|\/>)/i';
        preg_match_all($pattern_tag, $content, $match_content);
        if (isset($match_content[0])) {
            foreach ($match_content[0] as $key => $tag) {
                $pattern_src = '/\bsrc\b\s*=\s*[\'\"]?([^\'\"]*)[\'\"]?/i';
                preg_match_all($pattern_src, $tag, $matchSrc);
                if (isset($matchSrc[1])) {
                    foreach ($matchSrc[1] as $src) {
                        $src_arr[] = $src;
                    }
                }
            }
        }
        return $src_arr;
    }
}

/**
 * 获取远程视频大小
 * @param $url 资源链接(线上)
 **/
if (!function_exists('get_resource_size')) {
    function get_resource_size($url)
    {
        $res = get_headers($url, true);
        return $res['Content-Length'];
    }
}


/**
 * php 中文逗号 转英文,PHP把空格、换行符、中文逗号等替换成英文逗号
 */
if (!function_exists('replace_chars')) {
    function replace_chars($content)
    {
        return preg_replace("/(\n)|(\s)|(\t)|(\')|(')|(，)/", ',', $content);
    }
}

/**
 * 获取本季度的开始时间和结束时间
 */
if (!function_exists('get_now_quarter')) {
    function get_now_quarter()
    {
        $season = ceil((date('n')) / 3); //当月是第几季度
        $quarter_start = date('Y-m-d H:i:s', mktime(0, 0, 0, $season * 3 - 3 + 1, 1, date('Y')));
        $quarter_end = date('Y-m-d H:i:s', mktime(23, 59, 59, $season * 3, date('t', mktime(0, 0, 0, $season * 3, 1, date("Y"))), date('Y')));

        return [$quarter_start, $quarter_end];
    }
}

/**
 * 下载大文件
 */
if (!function_exists('download_file')) {
    function download_file($filename)
    {
        //获取文件的扩展名
        $allowDownExt = array('rar', 'zip', 'png', 'txt', 'mp4', 'html');

        //获取文件信息
        $fileExt = pathinfo($filename);
        //检测文件类型是否允许下载
        if (empty($fileExt['extension']) || !in_array($fileExt['extension'], $allowDownExt)) {
            return false;
        }

        //设置脚本的最大执行时间，设置为0则无时间限制
        set_time_limit(0);
        ini_set('max_execution_time', '0');

        //通过header()发送头信息
        //因为不知道文件是什么类型的，告诉浏览器输出的是字节流
        header('content-type:application/octet-stream');

        //告诉浏览器返回的文件大小类型是字节
        header('Accept-Ranges:bytes');

        //获得文件大小
        if (strtolower(substr($filename, 0, 4)) == 'http') {
            //如果获取远程图片大小，用下面的方法
            $header_array = get_headers($filename, true);
            $filesize = $header_array['Content-Length'];
        } else {
            if (!file_exists($filename)) {
                return false;
            }
            $filesize = filesize($filename); //(此方法无法获取到远程文件大小)
        }

        //告诉浏览器返回的文件大小
        header('Accept-Length:' . $filesize);
        //告诉浏览器文件作为附件处理并且设定最终下载完成的文件名称
        header('content-disposition:attachment;filename=' . basename($filename));

        //针对大文件，规定每次读取文件的字节数为4096字节，直接输出数据
        $read_buffer = 4096;
        $handle = fopen($filename, 'rb');
        //总的缓冲的字节数
        $sum_buffer = 0;
        //只要没到文件尾，就一直读取
        while (!feof($handle) && $sum_buffer < $filesize) {
            echo fread($handle, $read_buffer);
            $sum_buffer += $read_buffer;
        }

        //关闭句柄
        fclose($handle);
        exit;
    }
}

/**
 * 创建目录
 */
if (!function_exists('create_dirs')) {
    function create_dirs($directory_path)
    {
        if (!is_dir($directory_path)) {
            @mkdir($directory_path, 0777, true);
        }
    }
}
/**
 * 分钟转换为小时+分钟
 * @param 分钟
 */
if (!function_exists('minutes_to_hour')) {
    function minutes_to_hour($minutes)
    {
        // 计算小时部分
        $hours = floor($minutes / 60);
        // 计算分钟部分（取余）
        $remainingMinutes = $minutes % 60;

        $data = '';
        if ($hours) {
            $data = "{$hours}小时";
        }
        if ($remainingMinutes) {
            $data .=  "{$remainingMinutes}分钟";
        }
        if (empty($data)) {
            $data = '0分钟';
        }
        return $data;
    }
}

/**
 * 获取文件夹内容图片
 * @param $filedir 需要获取的文件夹路径
 * @param $type 1 只获取文件夹下的文件名， 2 获取全路径
 */
if (!function_exists('get_dir_content_list')) {
    function get_dir_content_list($filedir, $type = 1)
    {
        if (file_exists($filedir)) {
            $file = scandir($filedir);
            //文件里包括"."和".."，两个目录
            $data = [];
            for ($i = 2, $count = count($file); $i < $count; $i++) {
                if ($type == 1) {
                    $data[] = $file[$i];
                } else {
                    $data[] = $filedir . $file[$i];
                }
            }
            return $data;
        }
        return '';
    }
}

/**
 * 按年获取每天的日期
 * @param $year 年份
 * @return array
 */
if (!function_exists('get_days_by_year')) {
    function get_days_by_year($year)
    {
        $days = [];
        $date = new DateTime("$year-01-01"); // 开始日期为年份的第一天
        $date->modify('last day of december'); // 修改为年份的最后一天

        for ($i = 0; $i < $date->format('z') + 1; $i++) {
            $days[] = date('Y-m-d', strtotime("$year-01-01 +$i days"));
        }
        return $days;
    }
}


/**
 * 获取2个时间段内所有的月份
 * @param start_date 2023-08-08
 * @param end_date 2024-12-08
 */
if (!function_exists('get_months_between_two_dates')) {

    function get_months_between_two_dates($start_date, $end_date)
    {
        $start = new DateTime($start_date);
        $end = new DateTime($end_date);
        $end->modify('last day of this month'); // 调整到月底，确保循环到最后一个月
        $interval = new DateInterval('P1M');
        $period = new DatePeriod($start, $interval, $end);

        $months = [];
        foreach ($period as $month) {
            $months[] = $month->format('Y-m');
        }

        return $months;
    }
}
