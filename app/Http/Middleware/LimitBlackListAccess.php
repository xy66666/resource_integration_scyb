<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class LimitBlackListAccess
{
    private $blacklist = [
        '182.139.67.1501', // 替换成实际的黑名单IP

        // ... 其他黑名单IP
    ];

    public function handle(Request $request, Closure $next)
    {
        if (in_array($request->ip(), $this->blacklist)) {
            // 如果请求来自于黑名单中的IP，返回403禁止访问
            return response('Forbidden.', 403);
        }

        return $next($request);
    }
}
