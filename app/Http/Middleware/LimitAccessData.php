<?php

namespace App\Http\Middleware;

use App\Http\Controllers\RedisServiceController;
use App\Models\AnswerActivityBrowseCount;
use App\Models\Manage;
use App\Models\TurnActivityBrowseCount;
use App\Models\UserInfo;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * 限制访问人数
 */
class LimitAccessData
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        return $next($request);
    }
}
