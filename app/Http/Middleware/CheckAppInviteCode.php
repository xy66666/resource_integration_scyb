<?php

namespace App\Http\Middleware;

use App\Http\Controllers\JwtController;
use App\Models\AppInviteCode;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * 验证前台邀请码访问
 */
class CheckAppInviteCode
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        return $next($request); //验证通过   正式删除
    }

    /**
     * 获取请求参数中的 app_invite_code
     * @param request  object 请求参数对象
     */
    public function getInviteCode($request)
    {
        $invite_code = $request->app_invite_code;

        return $invite_code;
    }
}
