<?php

namespace App\Http\Controllers;

use Intervention\Image\ImageManagerStatic as Image;

/**
 * composer require intervention/image
 * 
 * 图片处理
 */
class ImgController extends Controller
{
    /**
     * 将两张图片合成一张
     * $bg_path    背景图地址
     * $poster     图片2
     * $x           图片2在背景图片上位置的左边距,单位：px （例：436）
     * $y           图片2在背景图片上位置的上边距,单位：px （例：1009）
     * $poster_w	图片2宽度,单位：px （例：200）
     * $poster_y		图片2高度,单位：px （例：300）
     * $echo_path   生成的新图片存放路径
     **/
    public function createPoster($bg_path, $poster, $x, $y, $poster_w, $poster_y, $echo_path, $is_base64 = 0)
    {
        $background = imagecreatefromstring(file_get_contents($bg_path));
        $poster_res = imagecreatefromstring(file_get_contents($poster));
        //参数内容:目标图象连接资源，源图象连接资源，目标X坐标点，目标Y坐标点，源的X坐标点，源的Y坐标点，目标宽度，目标高度，源图象的宽度，源图象的高度
        imagecopyresampled($background, $poster_res, $x, $y, 0, 0, $poster_w, $poster_y, $poster_w, $poster_y);
        //输出到本地文件夹，返回生成图片的路径
        if (!is_dir(dirname($echo_path))) {
            mkdir(dirname($echo_path), 0755, true);
            chown(dirname($echo_path), 'nobody');
            chgrp(dirname($echo_path), 'nobody');
        }

        if ($is_base64) {
            ob_start();
            //imagepng展示出图片
            imagepng($background);
            $imageData = ob_get_contents();
            ob_end_clean();
            //得到这个结果，可以直接用于前端的img标签显示
            $res = "data:image/jpeg;base64," . base64_encode($imageData);
        } else {
            imagepng($background, $echo_path);
            $res = $echo_path;
        }
        imagedestroy($background);
        imagedestroy($poster_res);
        return $res;
    }
    /**
     * 给图片加文字
     * $bg_path    背景图地址
     * $text			要添加的文字
     * $x           文字在背景图片上位置的左边距,单位：px （例：436）
     * $y           文字在背景图片上位置的上边距,单位：px （例：1009）
     * $font_size	字体大小,单位：px （例：20）
     * $echo_path   生成的新图片存放路径
     **/
    public function imageAddText($bg_path, $text, $x, $y, $font_size, $echo_path)
    {
        $background = imagecreatefromstring(file_get_contents($bg_path));
        $font = "/datacentr/fonts/simhei.ttf"; //字体在服务器上的绝对路径
        $black = imagecolorallocate($background, 0, 0, 0); //字体颜色黑色

        $str = mb_convert_encoding($text, "html-entities", "utf-8");

        imagefttext($background, $font_size, 0, $x, $y, $black, $font, $str);
        imagepng($background, $echo_path);
        imagedestroy($background);


        //文字在图片中居中算法
        //参数： font_size, 文字倾斜角度， 字体文件绝对路径， 文字内容
        // $fontBox = imagettfbbox(78, 0, realpath("fzhtjt.ttf"), $title);//获取文字所需的尺寸大小 

        // // 居中算法
        // //$widht 背景图宽度   $height 背景图高度
        // ceil(($widht- $fontBox[2]) / 2)  //文字在背景图中的起始宽度
        // ceil(($height - $fontBox[1] - $fontBox[7]) / 2)  //文字在背景图中的高度

        return $echo_path;
    }

    /**
     * 文字转换为图片
     * 
     *  # size($size) 设置字体大小
        # $size int
        # 如果不设置字体，字体大小不生效

        # file($file) 设置字体文件
        # $file位置

        # color($color) 设置字体颜色
        # $color 支持16进制颜色｜rgb颜色
     */
    public function textToImg($content)
    {
        $image = Image::canvas(500, 500);
        $image->text("给我点个赞，\n可以不可以。给我点个赞，\n可以不可以。给我点个赞，\n可以不可以。给我点个赞，\n可以不可以。给我点个赞，\n可以不可以。给我点个赞，\n可以不可以。
        给我点个赞，\n可以不可以。给我点个赞，\n可以不可以。给我点个赞，\n可以不可以。", 100, 200, function ($font) {
            /** @var \Intervention\Image\Gd\Font $font */
            $font->size(25)
                // ->file(public_path('font/华文行楷.ttf'))
                ->file(public_path('font/MSYH.ttf'))
                //                ->color('EBC825');
                ->color([235, 200, 37]);
        })->save(public_path('uploads/default/1.jpg'));
        //  return $image->response();
    }
}
