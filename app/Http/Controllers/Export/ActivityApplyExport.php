<?php

namespace App\Http\Controllers\Export;

use App\Http\Controllers\Controller;
use App\Models\Activity;
use App\Models\ActivityApply;
use Maatwebsite\Excel\Facades\Excel;

/**
 * excel导出导入功能
 */
class ActivityApplyExport extends Controller
{

    public $model = null;
    public $activityModel = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new ActivityApply();
        $this->activityModel = new Activity();
    }

    /**
     * 批量活动申请列表导出
     * @param keywords string 搜索关键词
     * @param start_time datetime 活动开始时间    数据格式  年月日
     * @param end_time datetime 活动结束时间
     * @param is_recom 是否热门活动  0或空表示全部  1 是  2 否 （默认）
     * @param is_apply 是否需要报名 1.是  2.否  默认1
     * @param is_play 是否发布 1.是  2.否  默认1
     * @param type_id 活动类型筛选   0或空表示全部
     * @param create_start_time datetime 活动创建开始时间   数据格式 年月日时分秒
     * @param create_end_time datetime 活动创建结束时间
     */
    public function activityApplyListAll()
    {
        $keywords = $this->request->keywords;
        $start_time = $this->request->start_time;
        $end_time = $this->request->end_time;
        $create_start_time = $this->request->create_start_time;
        $create_end_time = $this->request->create_end_time;
        $type_id = $this->request->type_id;
        $is_apply = $this->request->is_apply;
        $is_play = $this->request->is_play;
        $is_recom = $this->request->is_recom;

        $act_res = $this->activityModel->lists($keywords, $type_id, $start_time, $end_time, $create_start_time, $create_end_time, $is_apply, $is_recom, $is_play, null, 99999);

        if (empty($act_res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }
        $act_id = array_column($act_res['data'], 'id');
        $status = empty($status) ? [1, 3, 4] : $status;
        $res = $this->model->activityApplyUserList($act_id, $status, null, null, null, null, null, 10000);
        if (empty($res)) {
            $res['data'] = [];
        }

        //设置表头
        $row = [[
            "id" => '序号',
            "title" => '活动标题',
            "nickname" => '微信昵称',
            "username" => '姓名',
            "tel" => '电话号码',
            "id_card" => '身份证号码',
            "reader_id" => '读者证号',
            "sex" => '性别',
            "age" => '年龄',
            "unit" => '单位',
            "school" => '学校',
            "grade" => '年级',
            "class_grade" => '班级',
            "wechat_number" => '微信号',
            "remark" => '备注',
            "status" => '状态',
            "create_time" => '申请时间',
            "reason" => '拒绝理由',
            "sign_num" => '签到次数',
            "is_violate" => '是否违规',
            "violate_reason" => '违规理由',
            "manage_name" => '审核管理员名称',
            "change_time" => '审核时间',
        ]];
        $title = '批量活动申请列表-' . date('YmdHis');
        //执行导出
        $data = $this->setData1($res['data'], true); //要导入的数据
        return $this->exportData($data, $title, $row);
    }


    /**
     * 批量活动人员列表导出
     * @param keywords string 搜索关键词
     * @param start_time datetime 活动开始时间    数据格式  年月日
     * @param end_time datetime 活动结束时间
     * @param is_recom 是否热门活动  0或空表示全部  1 是  2 否 （默认）
     * @param is_apply 是否需要报名 1.是  2.否  默认1
     * @param is_play 是否发布 1.是  2.否  默认1
     * @param type_id 活动类型筛选   0或空表示全部
     * @param create_start_time datetime 活动创建开始时间   数据格式 年月日时分秒
     * @param create_end_time datetime 活动创建结束时间
     */
    public function activityApplyUserAll()
    {
        $keywords = $this->request->keywords;
        $start_time = $this->request->start_time;
        $end_time = $this->request->end_time;
        $create_start_time = $this->request->create_start_time;
        $create_end_time = $this->request->create_end_time;
        $type_id = $this->request->type_id;
        $is_apply = $this->request->is_apply;
        $is_play = $this->request->is_play;
        $is_recom = $this->request->is_recom;

        $act_res = $this->activityModel->lists($keywords, $type_id, $start_time, $end_time, $create_start_time, $create_end_time, $is_apply, $is_recom, $is_play, null, 99999);

        if (empty($act_res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }
        $act_id = array_column($act_res['data'], 'id');


        $res = $this->model->activityApplyUserList($act_id, 1, null, null, null, null, null, 10000);
        if (empty($res)) {
            $res['data'] = [];
        }
        //设置表头
        $row = [[
            "id" => '序号',
            "title" => '活动标题',
            "nickname" => '微信昵称',
            "username" => '姓名',
            "tel" => '电话号码',
            "id_card" => '身份证号码',
            "reader_id" => '读者证号',
            "sex" => '性别',
            "age" => '年龄',
            "unit" => '单位',
            "school" => '学校',
            "grade" => '年级',
            "class_grade" => '班级',
            "wechat_number" => '微信号',
            "remark" => '备注',
            "create_time" => '申请时间',
            "sign_num" => '签到次数',
            "is_violate" => '是否违规',
            "violate_reason" => '违规理由',
            "manage_name" => '审核管理员名称',
            "is_sign" => '今日是否签到',
            "last_sign_date" => '最后签到时间',
        ]];
        $title = '活动人员列表-' . date('YmdHis');
        //执行导出
        $data = $this->setData2($res['data'], true); //要导入的数据
        return $this->exportData($data, $title, $row);
    }


    /**
     * 活动申请列表
     * @param act_id int 活动id
     * @param keywords string 搜索关键词
     * @param status int 报名状态    1.已通过  3.已拒绝  4.审核中
     */
    public function activityApplyList()
    {
        $keywords = $this->request->keywords;
        $status = $this->request->status;
        $act_id = $this->request->act_id;

        $status = empty($status) ? [1, 3, 4] : $status;
        $res = $this->model->activityApplyUserList($act_id, $status, null, $keywords, null, null, null, 10000);
        if (empty($res)) {
            $res['data'] = [];
        }

        //设置表头
        $row = [[
            "id" => '序号',
            "nickname" => '微信昵称',
            "username" => '姓名',
            "tel" => '电话号码',
            "id_card" => '身份证号码',
            "reader_id" => '读者证号',
            "sex" => '性别',
            "age" => '年龄',
            "unit" => '单位',
            "school" => '学校',
            "grade" => '年级',
            "class_grade" => '班级',
            "wechat_number" => '微信号',
            "remark" => '备注',
            "status" => '状态',
            "create_time" => '申请时间',
            "reason" => '拒绝理由',
            "sign_num" => '签到次数',
            "is_violate" => '是否违规',
            "violate_reason" => '违规理由',
            "manage_name" => '审核管理员名称',
            "change_time" => '审核时间',
        ]];

        $title = '活动申请列表-' . date('YmdHis');
        //执行导出
        $data = $this->setData1($res['data']); //要导入的数据
        return $this->exportData($data, $title, $row);
    }


    /**
     * 活动人员列表
     * @param act_id int 活动id
     * @param keywords string 搜索关键词
     * @param keywords_type string 关键词类型 username(名称) id_card(身份证) tel(电话) reader_id(读者证) qr_code(二维码)
     * @param is_sign 今日是否签到 0全部 1已签到 2未签到  默认等于2
     * @param start_time 报名开始时间
     * @param end_time 报名结束时间
     */
    public function activityApplyUser()
    {
        $keywords = $this->request->keywords;
        $keywords_type = $this->request->keywords_type ? $this->request->keywords_type : '';
        $act_id = $this->request->act_id;
        $start_time = $this->request->start_time;
        $end_time = $this->request->end_time;
        $is_sign = $this->request->is_sign ? $this->request->is_sign : 0;

        $res = $this->model->activityApplyUserList($act_id, 1, $keywords_type, $keywords, $start_time, $end_time, $is_sign, 10000);
        if (empty($res)) {
            $res['data'] = [];
        }
        //设置表头
        $row = [[
            "id" => '序号',
            "nickname" => '微信昵称',
            "username" => '姓名',
            "tel" => '电话号码',
            "id_card" => '身份证号码',
            "reader_id" => '读者证号',
            "sex" => '性别',
            "age" => '年龄',
            "unit" => '单位',
            "school" => '学校',
            "grade" => '年级',
            "class_grade" => '班级',
            "wechat_number" => '微信号',
            "remark" => '备注',
            "create_time" => '申请时间',
            "sign_num" => '签到次数',
            "is_violate" => '是否违规',
            "violate_reason" => '违规理由',
            "manage_name" => '审核管理员名称',
            "is_sign" => '今日是否签到',
            "last_sign_date" => '最后签到时间',
        ]];

        $title = '活动人员列表-' . date('YmdHis');
        //执行导出
        $data = $this->setData2($res['data']); //要导入的数据
        return $this->exportData($data, $title, $row);
    }


    /**
     * 导出数据
     *
     * @param [type] $data
     * @return void
     */
    public function exportData($data, $title, $row)
    {
        $header = $row; //导出表头
        $excel = new Export($data, $header, $title);
        $excel->setColumnWidth(['A' => 10, 'B' => 30, 'C' => 30, 'D' => 20, 'E' => 20, 'F' => 20, 'G' => 20, 'H' => 20, 'I' => 20, 'J' => 20, 'K' => 20, 'L' => 20, 'M' => 20, 'N' => 20, 'O' => 20, 'P' => 20, 'Q' => 20, 'R' => 20, 'S' => 20, 'T' => 20, 'U' => 20, 'V' => 20]);
        $excel->setRowHeight([1 => 30]);
        // $excel->setFreezePane('A2');
        // $excel->setFont(['A1:Z1265' => '宋体']);
        // $excel->setFontSize(['A1:I1' => 14,'A2:Z1265' => 10]);
        // $excel->setBold(['A1:Z2' => true]);
        // $excel->setBackground(['A1:A1' => '808080','C1:C1' => '708080']);
        // $excel->setMergeCells(['A1:I1']);
        // $excel->setBorders(['A2:D5' => '#000000']);
        return Excel::download($excel, $title . '.xlsx');
    }


    /**
     * 处理 数据
     * @param $is_view_title 是否显示活动标题
     */
    public function setData1($data, $is_view_title = false)
    {
        $excel_data = [];
        /*设置excel内容*/
        foreach ($data as $key => $val) {
            $excel_data[$key]['id'] = $key + 1;
            if ($is_view_title) $excel_data[$key]['title'] = $val['title'];
            $excel_data[$key]['nickname'] = $val['nickname'];
            $excel_data[$key]['username'] = $val['username'];
            $excel_data[$key]['tel'] = "\t" . $val['tel'];
            $excel_data[$key]['id_card'] = "\t" . $val['id_card'];
            $excel_data[$key]['reader_id'] = "\t" . $val['reader_id'];
            $excel_data[$key]['sex'] = $val['sex'] == 1 ? '男' : ($val['sex'] == 2 ? '女' : '未知');
            $excel_data[$key]['age'] = $val['age'];
            $excel_data[$key]['unit'] = $val['unit'];
            $excel_data[$key]['school'] = $val['school'];
            $excel_data[$key]['grade'] = $val['grade'];
            $excel_data[$key]['class_grade'] = $val['class_grade'];
            $excel_data[$key]['wechat_number'] = "\t" . $val['wechat_number'];
            $excel_data[$key]['remark'] = $val['remark'];
            $excel_data[$key]['status'] = $val['status'] == 1 ? '已通过' : ($val['status'] == 3 ? '已拒绝' : '审核中');
            $excel_data[$key]['create_time'] = $val['create_time'];
            $excel_data[$key]['reason'] = strip_tags($val['reason']);
            $excel_data[$key]['sign_num'] = $val['sign_num'];
            $excel_data[$key]['is_violate'] = $val['is_violate'] == 1 ? '正常' : '违规';
            $excel_data[$key]['violate_reason'] = strip_tags($val['violate_reason']);
            $excel_data[$key]['manage_name'] = $val['manage_name'];
            $excel_data[$key]['change_time'] = $val['change_time'];
        }
        return $excel_data;
    }
    /**
     * 处理  数据
     * @param $is_view_title 是否显示活动标题
     */
    public function setData2($data, $is_view_title = false)
    {
        $excel_data = [];
        /*设置excel内容*/
        foreach ($data as $key => $val) {
            $excel_data[$key]['id'] = $key + 1;
            if ($is_view_title) $excel_data[$key]['title'] = $val['title'];
            $excel_data[$key]['nickname'] = $val['nickname'];
            $excel_data[$key]['username'] = $val['username'];
            $excel_data[$key]['tel'] = "\t" . $val['tel'];
            $excel_data[$key]['id_card'] = "\t" . $val['id_card'];
            $excel_data[$key]['reader_id'] = "\t" . $val['reader_id'];
            $excel_data[$key]['sex'] = $val['sex'] == 1 ? '男' : ($val['sex'] == 2 ? '女' : '未知');
            $excel_data[$key]['age'] = $val['age'];
            $excel_data[$key]['unit'] = $val['unit'];
            $excel_data[$key]['school'] = $val['school'];
            $excel_data[$key]['grade'] = $val['grade'];
            $excel_data[$key]['class_grade'] = $val['class_grade'];
            $excel_data[$key]['wechat_number'] = "\t" . $val['wechat_number'];
            $excel_data[$key]['remark'] = $val['remark'];
            $excel_data[$key]['create_time'] = $val['create_time'];
            $excel_data[$key]['sign_num'] = $val['sign_num'];
            $excel_data[$key]['is_violate'] = $val['is_violate'] == 1 ? '正常' : '违规';
            $excel_data[$key]['violate_reason'] = strip_tags($val['violate_reason']);

            $excel_data[$key]['manage_name'] = $val['manage_name'];
            $excel_data[$key]['is_sign'] = $val['is_sign'] == 1 ? '签到' : '未签到';
            $excel_data[$key]['last_sign_date'] = $val['last_sign_date'];
        }
        return $excel_data;
    }
}
