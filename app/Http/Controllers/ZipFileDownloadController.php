<?php

namespace App\Http\Controllers;


/**
 * 下载压缩包文件
 */
class ZipFileDownloadController extends Controller
{

    /**
     * 添加文件到压缩包
     * @param path_arr 文件数组   
     *                  file_path 文件路径  
     *                  file_name 文件名称，带后缀 
     * @param zip_path 压缩包路径
     * @param zip_name 压缩包名称
     * @param del_past_file_times 删除过期压缩包时间，单位秒，若传空，则不删除
     */
    public function addPackage($path_arr, $zip_path, $zip_name, $del_past_file_times = 60)
    {
        $file_addr = public_path('uploads'); //压缩包文件夹路径
        if (empty($zip_path)) {
            $zip_path =  '/zip_downloads_dir/'; //压缩包路径
        }
        $zip_full_path =  $file_addr . $zip_path . $zip_name . '.zip'; //压缩包路径

        //先删除过期文件,有效期1个小时
        if ($del_past_file_times) del_past_file($file_addr . $zip_path, $del_past_file_times);

        if (!file_exists($zip_full_path)) {
            $address_dir = dirname($zip_full_path);
            //创建目录
            if (!file_exists($address_dir)) {
                mkdir($address_dir, 0777);
            }
            $zip = new \ZipArchive();
            $res = $zip->open($zip_full_path, \ZipArchive::CREATE);
            if ($res === TRUE) {
                foreach ($path_arr as $key => $val) {
                    //这里将服务器上的文件添加到下载内容中，并重新赋值下载zip文件内该文件的路径
                    if (!empty($val['file_path'])) $zip->addFile($file_addr . '/' . $val['file_path'], $val['file_name']); //第二个参数是文件名   图片
                    if (!empty($val['voice_path'])) $zip->addFile($file_addr . '/' . $val['voice_path'], $val['voice_name']); //第二个参数是文件名   音频
                    if (!empty($val['video_path'])) $zip->addFile($file_addr . '/' . $val['video_path'], $val['video_name']); //第二个参数是文件名   视频
                    if (!empty($val['pdf_path'])) $zip->addFile($file_addr . '/' . $val['pdf_path'], $val['pdf_name']); //第二个参数是文件名    pdf 文件
                    if (!empty($val['img_path'])) $zip->addFile($file_addr . '/' . $val['img_path'], $val['img_name']); //第二个参数是文件名    pdf 文件
                    if (!empty($val['pdf_works_path'])) $zip->addFile($file_addr . '/' . $val['pdf_works_path'], $val['pdf_works_name']); //第二个参数是文件名    pdf 文件
                    if (!empty($val['authorizations_path'])) $zip->addFile($file_addr . '/' . $val['authorizations_path'], $val['authorizations_name']); //第二个参数是文件名    pdf 文件
                    if (!empty($val['promise_path'])) $zip->addFile($file_addr . '/' . $val['promise_path'], $val['promise_name']); //第二个参数是文件名    pdf 文件
                }
            } else {
                //return "压缩资源准备失败,请联系开发人员";
                return false;
            }
            $zip->close();
        }
        //  return str_replace($file_addr, '',  $zip_full_path); //去掉前缀
        return $zip_full_path; //去掉前缀
    }
}
