<?php

namespace App\Http\Controllers;

use Kkokk\Poster\PosterManager;
use Kkokk\Poster\Exception\PosterException;


/**
 * 滑动验证码  composer require kkokk/poster
 */
class SlideVerifyController
{

    # 滑块自定义参数
    public $slide_params = [
        'src'           => '',  // 背景图片，尺寸 340 * 191
        'im_width'      => 340, // 画布宽度
        'im_height'     => 251, // 画布高度
        'bg_width'      => 340, // 背景宽度
        'bg_height'     => 191, // 背景高度
        'slider_width'  => 50,  // 滑块宽度
        'slider_height' => 50,  // 滑块高度
        'slider_border' => 2,   // 滑块边框
    ];
    public $type = 'slider'; //验证码方式

    /**
     * 获取滑动验证码参数
     * 内部使用了 laravel 的 cache 缓存，返回的是图片的 base64 、 缓存key 、滑块高度
     * @param string $type   验证码类型
     * @param array  $slide_params 验证码自定义参数
     * @return arary
     */
    public function getCaptcha()
    {
        $bg = mt_rand(1, 8);
        if (is_dir(public_path('uploads') . '/default/slide_verify')) {
            $file_bg = public_path('uploads') . '/default/slide_verify/bg/' . $bg . '.png';
        } else {
            $file_bg = public_path('uploads') . '/slide_verify/bg/' . $bg . '.png';
        }
        $this->slide_params['src'] = $file_bg; //设置自定义背景图片
        try {
            $data = PosterManager::Captcha()->type($this->type)->config($this->slide_params)->get();

            return response()->json(['code' => 200, 'msg' => '获取成功', 'content' => $data]);
        } catch (PosterException $e) {
            return response()->json(['code' => 201, 'msg' => $e->getMessage()]);
        }
    }
    /** 
     * 验证滑动验证码  （外部使用，实际未使用）
     * 前端根据相关滑块操作进行处理, 返回x坐标，返回 true 则验证成功
     * @param string     $key_str     缓存key
     * @param string|int $captcha   前端传回来的x坐标
     * @param int        $leeway  误差值
     * @return boolean
     */
    public function checkCaptcha()
    {
        $key_str = request()->key_str;
        $captcha = request()->captcha;

        if (empty($key_str) || empty($captcha))  return response()->json(['code' => 201, 'msg' => '参数错误']);

        try {
            $res = PosterManager::Captcha()->type($this->type)->check($key_str, $captcha, 1);
            if ($res) {
                return response()->json(['code' => 200, 'msg' => '验证成功', 'content' => $res]);
            }
            return response()->json(['code' => 202, 'msg' => '验证失败']);
        } catch (PosterException $e) {
            return response()->json(['code' => 201, 'msg' => $e->getMessage()]);
        }
    }
}
