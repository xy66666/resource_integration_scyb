<?php

namespace App\Http\Controllers;


use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * 生成二维码
 * composer require simplesoftwareio/simple-qrcode 1.3.*
 * 使用 1.*  2.0 版本即可，使用 大于2.0 以上的会报各种错误
 */
class QrCodeController extends Controller
{
    /**
     * 生成二维码
     * @param $content 二维码内容
     * @param $is_save 是否保存二维码  默认fasle
     * @param $size 二维码大小
     * @param $color 二维码颜色 数组
     * @param $margin 二维码边距
     * @param $error_level 容错级别 说明
                        L 7% 的字节码恢复率.
                        M 15% 的字节码恢复率.
                        Q 25% 的字节码恢复率.
                        H 30% 的字节码恢复率.
                        容错级别越高,二维码里能存储的数据越少.
     *  encoding('UTF-8') 支持中文
     */
    public function setQr($content, $is_save = false, $size = 360, $margin = 1, $color = [0, 0, 0], $error_level = 'H')
    {
        if ($is_save) {
            //二维码保存路径
            $save_dir = public_path('uploads/');
            $address = 'qr/' . date('Y-m-d') . '/' . uniqid() . date('ymdhis') . '.png'; //保存地址
            $address_all =   $save_dir . '/' . $address;

            $address_dir = dirname($address_all);

            if (!file_exists($address_dir)) {
                mkdir($address_dir, 0777, true); //true  表示递归创建
            }

            QrCode::format('png')->encoding('UTF-8')->color($color[0], $color[1], $color[2])->margin($margin)->size($size)->errorCorrection($error_level)->generate($content, $address_all);
            return $address;
        } else {
            //删除过期的临时文件
            $filedir = public_path('uploads') . '/temp_qr/';
            del_past_imgs($filedir, 7200); //删除过期图片

            //二维码保存路径
            $save_dir = public_path('uploads/');
            $address = 'temp_qr/' . uniqid() . date('ymdhis') . '.png'; //保存地址
            $address_all =   $save_dir . '/' . $address;

            $address_dir = dirname($address_all);

            if (!file_exists($address_dir)) {
                mkdir($address_dir, 0777, true); //true  表示递归创建
            }

            QrCode::format('png')->encoding('UTF-8')->color($color[0], $color[1], $color[2])->margin($margin)->size($size)->errorCorrection($error_level)->generate($content, $address_all);
            return $address;
        }


        //最早不保存二维码使用此代码，但是有时候显示不出来
        return QrCode::encoding('UTF-8')->color($color[0], $color[1], $color[2])->margin($margin)->size($size)->errorCorrection($error_level)->generate($content);
    }



    /**
     * 获取二维码 qr_code
     * @param  $table_name  表名
     */
    public function getQrCode($table_name, $filed = 'qr_code')
    {
        $prefix = $this->getQrCodePrefix($table_name);
        $i = 10;
        while ($i) {
            $qr_code = $prefix . get_rnd_upper_string(8); //固定前2位
            //判断qr_code是否重复
            $qr_code_exist = DB::table($table_name)->where($filed, $qr_code)->first();
            if ($qr_code_exist) {
                $i--;
            } else {
                return $qr_code;
            }
        }
        return false;
    }

    /**
     * 根据表名获取 二维码 前缀
     * @param $table_name
     */
    public function getQrCodePrefix($table_name)
    {
        $data = [
            'user_info' => '10', //用户
            'activity' => '11', //活动      扫码签到   内部扫码才有效
            'reservation' => '12', //预约   扫码签到   内部扫码才有效
            'reservation_seat' => '13', //预约的座位（座位号单独一个二维码）   扫码签到，签退   内部扫码才有效
            /*   'ebook' => '14', //电子书
            'audio_ebook' => '15', //有声电子书 */
            'goods_order' => '16', //商品兑换
            'answer_activity_unit' => '17', //阅享活动单位   微信扫码进入活动  内部扫码无效
            'answer_activity' => '18', //阅享活动      微信扫码进入活动   内部扫码无效
            'code_guide_exhibition' => '19', //扫码导游-展览  微信扫码进入活动    怎么配置内部扫码进入相应界面
            'code_guide_production' => '20', //扫码导游-展览作品  微信扫码进入活动   怎么配置内部扫码进入相应界面
            'picture_live' => '21', //图片直播活动       微信扫码进入活动   内部扫码无效
            'code_place' => '22', //扫码借   扫码调用不同接口   内部扫码才有效
            'study_room_reservation' => '23', //空间预约
            'video_live' => '24', //视频直播
            'reservation_apply' => '25', //预约领取码
        ];
        return $data[$table_name];
    }


    /**
     * 获取对应活动链接 //小程序统一跳转链接  https://eco.cdbottle.com/resource_integration_promote/applet?from=readshare_activity&token=18I0Z70Z9S
     */
    public function getAppletDataVisitUrl()
    {
        $domain_name = config('other.project_name') ? $this->getDomainName() . '/' . config('other.project_name') : $this->getDomainName();
        $url = $domain_name . '/applet?from=readshare_activity&token='; //活动二维码的链接  扫码跳转活动详情

        return $url;
    }
}
