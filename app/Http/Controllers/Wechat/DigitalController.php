<?php

namespace App\Http\Controllers\Wechat;

use App\Http\Controllers\Controller;
use App\Models\AppInviteCodeBehavior;
use App\Models\Digital;
use App\Models\DigitalAccessNum;
use App\Models\DigitalType;
use App\Models\OtherAccessNum;
use App\Validate\DigitalValidate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * 数字资源管理
 */
class DigitalController extends Controller
{
    public $model = null;
    public $typeModel = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new Digital();
        $this->typeModel = new DigitalType();
        $this->validate = new DigitalValidate();
    }

    /**
     * 数字资源类型
     */
    public function typeList()
    {
        $condition[] = ['is_del', '=', 1];
        return $this->typeModel->getFilterList(['id', 'type_name'], $condition);
    }


    /**
     * 列表
     * @param page int 当前页
     * @param limit int 分页大小
     * @param type_id int 0或空标识全部  类型id
     * @param keywords string 搜索关键词(名称)
     */
    public function lists()
    {
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $keywords = $this->request->keywords;
        $type_id = $this->request->type_id;

        $res = $this->model->lists(['id', 'title', 'img', 'url', 'type_id', 'sort', 'create_time'],  $keywords, $type_id, 1, null, null, 'sort desc', $limit);

        //添加数字资源访问量（应用）
        $otherAccessNumModel = new OtherAccessNum();
        $otherAccessNumModel->add(1);

        //增加行为分析
        // $appInviteCodeBehaviorModel = new AppInviteCodeBehavior();
        // $appInviteCodeBehaviorModel->add(10);

        if (empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }
        $res = $this->disPageData($res);
        return $this->returnApi(200, "获取成功", true, $res);
    }

    /**
     * 数字资源添加浏览量
     * @param id 数字资源id
     */
    public function addBrowseNum()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('add_browse_num')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $id = $this->request->id;
        $res = $this->model->where('id', $id)->first();
        if (empty($res)) {
            return $this->returnApi(201, '参数错误');
        }

        DB::beginTransaction();
        try {
            $res->browse_num = $res->browse_num + 1;
            $res->save();

            $digitalAccessNum = new DigitalAccessNum();
            $digitalAccessNum->dayBrowse($id);

            DB::commit();
            return $this->returnApi(200, '操作成功', true);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->returnApi(202, $e->getMessage());
        }
    }
}
