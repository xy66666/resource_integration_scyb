<?php

namespace App\Http\Controllers\Wechat;

use App\Http\Controllers\ScoreRuleController;
use App\Models\Activity;
use App\Models\ActivityApply;
use App\Models\ActivityTag;
use App\Models\ActivityType;
use App\Models\AppInviteCodeBehavior;
use App\Models\OtherAccessNum;
use App\Models\UserLibraryInfo;
use App\Models\UserViolate;
use App\Validate\ActivityValidate;
use Exception;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * 活动管理
 */
class ActivityController extends CommonController
{
    public $model = null;
    public $applyModel = null;
    public $validate = null;
    public $score_type = 4;

    public function __construct()
    {
        parent::__construct();

        $this->model = new Activity();
        $this->applyModel = new ActivityApply();
        $this->validate = new ActivityValidate();
    }

    /**
     * 活动类型列表
     */
    public function typeList()
    {
        $condition[] = ['is_del', '=', 1];
        $typeModel = new ActivityType();
        return $typeModel->getFilterList(['id', 'type_name'], $condition);
    }

    /**
     * 根据时间获取活动 展示活动日历
     * @param month 月份
     */
    public function getActivityCalendar()
    {
        $month = $this->request->month;
        if (empty($month)) {
            return $this->returnApi(201, '参数错误');
        }
        //活动
        $data =  $this->model->getActListByMonth($month);

        if ($data) {
            return $this->returnApi(200, '获取成功', true, $data);
        }
        return $this->returnApi(203, '暂无数据');
    }


    /**
     * 根据时间获取活动 展示活动日历（未使用）
     * @param start_time 开始时间
     * @param end_time 结束时间
     */
    public function getActivityCalendarByTime()
    {
        $start_time = $this->request->start_time;
        $end_time = $this->request->end_time;
        if (empty($start_time) || empty($end_time)) {
            return $this->returnApi(201, '参数错误');
        }
        //活动
        $data =  $this->model->getActListByTime($start_time, $end_time);
        if ($data) {
            return $this->returnApi(200, '获取成功', true, $data);
        }
        return $this->returnApi(203, '暂无数据');
    }

    /**
     * 获取热门活动
     */
    public function getHotActivity()
    {
        //活动
        $data =  $this->model->getActRecomList(3, null, true);
        if ($data) {
            return $this->returnApi(200, '获取成功', true, $data);
        }
        return $this->returnApi(203, '暂无数据');
    }

    /**
     * 获取推荐，每个类型推荐
     */
    public function getActivityRecom()
    {
        //活动
        $typeModelObj = new ActivityType();
        $condition[] = ['is_del', '=', 1];
        $type = $typeModelObj->getFilterList(['id', 'type_name'], $condition);
        if ($type['code'] != 200) {
            return $type;
        }
        $data = [];
        $i = 0;
        foreach ($type['content'] as $key => $val) {
            $res =  $this->model->getActRecomList(3, $val['id']);
            if ($res) {
                $data[$i]['id'] = $val['id'];
                $data[$i]['type_name'] = $val['type_name'];
                $data[$i]['data'] = $res;

                $i++;
            }
        }
        if ($data) {
            return $this->returnApi(200, '获取成功', true, $data);
        }
        return $this->returnApi(203, '暂无数据');
    }
    /**
     * 列表
     * @param page int 当前页
     * @param limit int 分页大小
     * @param type_id int 活动列表   0 或空标识全部
     * @param keywords string 搜索关键词(名称)
     */
    public function lists()
    {
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $keywords = $this->request->keywords;
        $type_id = $this->request->type_id;

        $res = $this->model->lists($keywords, $type_id, null, null, null, null, null, null, 1, 1, $limit);

        //添加活动访问量（应用）
        $otherAccessNumModel = new OtherAccessNum();
        $otherAccessNumModel->add(3);

        if (empty($res['data'])) {
            return $this->returnApi(203, '暂无数据');
        }

        $res = $this->disPageData($res);
        foreach ($res['data'] as $key => $val) {
            $res['data'][$key]['status'] = $this->model->getActListState($val);
            $res['data'][$key]['tag_info'] = ActivityTag::getTagNameByTagId($val['tag_id']);
            //   $res['data'][$key]['type_name'] = isset($val['con_type']['type_name']) ? $val['con_type']['type_name'] : '';

            if ($val['is_apply'] == 2) {
                $res['data'][$key]['number'] = null;
                $res['data'][$key]['apply_number'] = null;
                $res['data'][$key]['apply_start_time'] = null;
                $res['data'][$key]['apply_end_time'] = null;
            }
            $res['data'][$key]['start_time'] = date('Y-m-d H:i', strtotime($val['start_time']));
            $res['data'][$key]['end_time'] = date('Y-m-d H:i', strtotime($val['end_time']));
            $res['data'][$key]['type_name'] = !empty($val['con_type']['type_name']) ?  $val['con_type']['type_name'] : '';

            unset($res['data'][$key]['con_type']);
        }

        //增加行为分析
        // $appInviteCodeBehaviorModel = new AppInviteCodeBehavior();
        // $appInviteCodeBehaviorModel->add(2);

        return $this->returnApi(200, '获取成功', true, $res);
    }


    /**
     * 获取活动详情 + 我的活动详情
     * @param $authorrization 用户 token    例：Bearer 1可选
     * @param act_id  活动id
     * @param apply_id  报名id
     */
    public function detail()
    {
        //处理逾期未签到的数据
        $this->applyModel->checkApplyStatus();

        $act_id = $this->request->input('act_id');
        $apply_id = $this->request->input('apply_id');
        $user_id = $this->request->user_info['id'];
        $account_id = $this->request->user_info['account_id'];

        if (empty($apply_id) && empty($act_id)) {
            return $this->returnApi(201, '参数错误');
        }
        if (empty($act_id) && !empty($apply_id) && empty($user_id)) {
            return $this->returnApi(201, '参数错误');
        }

        if (empty($act_id) || !empty($apply_id)) {
            $apply_info = $this->applyModel->where('id', $apply_id)->first();
            if (empty($apply_info)) {
                return $this->returnApi(201, '参数错误');
            }
            $act_id = $apply_info->act_id;
        }

        if (empty($apply_id) && !empty($user_id)) {
            $apply_info = $this->applyModel->where('act_id', $act_id)->where('user_id', $user_id)->orderByDesc('id')->first();
            if (!empty($apply_info)) {
                $apply_id = $apply_info->id;
            }
        }

        $res = $this->model->from($this->model->getTable() . ' as a')
            ->select([
                't.type_name',
                't.id as type_id',
                'a.tag_id',
                'a.id',
                'a.cancel_end_time',
                'a.is_check',
                'a.is_qr',
                'a.is_continue',
                'a.is_reader',
                'a.title',
                'img',
                'apply_start_time',
                'apply_end_time',
                'start_time',
                'end_time',
                'start_age',
                'end_age',
                'astrict_sex',
                'is_real',
                'real_info',
                'tel',
                'contacts',
                'intro',
                'a.create_time',
                'province',
                'city',
                'district',
                'street',
                'address',
                'number',
                'apply_number',
                'a.create_time',
                'is_long',
                'everyday_start_time',
                'everyday_end_time',
                'lon',
                'lat',
                'is_continue',
                'is_apply',
                'a.link'
            ])
            ->join('activity_type as t', 't.id', '=', 'a.type_id')
            ->where('t.is_del', 1)
            ->where('a.is_del', 1)
            ->where('a.is_play', 1)
            ->where('a.id', $act_id)
            ->first();

        if ($res) {
            $res = $res->toArray();
            $res['status'] = $this->model->getActListState($res); //列表状态
            $res['user_status'] = $this->model->getActDetailState($res, $user_id, $apply_id); //用户状态

            $real_info_arr = $this->model->getActivityApplyParam(); //获取所有的数据
            $res['real_info'] = $this->getRealInfoArray($res['real_info'], $real_info_arr);

            if ($res['is_apply'] == 2) {
                $res['number'] = null;
                $res['apply_number'] = null;
            }

            //获取报名信息
            if (!empty($apply_info) &&  $res['is_apply'] == 1) {
                $res['apply_info'] = $apply_info;
            } else {
                //添加浏览量
                $this->model->where('id', $act_id)->increment('browse_num'); //自己从个人中心看自己的详情不增加
            }
            $res['apply_start_time'] = $res['is_apply'] == 1 && $res['apply_start_time'] ? date('Y-m-d H:i', strtotime($res['apply_start_time'])) : null;
            $res['apply_end_time'] = $res['is_apply'] == 1 &&  $res['apply_end_time'] ? date('Y-m-d H:i', strtotime($res['apply_end_time'])) : null;
            $res['start_time'] = $res['start_time'] ? date('Y-m-d H:i', strtotime($res['start_time'])) : null;
            $res['end_time'] = $res['end_time'] ? date('Y-m-d H:i', strtotime($res['end_time'])) : null;

            $res['apply_end_times'] = date('Y/m/d H:i:s', strtotime($res['apply_end_time'])); //处理苹果手机倒计时
            list($res['time']['apply_start_time_date'], $res['time']['apply_start_time_time'], $res['time']['apply_start_time_stage']) = $this->switchTime($res['apply_start_time']);
            list($res['time']['apply_end_time_date'], $res['time']['apply_end_time_time'], $res['time']['apply_end_time_stage']) = $this->switchTime($res['apply_end_time']);
            list($res['time']['start_time_date'], $res['time']['start_time_time'], $res['time']['start_time_stage']) = $this->switchTime(date('Y-m-d', strtotime($res['start_time'])) . ' ' . $res['everyday_start_time']);
            list($res['time']['end_time_date'], $res['time']['end_time_time'], $res['time']['end_time_stage']) = $this->switchTime(date('Y-m-d', strtotime($res['end_time'])) . ' ' . $res['everyday_end_time']);

            $res['time']['start_time_time'] = $res['everyday_start_time'];
            $res['time']['end_time_time'] = $res['everyday_end_time'];

            $res['tag_info'] = ActivityTag::getTagNameByTagId($res['tag_id']);

            /**
             * 判断是否有足够积分参加活动
             */
            //需要报名才返回
            if ($res['is_apply'] == 1 && !empty($user_id)) {

                $res['is_can_make'] = true;
                if (config('other.is_need_score') && !empty($account_id) && $res['is_reader'] == 1) {
                    $scoreRuleObj = new ScoreRuleController();
                    $score_status = $scoreRuleObj->checkScoreStatus($this->score_type, $user_id, $account_id);
                    if ($score_status['code'] == 202 || $score_status['code'] == 203) $res['is_can_make'] = '积分不足不能参加活动';
                }
                $apply_info = $this->applyModel->getApplyInfo($user_id, $act_id);
                if (!empty($apply_info)) {
                    if ($apply_info['status'] == 1 || $apply_info['status'] == 4) {
                        $res['is_can_make'] = '您已报名，请勿重复报名';
                    }
                    if (($apply_info['status'] == 2 || $apply_info['status'] == 3) && $res['is_continue'] == 2) {
                        $res['is_can_make'] = '取消或拒绝后，不能再次报名';
                    }
                }
                /*检查是否违规*/
                $validateModel = new UserViolate();
                $isViolate = $validateModel->checkIsViolate($user_id, 'activity');
                if ($isViolate) {
                    $res['is_can_make'] = '已达到违规次数上限,不能参加活动';
                }

                //判断是否可以取消
                $is_can_cancel = $this->applyModel->isCanCancel($res, $apply_info);
                $res['apply_info']['is_can_cancel'] = $is_can_cancel === true ? true : false;

                $res['cancel_end_time'] = date('Y-m-d H:i:s', strtotime('-' . $res['cancel_end_time'] . ' min', strtotime($res['apply_end_time'])));
            }

            return $this->returnApi(200, '获取成功', true, $res);
        }
        return $this->returnApi(203, '暂无数据');
    }

    /**
     * 活动报名
     * @param token  用户token  必选
     * @param act_id  活动id
     * @param username  姓名
     * @param id_card  身份证
     * @param tel  电话号码
     * @param reader_id  读者证
     * @param img  头像
    //  * @param accessory  附件
    //  * @param accessory_name  附件名
     * @param remark  备注
     * @param unit 单位
     * @param age  年龄
     * @param sex  性别  1 男  2 女
     *
     * @param school  学校名称
     * @param grade  年级
     * @param class_grade  班级
     * @param wechat_number  微信号
     */
    public function apply()
    {
        //处理逾期未签到的数据
        $this->applyModel->checkApplyStatus();

        //增加验证场景进行验证
        if (!$this->validate->scene('web_apply')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $user_id = $this->request->user_info['id'];
        $account_id = $this->request->user_info['account_id'];
        $act_id = $this->request->act_id;


        //限制用户多次点击
        $key = md5('activity_apply' . $user_id . $act_id . request()->ip());
        $oldKey =  Cache::get($key); //没有缓存返回false
        if ($oldKey) {
            return $this->returnApi(201, "您操作的太频繁了，请稍后重试");
        } else {
            Cache::put($key, 1, 3);
        }
        /*检查是否违规*/
        $validateModel = new UserViolate();
        $isViolate = $validateModel->checkIsViolate($user_id, 'activity');
        if ($isViolate) {
            return $this->returnApi(201, '已达到违规次数上限,不能参加活动');
        }

        //查询后，用于数据判断
        $act_info = $this->model->where('is_del', 1)->where('is_play', 1)->find($act_id);
        if (empty($act_info)) {
            return $this->returnApi(202, '活动不存在');
        }
        if ($act_info['is_apply'] == 2) {
            return $this->returnApi(202, '此活动无需报名');
        }
        if (!empty($act_info['number']) && $act_info['apply_number'] >= $act_info['number']) {
            return $this->returnApi(202, '名额已满，不允许报名');
        }
        if ($act_info['apply_end_time'] < date('Y-m-d H:i:s')) {
            return $this->returnApi(202, '活动报名已结束');
        }

        //判断是否需要绑定读者证
        if ($act_info->is_reader == 1) {
            //判断读者证号密码是否正确 和是否绑定读者证
            $userLibraryInfoModel = new UserLibraryInfo();
            $account_lib_info = $userLibraryInfoModel->checkAccountPwdIsNormal($user_id, $account_id);
            if (is_string($account_lib_info)) {
                return $this->returnApi(204, $account_lib_info);
            }
        }

        //判断积分是否满足条件
        if (config('other.is_need_score') === true  && $act_info->is_reader == 1) {
            $scoreRuleObj = new ScoreRuleController();
            $score_status = $scoreRuleObj->checkScoreStatus($this->score_type, $user_id, $account_id);
            if ($score_status['code'] == 202 || $score_status['code'] == 203) {
                return $this->returnApi(202, $score_status['msg']); //'积分不足无法参加活动'
            }
        }

        DB::beginTransaction();
        try {
            //再次查询，用于加锁判断
            $act_info = $this->model->where('is_del', 1)->where('is_play', 1)->lockForUpdate()->find($act_id);
            if (empty($act_info)) {
                throw new Exception('活动不存在');
            }
            $apply_info = $this->applyModel->getApplyInfo($user_id, $this->request->act_id);
            if (!empty($apply_info)) {
                if ($apply_info['status'] == 1) {
                    throw new Exception('您已报名，请勿重复报名');
                }
                if ($apply_info['status'] == 4) {
                    throw new Exception('您的报名信息还在审核中，请勿重复报名');
                }
                if (($apply_info['status'] == 2 || $apply_info['status'] == 3) && $act_info['is_continue'] == 2) {
                    throw new Exception('取消或拒绝后，不能再次报名');
                }
            }

            $state = $this->model->getActDetailState($act_info, $user_id);
            if ($state != 2) {
                $error_name =  $this->model->getStatusName($state);
                throw new Exception($error_name);
            }

            $where = [];
            if ($act_info->is_real == 1) {
                if (!empty($act_info->real_info)) {
                    $real_info = explode('|', $act_info->real_info);
                    $where = $this->model->checkApplyParam($act_info, $real_info, $this->request->all());
                }
            }

            // $where['accessory_name'] = $accessory_name;
            $where['user_id'] = $user_id;
            $where['account_id'] = !empty($account_id) ? $account_id : 0;
            $where['act_id'] = $this->request->act_id;
            $where['status'] = $act_info->is_check == 1 ? 4 : 1;
            $where['is_violate'] = 1;
            $where['score'] = !empty($score_status['score_info']['score']) ? $score_status['score_info']['score'] : 0;
            $this->applyModel->add($where);
            $act_info->apply_number = ++$act_info->apply_number;
            $act_info->save();

            $msg = $act_info->is_check == 1 ? "提交" : '报名';
            /*消息推送*/
            $system_id = $this->systemAdd('活动报名', $user_id, $account_id, 3, intval($this->applyModel->id), '您申请的活动：【' . $act_info->title . '】' . $msg . '成功！');

            //处理积分
            if (!empty($score_status['code']) && $score_status['code'] == 200) {
                $scoreRuleObj->scoreChange($score_status, $user_id, $account_id, $system_id); //添加积分消息
            }

            DB::commit();
            return $this->returnApi(200, $msg . '成功', true);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->returnApi(202, $e->getMessage());
        }
    }


    /**
     * 我的活动列表
     * @param token  用户token
     * @param $page  页数  默认为 1
     * @param $limit  条数  默认为 10
     */
    public function _myActList()
    {
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $user_id = $this->request->user_info['id'];

        $res = $this->model
            ->select('id', 'tag_id', 'type_id', 'title', 'img', 'intro', 'is_apply', 'apply_number', 'number', 'address', 'province', 'city', 'district', 'street', 'contacts', 'tel', 'start_time', 'end_time', 'apply_start_time', 'apply_end_time', 'is_play')
            ->with(['conType' => function ($query) {
                $query->select('id', 'type_name');
            }, 'conApply' => function ($query) {
                $query->select('id', 'user_id', 'act_id', 'account_id', 'status', 'reason', 'sign_num', 'username', 'id_card', 'tel', 'reader_id', 'img', 'unit', 'age', 'sex', 'school', 'grade', 'class_grade', 'wechat_number', 'remark', 'is_violate', 'violate_reason', 'create_time');
            }])
            ->whereHas('conApply', function ($query) use ($user_id) {
                $query->where('user_id', $user_id);
            })
            ->where('is_play', 1)
            ->where('is_del', 1)
            ->orderByDesc('end_time')
            ->paginate($limit)
            ->toArray();
        if (empty($res['data'])) {
            return $this->returnApi(203, '暂无数据');
        }

        $res = $this->disPageData($res);
        $i = 0;
        foreach ($res['data'] as $key => $val) {
            foreach ($val['con_apply'] as $k => $v) {
                $res['data'][$i] = $val;
                $res['data'][$i]['con_apply'] = $v;

                $res['data'][$i]['status'] = $this->model->getActListState($val); //列表状态
                $res['data'][$i]['user_status'] = $this->model->getActDetailState([
                    'id' => $val['id'],
                    'end_time' => $val['end_time'],
                    'apply_start_time' => $val['apply_start_time'],
                    'apply_end_time' => $val['apply_end_time'],
                    'apply_number' => $val['apply_number'],
                    'is_apply' => $val['is_apply'],
                    'is_long' => $val['is_long'],
                    'status' => $v['status'],
                ], $user_id); //个人中心列表，使用详情状态
                $res['data'][$i]['tag_info'] = ActivityTag::getTagNameByTagId($val['tag_id']);

                $i++;
            }
        }

        return $this->returnApi(200, '获取成功', true, $res);
    }

    /**
     * 我的活动列表
     * @param token  用户token
     * @param $page  页数  默认为 1
     * @param $limit  条数  默认为 10
     */
    public function myActList()
    {
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $user_id = $this->request->user_info['id'];

        $res = $this->applyModel->from('activity_apply as p')
            ->select(
                'a.id',
                'a.tag_id',
                'a.type_id',
                'a.title',
                'a.img',
                'a.intro',
                'a.is_apply',
                'a.is_long',
                'a.apply_number',
                'a.number',
                'a.address',
                'a.province',
                'a.city',
                'a.district',
                'a.street',
                'contacts',
                'a.tel',
                'a.start_time',
                'a.end_time',
                'a.apply_start_time',
                'a.apply_end_time',
                'a.is_play',
                'a.is_continue',
                'a.real_info',
                'a.cancel_end_time',
                'a.is_check',
            )
            ->join('activity as a', 'a.id', '=', 'p.act_id')
            ->where('p.user_id', $user_id)
            ->where('a.is_play', 1)
            ->where('a.is_del', 1)
            ->orderByDesc('p.id')
            ->groupBy('p.act_id')
            ->paginate($limit)
            ->toArray();


        if (empty($res['data'])) {
            return $this->returnApi(203, '暂无数据');
        }

        $res = $this->disPageData($res);
        $activityApplyModel = new ActivityApply();

        $real_info_arr = $this->model->getActivityApplyParam(); //获取所有的数据
        foreach ($res['data'] as $key => &$val) {
            //重新获取最新一条的报名信息，分组返回的是第一条报名信息
            $apply_info = $activityApplyModel->getApplyInfo($user_id, $val['id']);

            $res['data'][$key]['tag_info'] = ActivityTag::getTagNameByTagId($val['tag_id']);
            $res['data'][$key]['con_apply']['id'] = $apply_info['id'];
            $res['data'][$key]['con_apply']['user_id'] = $apply_info['user_id'];
            $res['data'][$key]['con_apply']['act_id'] = $apply_info['act_id'];
            $res['data'][$key]['con_apply']['account_id'] = $apply_info['account_id'];
            $res['data'][$key]['con_apply']['status'] = $apply_info['status'];
            $res['data'][$key]['con_apply']['reason'] = $apply_info['reason'];
            $res['data'][$key]['con_apply']['sign_num'] = $apply_info['sign_num'];
            $res['data'][$key]['con_apply']['username'] = $apply_info['username'];
            $res['data'][$key]['con_apply']['id_card'] = $apply_info['id_card'];
            $res['data'][$key]['con_apply']['tel'] = $apply_info['tel'];
            $res['data'][$key]['con_apply']['reader_id'] = $apply_info['reader_id'];
            $res['data'][$key]['con_apply']['img'] = $apply_info['img'];
            $res['data'][$key]['con_apply']['is_violate'] = $apply_info['is_violate'];
            $res['data'][$key]['con_apply']['violate_reason'] = $apply_info['violate_reason'];
            $res['data'][$key]['con_apply']['remark'] = $apply_info['remark']; //备注

            $res['data'][$key]['con_apply']['school'] = $apply_info['school'];
            $res['data'][$key]['con_apply']['grade'] = $apply_info['grade'];
            $res['data'][$key]['con_apply']['class_grade'] = $apply_info['class_grade'];
            $res['data'][$key]['con_apply']['wechat_number'] = $apply_info['wechat_number'];

            $res['data'][$key]['con_apply']['unit'] = $apply_info['unit']; //附件
            $res['data'][$key]['con_apply']['create_time'] = date('Y-m-d H:i:s', strtotime($apply_info['create_time']));
            //  $res['data'][$key]['con_apply']['age'] = $apply_info['age'] ? $apply_info['age'] : get_user_age_by_id_card($apply_info['id_card']);
            //   $res['data'][$key]['con_apply']['sex'] = $apply_info['sex'] ? $apply_info['sex'] : get_user_sex_by_id_card($apply_info['id_card']) == 1 ? '男' : '女';
            $res['data'][$key]['con_apply']['age'] = $apply_info['age'];
            $res['data'][$key]['con_apply']['sex'] = $apply_info['sex'] == 1 ? '男' : ($apply_info['sex'] == 2 ? '女' : '');


            $res['data'][$key]['status'] = $this->model->getActListState($val); //列表状态
            $res['data'][$key]['user_status'] = $this->model->getActDetailState($val, $user_id, $apply_info['id']); //个人中心列表，使用详情状态

            $res['data'][$key]['real_info'] = $this->getRealInfoArray($val['real_info'], $real_info_arr);

            //判断是否可以取消
            $is_can_cancel = $this->applyModel->isCanCancel($val, $apply_info);
            $res['data'][$key]['con_apply']['is_can_cancel'] = $is_can_cancel === true ? true : false;

            //是否能显示签到二维码
            $is_can_show_qr = $this->applyModel->isCanShowQr($user_id, null, $apply_info);
            $res['data'][$key]['con_apply']['is_can_show_qr'] = $is_can_show_qr === true ? true : false;
        }
        return $this->returnApi(200, '获取成功', true, $res);
    }

    /**
     * 取消活动
     * @param token  用户token
     * @param act_id int 活动id
     */
    public function cancel()
    {
        if (!$this->validate->scene('web_cancel')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $act_id = request()->act_id;
        $user_id = $this->request->user_info['id'];
        $account_id = $this->request->user_info['account_id'];

        //限制用户多次点击
        $key = md5('activity_cancel' . $user_id . $act_id . request()->ip());
        $oldKey =  Cache::get($key); //没有缓存返回false
        if ($oldKey) {
            return $this->returnApi(201, "您操作的太频繁了，请稍后重试");
        } else {
            Cache::put($key, 1, 3);
        }
        // 启动事务
        DB::beginTransaction();
        try {
            $activity = $this->model->where('id', $act_id)->where('is_del', 1)->lockForUpdate()->first();

            if (!$activity) {
                throw new Exception("参数传递错误");
            }

            $activity_apply = $this->applyModel
                ->where('user_id', $user_id)
                ->where('act_id', $act_id)->orderByDesc('create_time')->first();

            if (!$activity_apply) {
                throw new Exception("参数传递错误");
            }

            $is_can_cancel = $this->applyModel->isCanCancel($activity, $activity_apply);
            if ($is_can_cancel !== true) {
                throw new Exception($is_can_cancel);
            }

            $activity_apply->status = 2;
            $activity->apply_number = $activity->apply_number - 1;
            $activity_apply->save();
            $activity->save();

            /*消息推送*/
            $system_id = $this->systemAdd('取消报名活动', $user_id, $account_id, 4, intval($activity_apply->id), '您报名的活动：【' . $activity_apply->conActivity->title . '】取消活动报名成功');

            /**执行积分规则 */
            if (!empty($activity_apply->score)) {
                $scoreRuleObj = new ScoreRuleController();
                $score_msg = $scoreRuleObj->getScoreMsg($activity_apply->score);
                $scoreRuleObj->scoreReturn($this->score_type, $activity_apply->score, $activity_apply->user_id, $activity_apply->account_id, '取消活动报名，' . $score_msg . ' ' . abs($activity_apply->score) . ' 积分', $system_id, '取消活动报名');
            }

            DB::commit();
            return $this->returnApi(200, "取消成功", true);
        } catch (\Exception $e) {
            // 回滚事务
            DB::rollBack();
            return $this->returnApi(202, $e->getMessage());
        }
    }

    /**
     * 出示二维码
     * @param  $authorrization ：用户 token   必选
     * @param apply_id int 报名id
     */
    public function getApplyQr()
    {
        $user_id = $this->request->user_info['id'];
        $apply_id = $this->request->apply_id;

        $is_can_show_qr = $this->applyModel->isCanShowQr($user_id, $apply_id);
        if ($is_can_show_qr !== true) {
            return $this->returnApi(202, $is_can_show_qr);
        }

        $data['node'] = 'activity';
        $data['apply_id'] = $apply_id;
        //可以出示二维码
        $qr_url = $this->dataEncryptCreateQr($data, 2);
        if ($qr_url) {
            return $this->returnApi(200, "二维码生成成功", true, ['qr_url' => $qr_url]);
        }
        return $this->returnApi(202, '二维码生成失败，请重试！');
    }
}
