<?php

namespace App\Http\Controllers\Wechat;

use App\Http\Controllers\Controller;
use App\Models\News;
use App\Models\NewsCollect;
use App\Models\NewsType;
use App\Validate\NewsValidate;


/**
 * 前台新闻 
 */
class NewsController extends Controller
{
    public $model = null;
    public $typeModel = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new News();
        $this->typeModel = new NewsType();
        $this->validate = new NewsValidate();
    }


    /**
     * 新闻类型
     */
    public function typeList()
    {

        $condition[] = ['is_del', '=', 1];

        return $this->typeModel->getFilterList(['id', 'type_name', 'type'], $condition);
    }
    /**
     * 根据新闻类型获取新闻列表
     * @param type_id  新闻类型id  未传表示全部
     * @param keywords 关键字搜索
     * @param page 页数    默认第一页
     * @param limit 限制条数   默认 10 条
     */
    public function lists()
    {
        $type_id = $this->request->type_id;
        $keywords = $this->request->keywords;
        $limit = $this->request->input('limit', 10);
        $res = $this->model->lists($keywords, $type_id, null, null, $limit);

        //增加行为分析
        $appInviteCodeBehaviorModel = new  \App\Models\AppInviteCodeBehavior();
        $appInviteCodeBehaviorModel->add(15);

        if (empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }
        foreach ($res['data'] as $key => $val) {
            $res['data'][$key]['type_name'] = !empty($val['con_type']) ? $val['con_type']['type_name'] : '';
            $res['data'][$key]['content'] = str_replace('&nbsp;', '', strip_tags($val['content']));
            $res['data'][$key]['add_time'] = date('Y-m-d', strtotime($val['add_time']));
            $res['data'][$key]['img'] = $val['img'] ? explode('|', $val['img']) : null;
            unset($res['data'][$key]['create_time']);
            unset($res['data'][$key]['con_type']);
        }
        $res = $this->disPageData($res);
        return $this->returnApi(200, "获取成功", true, $res);
    }

    /**
     * 新闻详情
     * @param token  用户token  用户筛选用户是否收藏  
     * @param id  新闻id  
     */
    public function detail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $user_id = $this->request->user_info['id'];

        $res = $this->model->detail($this->request->id);

        if (empty($res)) {
            return $this->returnApi(203, "暂无数据");
        }
        $res->browse_num = ++$res->browse_num;
        $res->save(); //添加浏览量


        // //获取上一条
        // $last_data = $this->model->from($this->model->getTable() .' as n')->select('n.id','n.title')
        //     ->join('news_type as t', 't.id' ,'=' ,'n.type_id')
        //     ->where('n.is_del', 1)
        //     ->where('t.is_del', 1)
        //     ->where('n.type_id', $res['type_id'])
        //     ->where('n.id', '>', $this->request->id)
        //     ->orderBy('n.id')
        //     ->first();

        // //获取下一条
        // $next_data = $this->model->from($this->model->getTable() .' as n')->select('n.id','n.title')
        //     ->join('news_type as t', 't.id' ,'=' ,'n.type_id')
        //     ->where('n.is_del', 1)
        //     ->where('t.is_del', 1)
        //     ->where('n.type_id', $res['type_id']) 
        //     ->where('n.id', '<', $this->request->id)
        //     ->orderByDesc('n.id')
        //     ->first();

        $data = $res->toArray();
        // $data['last_data'] = $last_data;
        // $data['next_data'] = $next_data;

        $res['type_name'] = !empty($res['con_type']['type_name']) ?  $res['con_type']['type_name'] : '';
        unset($res['con_type']);
        if ($user_id) {
            //判断是否收藏
            $is_collect = NewsCollect::isCollect($this->request->id, $user_id);
            $data['is_collect'] = $is_collect ? true : false;
        } else {
            $data['is_collect'] = false; //未登录，默认未收藏
        }

        return $this->returnApi(200, "获取成功", true, $data);
    }

    /**
     * 新闻收藏与取消收藏
     * @param token  用户token  用户筛选用户是否收藏  
     * @param id  新闻id 
     */
    public function collectAndCancel()
    {
        $user_id = $this->request->user_info['id'];
        $model = new NewsCollect();
        list($res, $msg) = $model->collectAndCancel($this->request->id, $user_id);
        if ($res) {
            return $this->returnApi(200, $msg . "成功", true);
        }
        return $this->returnApi(202, $msg . "失败");
    }

    /**
     * 收藏新闻列表
     * @param token  用户token  用户筛选用户是否收藏  
     * @param type_id  新闻类型id  未传表示全部
     * @param keywords 关键字搜索
     * @param page 页数    默认第一页
     * @param limit 限制条数   默认 10 条
     */
    public function collectList()
    {
        $user_id = $this->request->user_info['id'];
        $type_id = $this->request->type_id;
        $keywords = $this->request->keywords;
        $limit = $this->request->input('limit', 10);

        $res = $this->model->collectList($user_id, $keywords, $type_id, $limit);
        if (empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }

        $res = $this->disPageData($res);
        foreach ($res['data'] as $key => $val) {
            $res['data'][$key]['type_name'] = !empty($val['con_type']) ? $val['con_type']['type_name'] : '';
            $res['data'][$key]['content'] = strip_tags($val['content']);
            $res['data'][$key]['add_time'] = date('Y-m-d', strtotime($val['add_time']));
            $res['data'][$key]['img'] = $val['img'] ? explode('|', $val['img']) : null;
            unset($res['data'][$key]['con_type']);
        }

        return $this->returnApi(200, "获取成功", true, $res);
    }
}
