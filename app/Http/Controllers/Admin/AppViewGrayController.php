<?php

namespace App\Http\Controllers\Admin;

use App\Models\Manage;

/**
 * 应用显示灰色
 */
class AppViewGrayController extends CommonController
{
    protected $model;
    protected $validate;

    public function __construct()
    {
        parent::__construct();
        $this->model = new \App\Models\AppViewGray();
        $this->validate = new  \App\Validate\AppViewGrayValidate();
    }

    /**
     * 列表
     * @param page int 页码
     * @param limit int 分页大小
     * @param keywords string 搜索关键词
     * @param start_time datetime 开始时间    数据格式  年月日
     * @param end_time datetime 结束时间
     */
    public function lists()
    {
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $keywords = $this->request->keywords;
        $start_time = $this->request->start_time;
        $end_time = $this->request->end_time;

        $res = $this->model->lists($keywords, $start_time, $end_time, $limit);

        if (empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }
        foreach ($res['data'] as $key => $val) {
            $res['data'][$key][$this->list_index_key] = $this->addSerialNumberOne($key, $page, $limit);
            $res['data'][$key]['manage_name'] = Manage::getManageNameByManageId($val['manage_id']);
        }
        $res = $this->disPageData($res);
        return $this->returnApi(200, "查询成功", true, $res);
    }

    /**
     * 详情
     * @param id int 类型id
     */
    public function detail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->select('id', 'name', 'start_time', 'end_time', 'manage_id', 'create_time')->find($this->request->id);

        if (!$res) {
            return $this->returnApi(201, "参数传递错误");
        }
        $res =  $res->toArray();
        $res['manage_name'] = Manage::getManageNameByManageId($res['manage_id']);
        return $this->returnApi(200, "获取成功", true, $res);
    }

    /**
     * 新增
     * @param name string 事件名称
     * @param start_time datetime 开始时间    数据格式  年月日
     * @param end_time datetime 结束时间
     */
    public function add()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('add')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $name = $this->request->name;
        $is_exists = $this->model->nameIsExists($name, 'name');

        if ($is_exists) {
            return $this->returnApi(202, "该事件已存在");
        }
        $this->request->merge(['manage_id' => request()->manage_id]);
        $res = $this->model->add($this->request->all());

        if (!$res) {
            return $this->returnApi(202, "新增失败");
        }
        return $this->returnApi(200, "新增成功", true);
    }

    /**
     * 修改
     * @param id int 类型id
     * @param name string 事件名称
     * @param start_time datetime 开始时间    数据格式  年月日
     * @param end_time datetime 结束时间
     */
    public function change()
    {

        //增加验证场景进行验证
        if (!$this->validate->scene('change')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $id = $this->request->id;
        $name = $this->request->name;

        $is_exists = $this->model->nameIsExists($name, 'name', $id);

        if ($is_exists) {
            return $this->returnApi(202, "该事件已存在");
        }

        $res = $this->model->change($this->request->all());

        if (!$res) {
            return $this->returnApi(202, "修改失败");
        }
        return $this->returnApi(200, "修改成功", true);
    }

    /**
     * 删除
     * @param id int 类型id
     */
    public function del()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('del')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->del($this->request->id);

        if ($res === true) {
            return $this->returnApi(200, "删除成功", true);
        }
        return $this->returnApi(202, $res);
    }
}
