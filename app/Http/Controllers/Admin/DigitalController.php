<?php

namespace App\Http\Controllers\Admin;

use App\Models\Digital;
use App\Validate\DigitalValidate;
use Illuminate\Support\Facades\DB;

/**
 * 数字阅读
 */
class DigitalController extends CommonController
{
    public $model = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new Digital();
        $this->validate = new DigitalValidate();
    }



    /**
     * 列表
     * @param is_play int 是否发布 1.发布 2.未发布    默认1
     * @param page int 当前页
     * @param limit int 分页大小
     * @param type_id int 类型id
     * @param keywords string 搜索关键词(名称)
     */
    public function lists()
    {
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $keywords = $this->request->keywords;
        $type_id = $this->request->type_id;
        $is_play = $this->request->is_play;

        $res = $this->model->lists(['id', 'is_play', 'title', 'img', 'url', 'type_id', 'sort', 'browse_num', 'create_time'],  $keywords, $type_id, $is_play, null, null, 'sort desc', $limit);

        if (empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }
        $res['data'] = $this->addSerialNumber($res['data'], $page, $limit);
        $res = $this->disPageData($res);

        return $this->returnApi(200, "获取成功", true, $res);
    }

    /**
     * 详情
     * @param id int id
     */
    public function detail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->detail($this->request->id);

        if (empty($res)) {
            return $this->returnApi(203, "暂无数据");
        }


        return $this->returnApi(200, "获取成功", true, $res->toArray());
    }

    /**
     * 新增
     * @param title string 名称
     * @param type_id string 类型id
     * @param img string 图片
     * @param url string 地址
     * @param sort string 排序 数字越大越靠前
     */
    public function add()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('add')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        if (config('other.project_type') == 'applet') {
            if (mb_substr($this->request->url, 0, 5) !== 'https' && mb_substr($this->request->url, 0, 4) !== '#小程序') {
                return $this->returnApi(202, "链接必须是 https 或 #小程序 开头");
            }
        } else {
            if (mb_substr($this->request->url, 0, 5) !== 'https' && mb_substr($this->request->url, 0, 4) !== 'http') {
                return $this->returnApi(202, "链接必须是 http 或 https 开头");
            }
        }
        $is_exists = $this->model->nameIsExists($this->request->title, 'title');

        if ($is_exists) {
            return $this->returnApi(202, "此名称已存在");
        }

        $res = $this->model->add($this->request->all());

        if (!$res) {
            return $this->returnApi(202, "新增失败");
        }
        return $this->returnApi(200, "新增成功", true);
    }

    /**
     * 修改
     * @param id int id
     * @param title string 名称
     * @param type_id string 类型id
     * @param img string 图片
     * @param url string 地址
     * @param sort string 排序 数字越大越靠前
     */
    public function change()
    {

        //增加验证场景进行验证
        if (!$this->validate->scene('change')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        if (config('other.project_type') == 'applet') {
            // if (mb_substr($this->request->url, 0, 5) !== 'https' && mb_substr($this->request->url, 0, 4) !== '#小程序') {
            //     return $this->returnApi(202, "链接必须是 https 或 #小程序 开头");
            // }
            if (mb_substr($this->request->url, 0, 5) !== 'https') {
                return $this->returnApi(202, "链接必须是 https 开头");
            }
        } else {
            if (mb_substr($this->request->url, 0, 5) !== 'https' && mb_substr($this->request->url, 0, 4) !== 'http') {
                return $this->returnApi(202, "链接必须是 http 或 https 开头");
            }
        }

        $is_exists = $this->model->nameIsExists($this->request->title, 'title', $this->request->id);

        if ($is_exists) {
            return $this->returnApi(202, "此名称已存在");
        }

        $res = $this->model->change($this->request->all());

        if (!$res) {
            return $this->returnApi(202, "修改失败");
        }
        return $this->returnApi(200, "修改成功", true);
    }

    /**
     * 删除
     * @param id int id
     */
    public function del()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('del')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->del($this->request->id);

        if ($res === true) {
            return $this->returnApi(200, "修改成功", true);
        }
        return $this->returnApi(202, $res);
    }

    /**
     * 撤销 和发布
     * @param id int 活动id
     * @param is_play int 发布   1 发布  2 撤销
     */
    public function cancelAndRelease()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('cancel_and_release')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        DB::beginTransaction();
        try {
            list($code, $msg) = $this->model->resumeAndCancel($this->request->id, 'is_play', $this->request->is_play);

            $is_play = $this->request->is_play == 1 ? '发布' : '撤销';

            DB::commit();
            return $this->returnApi($code, $is_play . $msg, $code === 200 ? true : false);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->returnApi(202, $e->getMessage());
        }
    }

    /**
     * 数字资源排序
     * content json格式数据  [{"id":1,"sort":2},{"id":2,"sort":2},{"id":3,"sort":3},{"id":4,"sort":4},{"id":5,"sort":5}]   第一个 sort 最大
     */
    public function sortChange()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('sort_change')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $content = $this->request->input('content', '');
        if (empty($content)) {
            return $this->returnApi(201, "参数传递错误");
        }
        $content = json_decode($content, true);

        DB::beginTransaction();
        try {
            foreach ($content as $key => $val) {
                $this->model->where('id', $val['id'])->update(['sort' => $val['sort']]);
            }
            DB::commit();
            return $this->returnApi(200, "修改成功", true);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->returnApi(202, $e->getMessage());
        }
    }
}
