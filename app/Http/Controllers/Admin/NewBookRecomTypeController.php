<?php

namespace App\Http\Controllers\Admin;

use App\Models\ShopBookType;
use App\Validate\ShopBookTypeValidate;

/**
 * （图书到家）书店新书类型
 */
class NewBookRecomTypeController extends CommonController
{

    public $model = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new ShopBookType();
        $this->validate = new ShopBookTypeValidate();
    }

    /**
     * 类型(用于下拉框选择)
     * @param shop_id int 书店id
     */
    public function filterList()
    {

        $shop_all_id = $this->getAdminShopIdAll();
        if (empty($shop_all_id)) {
            return $this->returnApi(203, '暂无数据'); //无权限访问
        }
        $shop_id = $this->request->shop_id;

        $res = $this->model->lists(null, $shop_id, null, $shop_all_id, 1000);
        if (empty($res['data'])) {
            return $this->returnApi(203, '暂无数据');
        }

        if (empty($shop_id)) {
            foreach ($res['data'] as $key => $val) {
                $res['data'][$key]['type_name'] = $val['type_name'] . ' (' . $val['con_shop']['name'] . ')'; //解决同一个类型，多家书店同时存在的情况
                unset($res['data'][$key]['con_shop']);
            }
        }

        return $this->returnApi(200, '获取成功', true, $res['data']); //只返回数据
    }

    /**
     * 列表
     * @param shop_id int 书店id
     * @param page int 当前页
     * @param limit int 分页大小
     * @param keywords string 搜索关键词(类型名称)
     */
    public function lists()
    {
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $keywords = $this->request->keywords;
        $shop_id = $this->request->shop_id;

        $shop_all_id = $this->getAdminShopIdAll();
        if (empty($shop_all_id)) {
            return $this->returnApi(203, '暂无数据'); //无权限访问
        }
        $res = $this->model->lists(null, $shop_id, $keywords, $shop_all_id, $limit);
        if (empty($res['data'])) {
            return $this->returnApi(203, '暂无数据');
        }

        $res = $this->disPageData($res);
        $res['data'] = $this->addSerialNumber($res['data'], $page, $limit);
        $res['data'] = $this->disDataSameLevel($res['data'], 'con_shop', ['name' => 'shop_name']);

        return $this->returnApi(200, '获取成功', true, $res); //只返回数据
    }

    /**
     * 详情
     * @param id int 类型id
     */
    public function detail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $shop_all_id = $this->getAdminShopIdAll();
        if (empty($shop_all_id)) {
            return $this->returnApi(203, '暂无数据'); //无权限访问
        }
        $res = $this->model->detail($this->request->id, $shop_all_id);

        if (!$res) {
            return $this->returnApi(201, "参数传递错误");
        }
        $res = $res->toArray();
        unset($res['con_shop']);
        return $this->returnApi(200, "获取成功", true, $res);
    }

    /**
     * 新增
     * @param shop_id string 书店id
     * @param type_name string 类型名称
     */
    public function add()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('add')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $shop_all_id = $this->getAdminShopIdAll();
        if (empty($shop_all_id) || !in_array($this->request->shop_id, $shop_all_id)) {
            return $this->returnApi(202, '您无权添加此书店类型'); //无权限访问
        }

        $findWhere[] = ['shop_id', '=', $this->request->shop_id];
        $is_exists = $this->model->nameIsExists($this->request->type_name, 'type_name', null, 1, $findWhere);

        if ($is_exists) {
            return $this->returnApi(202, "此名称已存在");
        }

        $res = $this->model->add($this->request->all());

        if (!$res) {
            return $this->returnApi(202, "新增失败");
        }
        return $this->returnApi(200, "新增成功", true);
    }

    /**
     * 修改
     * @param id int 类型id
     * @param shop_id string 书店id
     * @param type_name string 类型名称
     */
    public function change()
    {

        //增加验证场景进行验证
        if (!$this->validate->scene('change')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $shop_all_id = $this->getAdminShopIdAll();
        if (empty($shop_all_id) || !in_array($this->request->shop_id, $shop_all_id)) {
            return $this->returnApi(202, '您无权操作此书店类型'); //无权限访问
        }

        $findWhere[] = ['shop_id', '=', $this->request->shop_id];
        $is_exists = $this->model->nameIsExists($this->request->type_name, 'type_name', $this->request->id, 1, $findWhere);

        if ($is_exists) {
            return $this->returnApi(202, "此名称已存在");
        }

        $res = $this->model->change($this->request->all());

        if (!$res) {
            return $this->returnApi(202, "修改失败");
        }
        return $this->returnApi(200, "修改成功", true);
    }

    /**
     * 删除
     * @param id int 类型id
     */
    public function del()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('del')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $shop_all_id = $this->getAdminShopIdAll();
        if (empty($shop_all_id)) {
            return $this->returnApi(202, '您无权删除此书店类型'); //无权限访问
        }

        $result = $this->model->detail($this->request->id, $shop_all_id);
        if (empty($result)) {
            return $this->returnApi(202, '您无权删除此书店类型'); //无权限访问
        }
        $res = $this->model->del($this->request->id);

        if ($res === true) {
            return $this->returnApi(200, "删除成功", true);
        }
        return $this->returnApi(202, $res);
    }
}
