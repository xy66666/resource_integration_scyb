<?php

namespace App\Http\Controllers\Admin;

use App\Models\Manage;
use App\Models\ManageRole;
use Illuminate\Support\Facades\DB;

/**
 * 角色
 */
class RoleController extends CommonController
{
    public $model = null;
    public $permission_model = null;
    public function __construct()
    {
        parent::__construct();

        $this->model = new \App\Models\Role();
        $this->permission_model = new \App\Models\Permission();
    }

    /**
     * 角色列表 （筛选使用）
     * @param keywords 关键字搜索
     */
    public function roleFilterList()
    {
        //查看所有下级管理员id
        $manageMoldeObj = new Manage();
        $manage_id_all = $manageMoldeObj->getManageIdAll(request()->manage_id);
        //获取管理员的角色
        $manageRoleModelObj = new ManageRole();
        $role_id_all = $manageRoleModelObj->getManageRole(request()->manage_id);
        $role_id_all = array_column($role_id_all , 'id');

        $keywords = $this->request->input('keywords', '');
        $res = $this->model->select('id', 'role_name')->where(function ($query) use ($keywords) {
            if ($keywords) {
                $query->where('role_name', 'like', '%' . $keywords . '%');
            }
        })->where('is_del', 1)
        ->where(function ($query) use ($manage_id_all, $role_id_all) {
            if ($manage_id_all) {
                $query->orWhereIn('manage_id', $manage_id_all);
            }
            if ($role_id_all) {
                $query->orWhereIn('id', $role_id_all);
            }
        })
        // ->where('manage_id' , $manage_id_all)
        ->orderByDesc('id')
        ->get();

        if ($res) {
            return $this->returnApi(200, '获取成功', true, $res);
        }

        return $this->returnApi(203, '暂无数据');
    }


    /**
     * 列表
     * @param page int 当前页
     * @param limit int 分页大小
     * @param keywords string 搜索关键词
     */
    public function roleList()
    {
        $page = request()->page ? intval(request()->page) : 1;
        $limit = request()->limit ? intval(request()->limit) : 10;
        $keywords = request()->keywords;
        $res = $this->model->getRoleList($keywords, $limit);

        if ($res['data']) {
            $res = $this->disPageData($res);
            //增加序号
            $res['data'] = $this->addSerialNumber($res['data'], $page, $limit);

            return $this->returnApi(200, '获取成功', true, $res);
        }

        return $this->returnApi(203, '暂无数据');
    }

 

    /**
     * 角色详情（只返回最子集权限）
     * @param id int 角色id
     * @return 只返回子集权限  （父节点一律不返回）
     */
    public function roleDetail()
    {
        $id = request()->id;

        if (!$id) {
            return $this->returnApi(201, '参数传递错误');
        }

        $permission_table_name = $this->permission_model->getTable();

        $res = $this->model->select('id', 'role_name', 'create_time', 'change_time')
            ->with(['conPermission' => function ($query) use ($permission_table_name) {
                $query->select("$permission_table_name.id", "permission_name");
            }])->find($id);


        if (!$res) {
            return $this->returnApi(201, '参数传递错误');
        }
        foreach ($res->conPermission as $key => $val) {
            //只返回子集（判断有无子集，有则不返回，无则返回）
            $is_exists_child = $this->permission_model->permissionIsExistsChild($val['id']);
            if($is_exists_child){
                unset($res->conPermission[$key]);
                continue;
            }
            unset($res->conPermission[$key]['pivot']);
        }
        $res = $res->toArray();
        sort($res['con_permission']);
  
        return $this->returnApi(200, 'ok', true, $res);
    }


    /**
     * 角色新增
     * @param role_name string 角色名称
     * @param permission_ids  角色id包含权限多个逗号拼接   可为空，空代表无权限  包括所有父级的权限
     */
    public function roleAdd()
    {
        $role_name = request()->role_name ? request()->role_name : null;
        if (empty($role_name)) {
            return $this->returnApi(201, '权限名称不能为空');
        }

        $is_exists = $this->model->roleIsExists($role_name);

        if ($is_exists) {
            return $this->returnApi(202, '该角色已存在，请重新输入');
        }

        // 启动事务
        DB::beginTransaction();
        try {
            $this->model->add($this->request);
            // 提交事务
            DB::commit();
            return $this->returnApi(200, "新增成功", true);
        } catch (\Exception $e) {
            // 回滚事务
            DB::rollBack();
            return $this->returnApi(202, "新增失败");
        }
    }


    /**
     * 角色修改
     * @param id string 角色id
     * @param role_name string 角色名称
     * @param permission_ids  角色id包含权限多个逗号拼接   可为空，空代表无权限  包括所有父级的权限
     */
    public function roleChange()
    {
        //获取管理员的角色
        $manageRoleModelObj = new ManageRole();
        $role_id_all = $manageRoleModelObj->getManageRole(request()->manage_id);
        $role_id_all = array_column($role_id_all , 'id');
        if(in_array(request()->id , $role_id_all) && $this->request->manage_id != 1){
            return $this->returnApi(201,  '您无权限修改此角色');
        }

        $id = request()->id ? request()->id : null;
        $role_name = request()->role_name ? request()->role_name : null;
        if (empty($id) || empty($role_name)) {
            return $this->returnApi(201, '参数错误');
        }

        $is_exists = $this->model->roleIsExists($role_name, $id);
        if ($is_exists) {
            return $this->returnApi(202, '该角色已存在，请重新输入');
        }

        // 启动事务
        DB::beginTransaction();
        try {
            $this->model->change($this->request);
            // 提交事务
            DB::commit();
            return $this->returnApi(200, "修改成功", true);
        } catch (\Exception $e) {
            // 回滚事务
            DB::rollBack();
            return $this->returnApi(202, "修改失败");
        }
    }

    /**
     * 角色删除
     * @param id int 角色id
     */
    public function roleDel()
    {
        $id = request()->id;

        //获取管理员的角色
        $manageRoleModelObj = new ManageRole();
        $role_id_all = $manageRoleModelObj->getManageRole(request()->manage_id);
        $role_id_all = array_column($role_id_all , 'id');

        if(in_array($id , $role_id_all)  && $this->request->manage_id != 1){
            return $this->returnApi(201,  '您无权限删除此角色');
        }


        if (!$id) {
            return $this->returnApi(201, '参数传递错误');
        }

        $role = $this->model->find($id);

        if (!$role) {
            return $this->returnApi(201, '参数传递错误');
        }

        $role->is_del = 2;
        $res = $role->save();

        if (!$res) {
            return $this->returnApi(202, '删除失败', true);
        }

        return $this->returnApi(200, '删除成功');
    }
}
