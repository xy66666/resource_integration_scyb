<?php

namespace App\Http\Controllers\Admin;

use App\Models\OnlineRegistrationConfig;
use Illuminate\Support\Facades\DB;

/**
 * 在线办证配置
 */
class OnlineRegistrationConfigController extends CommonController
{

    public $model = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new OnlineRegistrationConfig();
    }

    /**
     * 获取在线办证参数（获取已配置的）
     */
    public function getOnlineRegistrationConfigParam()
    {
        $real_info = $this->model->where('id', 1)->value('real_info');
        $param = [];
        if ($real_info) {
            $param = explode('|', $real_info);
        }
        return $this->returnApi(200, '获取成功', true, $param);
    }


    /**
     * 配置在线办证参数
     * @param content 多个id | 链接  1.头像(弃用) 2.居住地 3.身份证正反面 4.电子邮箱 5.邮政编码 6.座机号码 7.读者职业 8.文化程度 9.工作单位 10.备注
     */
    public function setOnlineRegistrationConfigParam()
    {
        $res = $this->model->where('id', 1)->first();
        if (empty($res)) {
            $res = $this->model;
        }
        $res->real_info = $this->request->content;
        $result = $res->save();
        if ($result) {
            return $this->returnApi(200, '设置成功', true);
        }
        return $this->returnApi(202, '设置失败');
    }
}
