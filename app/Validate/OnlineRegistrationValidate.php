<?php

namespace App\Validate;

use App\Validate\BaseValidate;

class OnlineRegistrationValidate extends BaseValidate
{
    protected  $rule = [
        'id' => 'required|integer',
        'ids' => 'required',
        'reader_id' => 'required|min:5',
        'id_card' => 'required|check_id_card',
        'username' => 'required|min:2',
        'certificate_type' => 'required',
        'card_type' => 'required',
        'tel' => 'required|size:11|check_tel',
        "key" => "required|size:32|check_tel_key",
        "state" => "required|in:1",
        "verify" => 'required|size:6|check_tel_verify'
    ];

    //自定义验证信息
    protected $message = [
        'id.required' => '办证ID不能为空',
        'id.integer' => '办证ID规则不正确',
        'ids.required' => '办证ID不能为空',
        'reader_id.required' => '读者证号不能为空',
        'reader_id.min' => '读者证号规则不正确',
        'id_card.required' => '身份证号不能为空',
        'id_card.check_id_card' => '身份证号规则不正确',

        'username.required' => '用户名不能为空',
        'username.min' => '用户名规则不正确',

        'certificate_type.required' => '证件类型不能为空',
        'card_type.required' => '读者证类型不能为空',

        'tel.required' => '电话号码不能为空',
        'tel.size' => '电话号码位数不正确',
        'tel.check_tel' => '电话号码规则不正确',

        'key.required' => 'key不能为空',
        'key.size' => 'key规则不正确',
        'key.check_tel_key' => 'key输入不正确',


        'state.required' => '验证码方式不能为空',
        'state.in' => '验证码方式规则不正确',

        'verify.required' => '手机验证码不能为空',
        'verify.size' => '手机验证码位数不正确',
        'verify.check_tel_Verify' => '手机验证码输入错误',
    ];




    protected  $scene = [
        "send" => ["tel", 'key', 'state'], //发送验证码 前台+后台
        'online_register' => ['id_card', 'username', 'certificate_type', 'card_type', 'tel', 'verify'], //在线办证  工作单位，邮箱、手机号码、邮编可以不要
        'detail' => ['id'], //在线办证详情

        'admin_get_card_type' => ['certificate_type'], //获取在线办证类型
        'admin_to_receive' => ['id'], //领取读者证
        'admin_sponsor_settle_state' => ['ids'], //（发起方）标记办证状态为结算中
        'admin_confirm_settle_state' => ['ids'], //（确认方） 标记邮费状态为已结算
    ];
}
