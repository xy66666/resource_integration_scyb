<?php

namespace App\Validate;

use App\Validate\BaseValidate;

/**
 * 支付模块
 * Class PayInfoValidate
 * @package app\port\validate
 */
class OwePayInfoValidate extends BaseValidate
{
    protected  $rule = [
        'order_id|订单id' => 'required|integer', //欠费id
    ];


    //自定义验证信息
    protected $message = [
        'order_id.required' => '订单ID不能为空',
        'order_id.integer' => '订单ID格式不正确',
    ];


    protected  $scene = [
        'payment_success'   => ['order_id'], //欠费详情
        'get_pay_param'   => ['order_id'], //欠费详情
    ];
}
