<?php

namespace App\Validate;

use App\Validate\BaseValidate;

/**
 * 数字阅读
 */
class DigitalValidate extends BaseValidate
{
    //验证规则
    protected $rule = [
        'id' => 'bail|required|integer',
        'type_id' => 'bail|required|integer',
        'title' => 'bail|required',
        'img' => 'bail|required',
        // 'url'=>'bail|required|url',
        'url' => 'bail|required',
        'content' => 'bail|required',
    ];
    //自定义验证信息
    protected $message = [
        'id.required' => 'ID不能为空',
        'id.integer' => 'ID格式不正确',
        'type_id.required' => '类型ID不能为空',
        'type_id.integer' => '类型ID格式不正确',
        'title.required' => '名称不能为空',
        'img.required' => '图片不能为空',
        'url.required' => '链接不能为空',
        //  'url.url'=>'链接规则不正确',

        'content.required' => '内容不能为空',

    ];

    //自定义场景
    protected $scene = [
        'add' => ['title', 'type_id', 'url'], //添加
        'change' => ['id', 'title', 'type_id', 'url'], //修改
        'detail' => ['id'], //详情
        'del' => ['id'], //删除
        'cancel_and_release' => ['id', 'is_play'], //撤销 和发布

        'add_browse_num' => ['id'], //前台增加访问量

        'sort_change' => ['content'], //banner 图排序
    ];
}
