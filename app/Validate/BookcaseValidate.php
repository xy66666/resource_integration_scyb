<?php

namespace App\Validate;

use App\Validate\BaseValidate;

/**
 * 展览屏幕验证器
 */
class BookcaseValidate extends BaseValidate
{
  //验证规则
  protected $rule = [
    'id' => 'bail|required|integer',
    'exhibition_id' => 'bail|required',
    'name' => 'bail|required',

  ];
  //自定义验证信息
  protected $message = [
    'id.required' => '机柜编号不能为空',
    'id.integer' => '机柜编号格式不正确',
    'exhibition_id.required' => '展览编号不能为空',
    'exhibition_id.integer' => '展览编号格式不正确',
    'name.required' => '机柜名称不能为空',

  ];


  //自定义场景
  protected $scene = [
    'bookcase_info' => ['id'], //获取书柜详情
    'bookcase_add' => ['name'], //机柜添加
    'bookcase_change' => ['id','name'], //机柜修改
    'bookcase_del' => ['id'], //机柜删除
    'bookcase_config_exhibition' => ['id'], //机柜配置展览
    'bookcase_del_exhibition' => ['id','exhibition_id'], //机柜删除展览
    'bookcase_add_exhibition' => ['id','exhibition_id'], //机柜添加展览

  ];
}
