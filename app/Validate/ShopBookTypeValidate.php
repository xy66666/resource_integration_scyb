<?php

namespace App\Validate;

use App\Validate\BaseValidate;


/**
 * 书店新书类型
 */
class ShopBookTypeValidate extends  BaseValidate
{
     //验证规则
  protected $rule = [
    'id' => 'bail|required|integer',
    'shop_id' => 'bail|required|integer',
    'type_name' => 'bail|required',

  ];
  //自定义验证信息
  protected $message = [
    'id.required' => 'ID不能为空',
    'id.integer' => 'ID格式不正确',
    'shop_id.required' => '书店ID不能为空',
    'shop_id.integer' => '书店ID格式不正确',
    'type_name.required' => '名称不能为空',
    'type.required' => '展示类型不能为空',

  ];


  //自定义场景
  protected $scene = [
    'detail' => ['id'], //详情
    'add' => ['type_name','shop_id'], //添加
    'change' => ['id','type_name','shop_id'], //修改
    'del' => ['id'], //删除


  ];

}