<?php
namespace App\Validate;

use App\Validate\BaseValidate;
/**
 * 用户协议
 */
class UserProtocolValidate extends BaseValidate {
    //验证规则
    protected $rule =[
        'id'=>'bail|required|integer',
        'type'=>'bail|required|integer|in:1,2,3,4,5',
        'content'=>'bail|required',

    ];
    //自定义验证信息
    protected $message = [
        'id.required'=>'ID不能为空',
        'id.integer'=>'ID格式不正确',
        'type.required' => '类型不能为空',
        'type.integer' => '类型格式不正确',
        'type.in' => '类型格式不正确',
        'content.required'=>'内容不能为空',


    ];

    //自定义场景
    protected $scene = [
        'change' => ['content','type'],//修改
        'detail' => ['id'],//详情


    ];




}