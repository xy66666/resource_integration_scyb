<?php
namespace App\Validate;

use App\Validate\BaseValidate;
/**
 * 用户验证器
 */
class UserAccountValidate extends BaseValidate {
    //验证规则
    protected $rule =[
        'id'=>'bail|required|integer',
        'account'=>'bail|required',
        'type_id' => 'required|integer',
        'username' => 'required',
        'tel' => 'required|check_tel',
        'id_card' => 'required|check_id_card',
        'status' => 'required|in:1,2,3,4',

    ];
    //自定义验证信息
    protected $message = [
        'id.required'           =>'读者证号ID不能为空',
        'id.integer'            =>'读者证号ID不合法',
        'account.required'      =>'读者证号不能为空',
        'type_id.required'      =>'读者证号类型不能为空',
        'type_id.integer'       =>'读者证号类型不合法',
        'username.required'     => '读者姓名不能为空',
        'tel.required'          => '电话号不能为空',
        'tel.check_tel'         => '电话号格式不正确',
        'id_card.required'      => '身份证号码不能为空',
        'id_card.check_id_card' => '身份证号码格式不正确',
        'status.required'       => '类型不能为空',
        'status.in'             => '类型格式不正确',
    ];

    //自定义场景
    protected $scene = [
        'user_add' => ['account', 'type_id','username','id_card'],//读者证号添加
        'user_change' => ['id','username'],//读者证号修改
        'switch_status' => ['id','status'],//读者证号状态切换
        'user_detail' => ['id'],//用户详情
        'get_user_info_by_account' => ['account'],//根据读者证号获取用户基本信息（用于借书获取用户信息）
    ];




}