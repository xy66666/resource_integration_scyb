<?php
namespace App\Validate;

use App\Validate\BaseValidate;
/**
 *在线荐购
 */
class RecommendBookVallidate extends BaseValidate {
    //验证规则
    protected $rule =[
        'ids'=>'bail|required',
        'status'=>'bail|required|in:2,3,4,5',


    ];
    //自定义验证信息
    protected $message = [
        'ids.required'=>'ID不能为空',
        'status.required'=>'状态不能为空',
        'status.in'=>'状态规则不正确',


    ];

    //自定义场景
    protected $scene = [
        'check' => ['ids' ,'status'],//审核

        
    ];




}