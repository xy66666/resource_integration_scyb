<?php
namespace App\Validate;

use App\Validate\BaseValidate;
/**
 * web 首页背景图片 Banner管理
 */
class WebBackgroundBannerValidate extends BaseValidate {
    //验证规则
    protected $rule =[
        'type'=>'bail|required|integer|in:1,2,3,4,5',
        'img'=>'bail|required',
    ];
    //自定义验证信息
    protected $message = [
        'type.required'=>'类型不能为空',
        'type.integer'=>'类型规则不正确',
        'type.in'=>'类型规则不正确',
        'img.required'=>'封面图片不能为空',
       
    ];

    //自定义场景
    protected $scene = [
        'change' => ['type'],//修改
    ];




}