<?php

namespace App\Validate;

use App\Validate\BaseValidate;

class ActivityValidate extends  BaseValidate
{
    protected  $rule =[
        'id'=>'bail|required|integer',
        'title' => 'required',
        'act_id' => 'required|integer',
        'type_id' => 'required|integer',
        'tag_id' => 'required',//可选多个

        'is_long' => 'required|integer|in:1,2',
        'is_apply' => 'required|integer|in:1,2',
    //    'is_reader' => 'required|integer|in:1,2',
        'is_qr' => 'required|integer|in:1,2',
        'is_continue' => 'required|integer|in:1,2',
        'is_real' => 'required|integer|in:1,2',
        'is_check' => 'required|integer|in:1,2',
        'is_push' => 'required|integer|in:1,2',

        'astrict_sex' => 'required|integer|in:1,2,3',
        'province' => 'required',
        'city' => 'required',
        'district' => 'required',
        'address' => 'required',
        'start_time' => 'required|date',
        'end_time' => 'required|date|after:start_time',
        'tel' => 'required|check_tel_and_phone:',

        'password' => 'bail|required|min:6',
    ];


    # 错误提示语
    protected $message = [
        'id.required'=>'ID不能为空',
        'id.integer'=>'ID格式不正确',
        "title.required" => "活动名称不能为空",

        "is_long.required" => "是否长期活动不能为空",
        "is_long.integer" => "是否长期活动参数错误",
        "is_long.in" => "是否长期活动参数错误",
        "is_apply.required" => "是否报名不能为空",
        "is_apply.integer" => "是否报名参数错误",
        "is_apply.in" => "是否报名参数错误",
   //     "is_reader.required" => "是否需要绑定读者证不能为空",
   //     "is_reader.integer" => "是否需要绑定读者证参数错误",
   //     "is_reader.in" => "是否需要绑定读者证参数错误",
        "is_qr.required" => "是否需要二维码不能为空",
        "is_qr.integer" => "是否需要二维码参数错误",
        "is_qr.in" => "是否需要二维码参数错误",
        "is_continue.required" => "取消报名后是否可以继续报名不能为空",
        "is_continue.integer" => "取消报名后是否可以继续报名参数错误",
        "is_continue.in" => "取消报名后是否可以继续报名参数错误",
        "is_real.required" => "是否需要真实信息不能为空",
        "is_real.integer" => "是否需要真实信息参数错误",
        "is_real.in" => "是否需要真实信息参数错误",
        "is_check.required" => "报名是否需要审核不能为空",
        "is_check.integer" => "报名是否需要审核参数错误",
        "is_check.in" => "报名是否需要审核参数错误",
        "is_push.required" => "是否推送不能为空",
        "is_push.integer" => "是否推送参数错误",
        "is_push.in" => "是否推送参数错误",
        "astrict_sex.required" => "是否限制性别不能为空",
        "astrict_sex.integer" => "是否限制性别参数错误",
        "astrict_sex.in" => "是否限制性别参数错误",
        "type_id.required" => "活动类型ID不能为空",
        "type_id.integer" => "活动类型ID参数错误",
        "act_id.required" => "活动ID不能为空",
        "act_id.integer" => "活动ID参数错误",
        "tag_id.required" => "活动标签ID不能为空",
        "province.required" => "省级地址不能为空",
        "city.required" => "市级地址不能为空",
        "district.required" => "区级地址不能为空",
        "address.required" => "详细地址不能为空",
        "start_time.required" => "活动开始时间不能为空",
        "start_time.date" => "活动开始时间参数错误",
        "end_time.required" => "活动结束时间不能为空",
        "end_time.date" => "活动结束时间参数错误",
        "end_time.after" => "活动结束时间不能小于开始时间",
        "tel.required" => "活动联系方式不能为空",
        "tel.check_tel_and_phone" => "活动联系方式格式不正确",


        "password.required" => "管理员密码不能为空",
        "password.min" => "管理员密码位数不正确",
    ];

    protected  $scene = [
        'add' => ['title','is_long','is_continue'/* ,'is_reader' */,'is_apply','is_qr','is_real','is_check','is_push','astrict_sex','type_id','tag_id','start_time','end_time'], //新增验证
        'change' => ['id','title','is_long','is_continue'/* ,'is_reader' */,'is_apply','is_qr','is_real','is_check','astrict_sex','type_id','tag_id','start_time','end_time'], //更新验证
        'push' => ['id'], //推送模板消息
        'cancel' => ['id'], //撤销活动
        'release' => ['id'], //发布活动
        'sign' => ['id'], //签到
        'switchover_type' => ['id','type_id'], //切换类型
        'recom_and_cancel' => ['id'], //推荐为热门活动 或 取消热门推荐
        'del' => ['id'], //活动删除
        'detail' => ['id'], //活动详情

        'copy_data' => ['id','password'], //活动详情

        'web_apply' => ['act_id'], //活动报名
        'web_cancel' => ['act_id'], //取消活动报名
    ];


}
