<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * The controller namespace for the application.
     *
     * When present, controller route declarations will automatically be prefixed with this namespace.
     *
     * @var string|null
     */
    // protected $namespace = 'App\\Http\\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $this->configureRateLimiting();

        $this->routes(function () {
            Route::prefix('api')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/api.php'));

            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/web.php'));

            // Route::namespace($this->namespace)
            //     ->group(base_path('routes/screen.php'));

            //自己增加路由配置文件
            // Route::middleware('port')   //中间件 port  名字与 路由名字一样，会在RouteServiceProvider 自动运用
            // Route::middleware('jwt')   //中间件 port  名字与 路由名字一样，会在RouteServiceProvider 自动运用
            //     ->namespace($this->namespace)
            //     ->group(base_path('routes/port.php'));

            //自己增加路由配置文件
            Route::middleware(['limit_black_list_access', 'jwt', 'invite', 'wechat_auth'])
                ->namespace($this->namespace)
                ->group(base_path('routes/wechat.php'));
            //自己增加路由配置文件
            // Route::middleware('jwt')
            //     ->namespace($this->namespace)
            //     ->group(base_path('routes/payinfo.php'));

            //自己增加路由配置文件
            Route::namespace($this->namespace)
                ->group(base_path('routes/admin.php'));

            //自己增加路由配置文件
            Route::namespace($this->namespace)
                ->group(base_path('routes/libapi.php'));

            //自己增加路由配置文件
            // Route::namespace($this->namespace)
            //     ->group(base_path('routes/accesssystem.php'));
            // //自己增加路由配置文件
            // Route::middleware('jwt')
            //     ->namespace($this->namespace)
            //     ->group(base_path('routes/wall.php'));

            //自己增加路由配置文件
            // Route::middleware(['jwt', 'web_auth'])
            //     ->namespace($this->namespace)
            //     ->group(base_path('routes/port.php'));
        });
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by(optional($request->user())->id ?: $request->ip());
        });
    }
}
