<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;


class SystemInfo extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'system_info';

    /**
     * 系统消息列表
     * @param $limit 显示条数
     * @param user_id 用户id
     * @param type 0 全部 1 系统通知  2 回复消息
     */
    public function lists($user_id, $type = 0, $limit = 10)
    {
        $res = $this->select('id', 'title', 'type', 'con_id', 'intro', 'is_look', 'create_time')
        ->where(function ($query) use ($type) {
            if ($type == 1) {
                $query->where('type', '<>', 2); //2 为后台回复消息
            } elseif ($type == 2) {
                $query->where('type', 2); //2 为后台回复消息
            }
        })->where('user_id', $user_id)
            ->where('is_del', 1)
            ->orderByDesc('id')
            ->paginate($limit)
            ->toArray();

        foreach ($res['data'] as $key => $val) {
            if ($val['type'] == 2) {
                //获取问题
                $res['data'][$key]['problem'] = Feedback::where('id', $val['con_id'])->value('feedback');
            } else {
                $res['data'][$key]['problem'] = '';
            }
        }
        return $res;
    }


    /**
     * 系统消息详情
     * @param id 系统消息id
     * @param type 0 全部 1 系统通知  2 回复消息
     */
    public function detail($id)
    {
        $res = $this->select('id', 'title', 'type', 'con_id', 'intro', 'is_look', 'create_time')->find($id);

        if (empty($res)) {
            return false;
        }

        if ($res['type'] == 2) {
            //获取问题
            $res['problem'] = Feedback::where('id', $res['con_id'])->value('feedback');
        } else {
            $res['problem'] = '';
        }
        return $res;
    }

    
}
