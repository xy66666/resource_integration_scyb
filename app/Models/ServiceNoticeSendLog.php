<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;



/**
 * 服务提醒通知发送日志
 */
class ServiceNoticeSendLog extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'service_notice_send_log';



    /**
     * 服务提醒通知发送列表
     * @param notice_id 服务提醒通知id
     * @param user_id 用户id
     * @param start_time 推送开始时间
     * @param end_time 推送结束时间
     * @param limit 条数
     */
    public function lists($notice_id, $user_id, $start_time, $end_time, $limit = 10)
    {
        $res = $this->from($this->getTable() . ' as l')
            ->select('l.id', 'l.status', 'l.reason', 's.content', 's.type_name', 'l.create_time')
            ->join('service_notice as s', 's.id', '=', 'l.notice_id')
            ->where(function ($query) use ($notice_id, $user_id, $start_time, $end_time) {
                if (!empty($notice_id)) {
                    $query->where('notice_id', $notice_id);
                }
                if (!empty($user_id)) {
                    $query->where('user_id', $user_id);
                }
                if (!empty($start_time) && !empty($end_time)) {
                    $query->whereBetween('s.create_time', [$start_time, $end_time]);
                }
            })->orderByDesc('l.id')
            ->where('s.is_del', 1)
            ->paginate($limit)
            ->toArray();
        return $res;
    }
}
