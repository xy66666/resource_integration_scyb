<?php
namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**活动签到model */
class ActivitySign extends BaseModel
{

    use HasFactory;

    const CREATED_AT='create_time';
    const UPDATED_AT=null;

    
    protected $table = 'activity_signs';

}