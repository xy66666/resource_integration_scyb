<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;


/**
 * 订单退款日志
 * Class BookHomeOrderRefundModel
 * @package app\common\model
 */
class OnlineRegistrationOrderRefund extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'online_registration_order_refund';

}