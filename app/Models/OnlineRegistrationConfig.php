<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;


/**
 * 在线办证配置
 * Class ArticleTypeModel
 * @package app\common\model
 */
class OnlineRegistrationConfig extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'online_registration_config';


    /**
     * 志愿者报名，必选参数
     *  1.头像(弃用) 2.居住地 3.身份证正反面 4.电子邮箱 5.邮政编码 6.座机号码 7.读者职业 8.文化程度 9.工作单位 10.备注
     */
    public function onlineRegistrationParam()
    {
        $data = [
            ['id' => 2, 'field' => '', 'value' => '居住地'],
            ['id' => 3, 'field' => 'birth', 'value' => '身份证正反面'],
            ['id' => 4, 'field' => 'id_card', 'value' => '电子邮箱'],
            ['id' => 5, 'field' => 'nation', 'value' => '邮政编码'],
            ['id' => 6, 'field' => 'politics', 'value' => '座机号码'],
            ['id' => 7, 'field' => 'health', 'value' => '读者职业'],
            ['id' => 8, 'field' => 'tel', 'value' => '文化程度'],
            ['id' => 9, 'field' => 'school', 'value' => '工作单位']
        ];
        return $data;
    }
}
