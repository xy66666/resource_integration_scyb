<?php
namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**活动收藏表 */
class ActivityCollect extends BaseModel
{

    use HasFactory;

    const CREATED_AT='create_time';
    const UPDATED_AT='change_time';
    
    
    protected $table = 'activity_collect';

    
}