<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/*活动model*/

class Activity extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';


    protected $table = 'activity';

    /// protected $fillable = ['is_long','title'];
    protected $guarded = []; //使用模型  update 更新时，允许被全部更新，若不写，则会报错 或 没任何数据变化

    /*关联活动类型*/
    public function conType()
    {
        return $this->hasOne(ActivityType::class, 'id', 'type_id');
    }

    /*关联活动申请*/
    public function conApply()
    {
        return $this->hasMany(ActivityApply::class, 'act_id', 'id');
    }




    /**
     * 列表
     * @param page int 页码
     * @param limit int 分页大小
     * @param keywords string 搜索关键词
     * @param start_time datetime 活动开始时间    数据格式  年月日
     * @param end_time datetime 活动结束时间
     * @param is_recom 是否热门活动  0或空表示全部  1 是  2 否 （默认）
     * @param is_apply 是否需要报名 1.是  2.否  默认1 
     * @param is_play 是否发布 1.是  2.否  默认1 
     * @param type_id 活动类型筛选   0或空表示全部 
     * @param create_start_time datetime 活动创建开始时间   数据格式 年月日时分秒
     * @param create_end_time datetime 活动创建结束时间
     * @param type_status datetime 类型状态  1 必须，其余不必须
     */
    public function lists($keywords = null, $type_id = null, $start_time = null, $end_time = null, $create_start_time = null, $create_end_time = null, $is_apply = null, $is_recom = null, $is_play = null, $type_status = 1, $limit = 10)
    {
        $res = $this->select([
            'id', 'tag_id', 'type_id', 'title', 'img', 'intro', 'is_apply', 'apply_number', 'number', 'address', 'province',
            'city', 'district', 'street', 'contacts', 'tel', 'start_time', 'end_time', 'apply_start_time', 'apply_end_time', 'is_play',
            'qr_url', 'is_recom', 'create_time', 'sign_way', 'sign_qr_url'
        ])
            ->with('conType', function ($query) {
                $query->select('id', 'type_name')->where('is_del', 1);
            })
            ->wherehas('conType', function ($query) use ($type_status) {
                if ($type_status == 1) {
                    $query->where('is_del', 1);
                }
            })->where(function ($query) use ($keywords) {
                if ($keywords) {
                    $query->where('title', 'like', "%$keywords%");
                }
            })->where(function ($query) use ($type_id, $start_time, $end_time, $create_start_time, $create_end_time, $is_apply, $is_recom, $is_play) {
                if ($start_time && $end_time) {
                    $query->whereBetween('start_time', [$start_time, $end_time]);
                }
                if ($create_start_time && $create_end_time) {
                    $query->whereBetween('create_time', [$create_start_time, $create_end_time]);
                }

                //类型筛选
                if (!empty($type_id)) {
                    $query->where('type_id', $type_id);
                }
                //类型是否需要报名
                if (!empty($is_apply)) {
                    $query->where('is_apply', $is_apply);
                }
                //是否发布
                if (!empty($is_play)) {
                    $query->where('is_play', $is_play);
                }
                //是否热门
                if (!empty($is_recom)) {
                    $query->where('is_recom', $is_recom);
                }
            })
            ->where('is_del', 1)
            ->orderBy('is_recom')
            ->orderByDesc('end_time')
            ->paginate($limit)
            ->toArray();

        return $res;
    }


    /**
     * 用户活动报名填写信息
     */
    public static function getActivityApplyParam()
    {
        return [
            ['id' => 5, 'field' => 'img', 'value' => '头像'],
            ['id' => 1, 'field' => 'username', 'value' => '姓名'],
            ['id' => 2, 'field' => 'id_card', 'value' => '身份证'],
            ['id' => 8, 'field' => 'sex', 'value' => '性别'], //性别  
            ['id' => 9, 'field' => 'age', 'value' => '年龄'], //年龄  
            ['id' => 3, 'field' => 'tel', 'value' => '电话号码'],
            ['id' => 4, 'field' => 'reader_id', 'value' => '读者证'],
            ['id' => 7, 'field' => 'unit', 'value' => '单位名称'], //单位名称  
            ['id' => 11, 'field' => 'school', 'value' => '学校'], //
            ['id' => 12, 'field' => 'grade', 'value' => '年级'], //
            ['id' => 13, 'field' => 'class_grade', 'value' => '班级'], //
            ['id' => 14, 'field' => 'wechat_number', 'value' => '微信号'], //
            ['id' => 6, 'field' => 'remark', 'value' => '备注'], //备注  最多100字

            // ['id' => 10, 'field' => 'accessory', 'value' => '附件'], //压缩包  最大3M  
        ];
    }
    /**
     * 处理报名参数
     * $data 管理员输入的活动信息
     */
    public function disRealInfo($data)
    {
        $is_real = $data['is_real'];
        if ($is_real == 2) {
            return [2, '']; //不需要实名，直接返回空
        }
        $real_info = $data['real_info'];
        $real_info_arr = explode('|', $real_info);
        if ($data['astrict_sex'] && ($data['astrict_sex'] == 1 || $data['astrict_sex'] == 2)) {
            $is_real = 1;
            if (!in_array(2, $real_info_arr) && !in_array(8, $real_info_arr)) {
                $real_info .= "|8";
            }
        }
        if ($data['start_age'] || $data['end_age']) {
            $is_real = 1;
            if (!in_array(2, $real_info_arr) && !in_array(9, $real_info_arr)) {
                $real_info .= "|9";
            }
        }
        $real_info = trim($real_info, '|');
        return [$is_real, $real_info];
    }

    /**
     * 判断某个活动是否有人报名
     */
    public function actIsApply($id)
    {
        $apply_status = ActivityApply::where('act_id', $id)->where(function ($query) {
            $query->whereRaw("status = 1 || status = 4");
        })->first();

        return $apply_status ? true : false;
    }

    /**
     * 活动日历显示状态  （只显示活动状态，以活动时间做参考标准）
     * state 11、活动未开始 12、活动中 6、活动已结束
     */
    public function getActListCalendarState($act_data)
    {
        $now_date = date('Y-m-d');
        if (date('Y-m-d', strtotime($act_data['start_time'])) > $now_date) {
            $state = 11;
        } elseif (date('Y-m-d', strtotime($act_data['start_time'])) <= $now_date && date('Y-m-d', strtotime($act_data['end_time'])) >= $now_date) {
            $state = 12;
        } else {
            $state = 6;
        }
        return $state;
    }


    /**
     * 活动列表状态
     * state 1 未开始报名 2.报名中 3.报名已结束 4.无需报名且活动未结束 (直接前往)  5 名额已满 6.活动已结束
     */
    public function getActListState($act_data)
    {
        $now_date = date('Y-m-d H:i:s');
        if ($act_data['end_time'] < $now_date) {
            $state = 6;
        } elseif ($act_data['is_apply'] == 2) {
            $state = 4;
        } elseif ($act_data['apply_start_time'] > $now_date) {
            $state = 1;
        } elseif ($act_data['apply_start_time'] < $now_date && $act_data['apply_end_time'] > $now_date) {
            if ($act_data['number'] !== 0 && $act_data['apply_number'] >= $act_data['number']) {
                $state = 5;
            } else {
                $state = 2;
            }
        } else {
            $state = 3;
        }
        return $state;
    }
    /**
     * 活动详情状态 包括用户状态
     * state 1 未开始报名 2.报名中 3.报名已结束 4.无需报名且活动未结束 (直接前往)  5 名额已满 6.活动已结束
     *  7 审核中 8已取消（针对取消后不可报名的活动） 9 已拒绝  10 已通过
     * 
     * @param  $apply_id 报名id  增对一个活动报名多次，产生不同记录
     */
    public function getActDetailState($act_data, $user_id = null, $apply_id = null)
    {
        $now_date = date('Y-m-d H:i:s');

        //查询我的报名信息
        $self_apply_info = null;
        if (!empty($user_id)) {
            $activityApplyModel = new ActivityApply();
            $self_apply_info = $activityApplyModel->getApplyInfo($user_id, $act_data['id'], $apply_id); //获取报名信息
        }

        if ($act_data['end_time'] < $now_date) {
            $state = 6;
        } elseif ($act_data['is_apply'] == 2) {
            $state = 4;
        } elseif ($act_data['apply_start_time'] > $now_date) {
            $state = 1;
        } elseif (!empty($self_apply_info)) {
            if ($self_apply_info['status'] == 1) {
                $state = 10;
            } else if ($self_apply_info['status'] == 4) {
                $state = 7;
            } elseif ($act_data['is_continue'] == 1) {
                //取消后或拒绝后还可以报名
                if ($act_data['number'] !== 0 && $act_data['apply_number'] >= $act_data['number']) {
                    $state = 5;
                } elseif ($act_data['apply_start_time'] < $now_date && $act_data['apply_end_time'] > $now_date) {
                    $state = 2;
                } else {
                    $state = 3;
                }
            } elseif ($self_apply_info['status'] == 2) {
                $state = 8;
            } elseif ($self_apply_info['status'] == 3) {
                $state = 9;
            }
        } elseif ($act_data['apply_start_time'] < $now_date && $act_data['apply_end_time'] > $now_date) {
            if ($act_data['number'] !== 0 && $act_data['apply_number'] >= $act_data['number']) {
                $state = 5;
            } else {
                $state = 2; //number 为 0，表示名额不限
            }
        } else {
            $state = 3;
        }

        return $state;
    }

    /**
     * 获取状态名称
     * @param status
     *
     * 活动详情状态 包括用户状态
     * state 1 未开始报名 2.报名中 3.报名已结束 4.无需报名且活动未结束 (直接前往)  5 名额已满 6.活动已结束
     *  7 审核中 8已取消（针对取消后不可报名的活动） 9 已拒绝  10 已通过
     * 
     * @param  $apply_id 报名id  增对一个活动报名多次，产生不同记录
     */
    public function getStatusName($status)
    {
        switch ($status) {
            case 1:
                $name = '未开始报名';
                break;
            case 2:
                $name = '报名中';
                break;
            case 3:
                $name = '报名已结束';
                break;
            case 4:
                $name = '直接前往';
                break;
            case 5:
                $name = '名额已满';
                break;
            case 6:
                $name = '活动已结束';
                break;
            case 7:
                $name = '审核中';
                break;
            case 8:
                $name = '已取消';
                break;
            case 9:
                $name = '已拒绝';
                break;
            case 10:
                $name = '已通过';
                break;
            case 11:
                $name = '活动未开始';
                break;
            case 12:
                $name = '活动中';
                break;
            default:
                $name = '网络错误';
                break;
        }
        return $name;
    }

    /**
     * 检查活动报名填报信息
     * @param array $act_info 活动信息
     * @param array $real_info
     * @param $data 需要验证的真实信息id用|连接起来    1、姓名   2 、身份证号码  3、电话号码   4、读者证号码    5、真实人物头像   6、备注  7 单位名称 8 性别 9 年龄  10、附件 
     *                  11、学校  12、年级  13、班级  14、微信号
     * @return array
     */

    public function checkApplyParam($act_info, array $real_info, array $data)
    {
        $where = [];
        foreach ($real_info as $key => $val) {
            if ($val == 1) {
                if (empty($data['username']) || mb_strlen($data['username']) < 2) {
                    throw new Exception('用户名规则不正确');
                } else {
                    $where['username'] = $data['username'];
                }
            } elseif ($val == 2) {
                if (empty($data['id_card']) || !is_legal_no($data['id_card'])) {
                    throw new Exception('身份证号码规则不正确');
                } else {
                    $where['id_card'] = $data['id_card'];
                }
            } elseif ($val == 3) {
                if (empty($data['tel']) || !verify_tel($data['tel'])) {
                    throw new Exception('电话号码规则不正确');
                } else {
                    $where['tel'] = $data['tel'];
                }
            } elseif ($val == 4) {
                if (empty($data['reader_id'])) {
                    throw new Exception('读者证不能为空');
                } else {
                    $where['reader_id'] = $data['reader_id'];
                }
            } elseif ($val == 5) {
                if (empty($data['img'])) {
                    throw new Exception('头像不能为空');
                } else {
                    $where['img'] = $data['img'];
                }
            } elseif ($val == 6) {
                // if (empty($data['remark'])) {
                //     throw new Exception('备注不能为空');
                // } else {
                $where['remark'] = addslashes($data['remark']); //备注可为空
                // }
            } elseif ($val == 7) {
                if (empty($data['unit'])) {
                    throw new Exception('单位不能为空');
                } else {
                    $where['unit'] = $data['unit'];
                }
            } elseif ($val == 8) {
                if (empty($data['sex'])  || ($data['sex'] != 1 && $data['sex'] != 2)) {
                    throw new Exception('性别规则不正确');
                } else {
                    $where['sex'] = $data['sex'];
                }
            } elseif ($val == 9) {
                if (empty($data['age']) || !is_numeric($data['age']) || $data['age'] > 100) {
                    throw new Exception('年龄规则不正确');
                } else {
                    $where['age'] = $data['age'];
                }
            } elseif ($val == 11) {
                if (empty($data['school']) || mb_strlen($data['school']) > 100 || mb_strlen($data['school']) < 3) {
                    throw new Exception('学校名称规则不正确');
                } else {
                    $where['school'] = $data['school'];
                }
            } elseif ($val == 12) {
                if (empty($data['grade']) || mb_strlen($data['grade']) > 100) {
                    throw new Exception('年级规则不正确');
                } else {
                    $where['grade'] = $data['grade'];
                }
            } elseif ($val == 13) {
                if (empty($data['class_grade']) || mb_strlen($data['class_grade']) > 100) {
                    throw new Exception('班级规则不正确');
                } else {
                    $where['class_grade'] = $data['class_grade'];
                }
            } elseif ($val == 14) {
                if (empty($data['wechat_number']) || mb_strlen($data['wechat_number']) > 100) {
                    throw new Exception('微信号规则不正确');
                } else {
                    $where['wechat_number'] = $data['wechat_number'];
                }
            }
        }
        //判断年龄和性别是否符合要求
        if ($act_info['start_age'] || $act_info['end_age']) {
            $age = !empty($data['age']) ?  $data['age'] : get_user_age_by_id_card($data['id_card']);

            if ($act_info['start_age'] && $act_info['start_age'] > $age) {
                throw new Exception('年龄不符合要求');
            }
            if ($act_info['end_age'] && $act_info['end_age'] < $age) {
                throw new Exception('年龄不符合要求');
            }
        }
        if ($act_info['astrict_sex'] && ($act_info['astrict_sex'] == 1 || $act_info['astrict_sex'] == 2)) {
            $sex = !empty($data['sex']) ?  $data['sex'] : get_user_sex_by_id_card($data['id_card']);

            if ($act_info['astrict_sex'] != $sex) {
                throw new Exception('性别不符合要求');
            }
        }
        return $where;
    }

    /**
     * 获取当月活动下的所有活动及状态
     * @param $data 活动数据
     * @param $time 时间   年月
     */
    public function getActInfoActivityState($data, $time)
    {
        $day = get_day_by_year_month($time);
        $data_arr = [];
        for ($i = 1; $i <= $day; $i++) {
            $date = $time . '-' . ($i < 10 ? '0' . $i : $i);
            $data_arr[$i - 1]['data'] = [];
            $data_arr[$i - 1]['date'] = $date;
            $data_arr[$i - 1]['state'] = [];
            foreach ($data as $key => $val) {
                $times = $val['start_time']; //统一采用活动时间
                if ($date . ' 00:00:00' <= $times && $date . ' 23:59:59' >= $times) {
                    $val['state'] = $this->getActListCalendarState($val); //已活动状态来显示
                    $data_arr[$i - 1]['data'][] = $val;
                    $data_arr[$i - 1]['state'][] = $val['state'];
                }
            }
            $data_arr[$i - 1]['state'] = array_unique($data_arr[$i - 1]['state']); //状态值保留一个
        }
        return $data_arr;
    }
    /**
     * 日历获取，按月返回
     */
    public function getActListByMonth($month)
    {
        $res = $this->from($this->table . ' as a')
            ->select([
                'a.id', 'a.title', 'a.apply_start_time', 'apply_end_time', 'img', 'province', 'city', 'district', 'street', 'address',
                'a.start_time', 'a.end_time', 'is_apply', 'number', 'apply_number', 'a.create_time',
                DB::raw("DATE_FORMAT(start_time,'%Y-%m-%d') as start_date")
            ])
            ->join('activity_type as t', 't.id', '=', 'a.type_id')
            ->where('t.is_del', 1)
            ->where('a.is_del', 1)
            ->where('a.is_play', 1)
            //->whereBetween('a.start_time' , [$time . ' 00:00:00' , $time. ' 23:59:59'])
            ->where(function ($query) use ($month) {
                //$query->whereRaw(DB::raw("DATE_FORMAT(apply_start_time,'%Y-%m') = '$month' || DATE_FORMAT(start_time,'%Y-%m') = '$month'"));
                $query->whereRaw(DB::raw("DATE_FORMAT(start_time,'%Y-%m') = '$month'")); //只按活动开始时间显示日历
            })
            ->orderBy('is_recom')
            ->orderByDesc('end_time')
            ->get() //最多返回 100 条，实际数据不可能这么多
            ->toArray();

        foreach ($res as $key => $val) {
            $res[$key]['status'] = $this->getActListState($val); //增加列表显示状态
        }

        $data = $this->getActInfoActivityState($res, $month); //增加日志显示状态



        return $data;
    }


    /**
     * 日历获取，按时间返回
     */
    public function getActListByTime($start_time, $end_time)
    {
        $data = $this->from($this->table . ' as a')
            ->select([
                'a.id', 'a.title', 'a.apply_start_time', 'apply_end_time', 'img', 'province', 'city', 'district', 'street', 'address',
                'a.start_time', 'a.end_time', 'is_apply', 'number', 'apply_number', 'a.create_time',
                DB::raw("DATE_FORMAT(start_time,'%Y-%m-%d') as start_date")
            ])
            ->join('activity_type as t', 't.id', '=', 'a.type_id')
            ->where('t.is_del', 1)
            ->where('a.is_del', 1)
            ->where('a.is_play', 1)
            //->whereBetween('a.start_time' , [$time . ' 00:00:00' , $time. ' 23:59:59'])
            // ->where(function ($query) use ($month) {
            //     //$query->whereRaw(DB::raw("DATE_FORMAT(apply_start_time,'%Y-%m') = '$month' || DATE_FORMAT(start_time,'%Y-%m') = '$month'"));
            //     $query->whereRaw(DB::raw("DATE_FORMAT(start_time,'%Y-%m') = '$month'")); //只按活动开始时间显示日历
            // })
            ->where(function ($query) use ($start_time, $end_time) {
                $query->whereBetween('start_time', [$start_time . ' 00:00:00', $end_time . ' 23:59:59']); //只按活动开始时间显示日历
            })
            ->orderBy('is_recom')
            ->orderByDesc('end_time')
            ->get() //最多返回 100 条，实际数据不可能这么多
            ->toArray();

        foreach ($data as $key => $val) {
            $data[$key]['state'] = $this->getActListCalendarState($val); //日历显示状态
            $data[$key]['status'] = $this->getActListState($val); //列表显示状态
        }
        return $data;
    }




    /**
     * 活动推荐
     * @param $type_id 类型id
     * @param $is_recom 是否查询热门活动，false 不需要（全部活动都可以）  true 只查询热门的活动
     */
    public function getActRecomList($limit, $type_id = null, $is_recom = false)
    {
        $res = $this->from($this->table . ' as a')
            ->select('t.type_name', 't.id as type_id', 'a.id', 'a.title', 'img', 'province', 'a.apply_start_time', 'apply_end_time', 'a.start_time', 'a.end_time', 'city', 'district', 'street', 'address', 'is_apply', 'number', 'apply_number', 'a.create_time')
            ->join('activity_type as t', 't.id', '=', 'a.type_id')
            ->where(function ($query) use ($is_recom, $type_id) {
                if ($is_recom) {
                    $query->where('is_recom', 1);
                }
                if ($type_id) {
                    $query->where('type_id', $type_id);
                }
            })
            ->where('t.is_del', 1)
            ->where('a.is_del', 1)
            ->where('a.is_play', 1)
            ->orderBy('is_recom')
            ->orderByDesc('end_time')
            ->orderByDesc('a.id')
            ->limit($limit)
            ->get() //最多返回 100 条，实际数据不可能这么多
            ->toArray();

        if ($res) {
            foreach ($res as $key => $val) {
                $res[$key]['state'] = $this->getActListState($val); //获取状态
            }
        }

        return $res;
    }



    /**
     * 撤销、发布 活动
     * @param $id 活动id
     * @param $node   1发布   2 撤销 （是否发布  是否发布 1.发布 2.未发布    与 当前状态相反）
     */
    public function cancelAndRelease($id, $node)
    {
        $res = $this->where('is_del', 1)->where('id', $id)->first();

        if (!$res) {
            return "参数传递错误";
        }
        if ($res->is_play == $node) {
            return '活动状态有误';
        }

        $res->is_play = $node;
        $result = $res->save();

        if (!$result) {
            return  "发布失败";
        }

        return true;
    }


    /**
     * 添加和取消热门 活动    返回的数组 
     * @param $id 活动id
     * @param     是否推荐为 热门活动  1 是  2 否  
     */
    public function recomAndCancel($id)
    {
        $res = $this->where('is_del', 1)->where('id', $id)->first();

        if (!$res) {
            return [201, "参数传递错误"];
        }

        if ($res->is_recom == 1) {
            $node = 2;
            $msg = '取消热门活动';
        } else {
            $node = 1;
            $msg = '添加热门活动';
        }

        $res->is_recom = $node;
        $result = $res->save();

        if (!$result) {
            return [202, $msg . "失败"];
        }

        return [200, $msg . "成功"];
    }


    /**
     * 切换类型
     * @param $id 活动id
     * @param $type_id  类型id
     **/
    public function switchoverType($id, $type_id)
    {
        $res = $this->where('is_del', 1)->where('id', $id)->first();

        if (!$res) {
            return "参数传递错误";
        }

        $res->type_id = $type_id;
        $result = $res->save();

        if (!$result) {
            return  "切换失败";
        }

        return true;
    }
}
