<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use App\Http\Controllers\QrCodeController;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Log;

/**
 * 用户微信小程序信息
 * Class ArticleTypeModel
 * @package app\common\model
 */
class UserAppletInfo extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'user_applet_info';


    /**
     * 登录或修改微信信息
     * @param open_id open_id  openid作为主账号
     */
    public function change($data, $field = [], $findWhere = [])
    {
        $res = $this->where('open_id', $data['open_id'])->first();
        $is_insert_user_info = false;
        if (empty($res)) {
            $res = $this;
            $is_insert_user_info = true;

            //已经存在的不允许修改昵称
            $res->open_id = $data['open_id'];
            $res->nickname = $data['nickname'];
            if (!empty($data['head_img'])) {
                $res->head_img = $data['head_img'];
            } else {
                //没有头像获取一个默认头像
                $userInfoModel = new UserInfo();
                $res->head_img = $userInfoModel->getDefaultHeadImg();
            }
        }
        $res->union_id = !empty($data['union_id']) ? $data['union_id'] : null; //补充union_id
        if (!empty($data['session_key'])) $res->session_key = $data['session_key']; //小程序增加
        $res->save();

        //写入用户信息
        $data = [];
        $userInfoModel = new UserInfo();
        if ($is_insert_user_info) {
            $userInfoModel->token = get_guid();
            $userInfoModel->applet_id = $res->id;

            $qrCodeObj = new QrCodeController();
            $qr_code = $qrCodeObj->getQrCode('user_info');
            $qr_url = $qrCodeObj->setQr($qr_code, true);

            $userInfoModel->qr_code = $qr_code;
            $userInfoModel->qr_url = $qr_url;

            $userInfoModel->add($data);

            $token =  $userInfoModel->token;
            $qr_code =  $userInfoModel->qr_code;
        } else {
            $user_info = $userInfoModel->getUserInfoByAppletId($res->id);
            $qr_code = $user_info['qr_code'];
            $token = $user_info['token'];
        }

        return ['is_add' => $is_insert_user_info, 'applet_id' => $res->id, 'token' => $token, 'qr_code' => $qr_code, 'nickname' => $res->nickname, 'head_img' => $res->head_img];
    }

    /**
     * 登录或修改微信信息
     * @param tel 用手机号码作为主账号
     */
    public function changeByTel($data)
    {
        $res = $this->where('tel', $data['tel'])->first();
        $is_insert_user_info = false;
        if (empty($res)) {
            $res = $this;
            $is_insert_user_info = true;
            //已经存在的不允许修改昵称
            $res->nickname = $data['nickname'];
            $res->head_img = $data['head_img'];
        }
        $res->open_id = $data['open_id'];
        if (!empty($data['session_key'])) $res->session_key = $data['session_key']; //小程序增加
        $res->save();

        //写入用户信息
        $userInfoModel = new UserInfo();
        $user_info = $userInfoModel->where('tel', $data['tel'])->first();
        if (empty($user_info)) {
            $userInfoModel->token = get_guid();
            $userInfoModel->applet_id = $res->id;

            $qrCodeObj = new QrCodeController();
            $qr_code = $qrCodeObj->getQrCode('user_info');
            $qr_url = $qrCodeObj->setQr($qr_code, true);

            $userInfoModel->tel =  $data['tel'];
            $userInfoModel->qr_code = $qr_code;
            $userInfoModel->qr_url = $qr_url;

            $userInfoModel->save();

            $token =  $userInfoModel->token;
            $qr_code =  $userInfoModel->qr_code;
        } elseif ($user_info && empty($user_info['applet_id'])) {
            $user_info->applet_id = $res->id;
            $user_info->save();
        }

        if ($user_info) {
            $qr_code = $user_info['qr_code'];
            $token = $user_info['token'];
        }
        return ['is_add' => $is_insert_user_info, 'applet_id' => $res->id, 'token' => $token, 'qr_code' => $qr_code, 'nickname' => $res->nickname, 'head_img' => $res->head_img];
    }


    /**
     * 修改微信昵称和头像
     */
    public function wechatInfoChange($applet_id, $nickname, $head_img)
    {
        $data = [];
        if ($head_img) {
            $controllerObj = new Controller();
            $head_img = strpos($head_img, 'http') !== false ? $head_img : ($controllerObj->getImgAddrUrl() . $head_img);
            $data['head_img'] = $head_img;
        }
        if ($nickname) {
            $data['nickname'] = $nickname;
        }

        $res = $this->where('id', $applet_id)->update($data);
        return $res;
    }

    /**
     * 获取用户open_id
     * @param $user_id
     */
    public static function getOpenIdByUserId($user_id)
    {
        $applet_id = UserInfo::where('id', $user_id)->value('applet_id');
        return self::where('id', $applet_id)->value('open_id');
    }

    /**
     * 根据wechat_id 获取读微信信息
     */
    public function getWechatByWechatIds($wechat_id_arr)
    {
        $res = $this->select('head_img', 'nickname')->whereIn('id', $wechat_id_arr)->get();
        return $res;
    }
}
