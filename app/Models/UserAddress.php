<?php
namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * 用户地址管理
 */
class UserAddress extends BaseModel
{
    use HasFactory;

    const CREATED_AT='create_time';
    const UPDATED_AT='change_time';

    
    protected $table = 'user_address';

  /**
     * 获取用户默认地址
     */
    public function getDefaultAddress($user_id){
        $res = $this
            ->where('user_id' , $user_id)
            ->where('is_del' ,1)
            ->where('is_default' , 1)
            ->first();
        return $res;
    }

    /**
     * 获取用户地址
     */
    public function getAddress($user_id){
        $res = $this
            ->where('user_id' , $user_id)
            ->where('is_del' ,1)
            ->first();
        return $res;
    }


    
}