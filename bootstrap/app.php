<?php

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| The first thing we will do is create a new Laravel application instance
| which serves as the "glue" for all the components of Laravel, and is
| the IoC container for the system binding all of the various parts.
|
*/

$app = new Illuminate\Foundation\Application(
    $_ENV['APP_BASE_PATH'] ?? dirname(__DIR__)
);

/*
|--------------------------------------------------------------------------
| Bind Important Interfaces
|--------------------------------------------------------------------------
|
| Next, we need to bind some important interfaces into the container so
| we will be able to resolve them when needed. The kernels serve the
| incoming requests to this application from both the web and CLI.
|
*/

$app->singleton(
    Illuminate\Contracts\Http\Kernel::class,
    App\Http\Kernel::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

/*
|--------------------------------------------------------------------------
| Return The Application
|--------------------------------------------------------------------------
|
| This script returns the application instance. The instance is given to
| the calling script so we can separate the building of the instances
| from the actual running of the application and sending responses.
|
*/



//**********************代码添加到这块*******************************//
/**根据服务器的hostname,加载不同的.env文件
*获取主机名 
*这里方式可以根据自己需求修改 可以根据自己域名区分.
*我的这里根据服务器的hostname进行区分
*/

// $hostname = gethostname();
// //自定义env文件路径
// //$app->useEnvironmentPath(realpath(__DIR__.'/../env'));

// $server_name = $_SERVER['SERVER_NAME'];
// $local_host_name = ['admin-PC'];//本地
// $local_service_name = '192.168';//本地

// if ($server_name = '127.0.0.1') {
//     //dev环境
//     $app->loadEnvironmentFrom('.env_dev');
// }elseif (strpos($server_name, $local_service_name) !== false) {
//    //dev环境
//   $app->loadEnvironmentFrom('.env_dev');
// }elseif (in_array($hostname, $local_host_name)) {
//     //dev环境
//    $app->loadEnvironmentFrom('.env_dev');
// }elseif (strpos($hostname, $strDev) !== false) {
//   //prod环境
//   $app->loadEnvironmentFrom('.env_prod');//正式环境
// } else {
//   //其他情况，走dev分支
//   $app->loadEnvironmentFrom('.env_dev');
// }
//**********************代码添加到这块*******************************//



//**********************自动判断加载不同的env文件 ------ 代码添加到这块*******************************//
/**根据服务器的hostname,加载不同的.env文件
 *获取主机名 
 *这里方式可以根据自己需求修改 可以根据自己域名区分.
 *我的这里根据服务器的hostname进行区分
 */

//自定义env文件路径
//$app->useEnvironmentPath(realpath(__DIR__.'/../env'));


$host_name = gethostname();

//函数php_sapi_name()  php_sapi_name() == 'cli'获取现在的运行环境,cli命令下，获取不到域名，所以用域名方式无效

$localhost_name = ['admin-PC','BF-202404211215'];//本地主机地址，用于切换不同的env文件

if (!in_array($host_name , $localhost_name)) {
    //prod正式环境
    $app->loadEnvironmentFrom('.env_prod');
} else {
    //其他情况，走dev分支 dev本地测试环境
    $app->loadEnvironmentFrom('.env_dev');
}


//**********************代码添加到这块*******************************//


return $app;
